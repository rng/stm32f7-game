# vim: set ft=make:

MAKEFLAGS += --jobs=2
PLAT ?= unix
# Set to the path to stm32f7-game checkout
BASE_DIR := .
GAME_DIR := example

ASSETTABLE = $(GAME_DIR)/asset_table.h

SRCS := \
    $(GAME_DIR)/game.cc \
    $(GAME_DIR)/player.cc \

GAME_ASSETS := \
    $(GAME_DIR)/data/ball_8x8.dat \
    $(GAME_DIR)/data/player_8x8.dat \
    $(GAME_DIR)/data/icons_8x8.dat \
    $(GAME_DIR)/data/water_8x8.dat \
    $(GAME_DIR)/data/spray_24x16.dat \
    $(GAME_DIR)/data/torch_8x12.dat \
    \
    $(GAME_DIR)/data/testmap.dat \
    $(GAME_DIR)/data/tiles_testmap_8x8.dat \

include $(BASE_DIR)/src/plat_$(PLAT)/defs.mk
include $(BASE_DIR)/common.mk
