# vim: set ft=make:

BINDIR ?= build/$(PLAT)
UNIFIED_BUILD ?= 0
LOADERBIN := $(BINDIR)/gameloader$(BINEXT)
GAMEBIN := $(BINDIR)/$(GAME_DIR)/game$(SOEXT)

BUILD_TIME = $(shell python3 -c "import time; print(int(time.time()))")
BUILD_SHA = $(shell git rev-parse --short HEAD | tr a-z A-Z)
BASECFLAGS := $(PLATFLAGS) $(INCS) -O$(OPT) -g -Wall -Werror -Wdouble-promotion -MMD -MP
BASECFLAGS += -DDEBUG=1 \
			  -DUNIFIED_BUILD=$(UNIFIED_BUILD) \
			  -DBUILD_TIME=$(BUILD_TIME) \
			  -DBUILD_SHA=\"$(BUILD_SHA)\" \
			  -DGAME_NAME=$(GAME_DIR) \
			  $(GAMEDEFS)
CFLAGS := $(BASECFLAGS) -std=gnu11
CXXFLAGS := $(BASECFLAGS) -std=c++17 -fno-rtti -fno-exceptions -Wno-register

ENGINE_SRCS := \
    $(BASE_DIR)/src/engine/asset.cc \
    $(BASE_DIR)/src/engine/anim.cc \
    $(BASE_DIR)/src/engine/bitmap.cc \
    $(BASE_DIR)/src/engine/camera.cc \
    $(BASE_DIR)/src/engine/collide.cc \
    $(BASE_DIR)/src/engine/common.c \
    $(BASE_DIR)/src/engine/entitymgr.cc \
    $(BASE_DIR)/src/engine/font.cc \
    $(BASE_DIR)/src/engine/render.cc \
    $(BASE_DIR)/src/engine/scene.cc \
    $(BASE_DIR)/src/engine/sound.cc \
    $(BASE_DIR)/src/engine/tilemap.cc \
    $(BASE_DIR)/src/engine/ui.cc \
    $(BASE_DIR)/src/lib/tinyprintf/tinyprintf.c \

PLATSRCS += $(BASE_DIR)/src/engine/asset.cc \
            $(BASE_DIR)/src/engine/bitmap.cc \
            $(BASE_DIR)/src/engine/common.c \
            $(BASE_DIR)/src/engine/ui.cc \
            $(BASE_DIR)/src/system/core.cc \
            $(BASE_DIR)/src/engine/font.cc \
            $(BASE_DIR)/src/engine/sound.cc \
            $(BASE_DIR)/src/plat_common/debug.cc \

GAME_PALETTE ?= $(BASE_DIR)/example/data/syspalette.gpl
GAME_ASSETS += $(GAME_PALETTE:%.gpl=%.dat)

SRCS += $(ENGINE_SRCS) $(BASE_DIR)/src/engine/gamelibstub.cc
OBJS = $(SRCS:%=$(BINDIR)/%.o) $(ASRCS:%=$(BINDIR)/%.o)
ASSETS = $(GAME_ASSETS:%.dat=$(BINDIR)/%.dat)
PLATOBJS = $(PLATSRCS:%=$(BINDIR)/%.o) $(PLATASRCS:%=$(BINDIR)/%.o)
GAMEOBJS = $(SRCS:%=$(BINDIR)/%.o)
DEPS = $(GAMEOBJS:%.o=%.d) $(PLATOBJS:%.o=%.d)
DISTZIP = dist/$(GAME_DIR)_$(PLAT).zip

ifeq ($(UNIFIED_BUILD), 1)
PLATOBJS += $(GAMEOBJS)
endif

PLATTARGS = $(PLATTARGETS:%=$(BINDIR)/%)

all: $(LOADERBIN) $(GAMEBIN) $(PLATTARGS) $(ASSETS)

-include $(DEPS)

src/system/core.cc: src/system/battery_16x8.h
src/engine/font.cc: src/engine/fontsystem_4x6.h

# code

$(LOADERBIN): $(PLATOBJS)
	@echo "[LD]    $@"
	@$(CC) -o $@ $^ $(LDFLAGS) $(PLATLDFLAGS)

$(GAMEBIN): $(GAMEOBJS)
	@echo "[LD]    $@"
	@$(CC) -o $@ $^ $(LDFLAGS) $(GAMELDFLAGS)

%.bin: %.elf
	@echo "[OBJCP] $@"
	@$(OBJCOPY) -Obinary $< $@

%.lst: %.elf
	@echo "[LST]   $@"
	@$(OBJDUMP) -d $< > $@

$(BINDIR)/%.s.o: %.s
	@echo "[AS]    $@"
	@mkdir -p $(dir $@)
	@$(AS) $(ARCHFLAGS) -o $@ -c $<

$(BINDIR)/%.c.o: %.c
	@echo "[CC]    $@"
	@mkdir -p $(dir $@)
	@$(CXX) -x c $(CFLAGS) -c -o $@ $<

$(BINDIR)/%.cc.o: %.cc $(ASSETTABLE) # FIXME: hack to force asset table generation
	@echo "[CXX]   $@"
	@mkdir -p $(dir $@)
	@$(CXX) $(CXXFLAGS) -c -o $@ $<

# assets

$(ASSETTABLE): $(ASSETS)
	@echo "[TABLE] $@"
	@$(BASE_DIR)/tool/assetheader.py $(GAME_DIR) $@ $^

$(BINDIR)/%.dat: %.png $(GAME_PALETTE)
	@echo "[ASSET] $@"
	@mkdir -p $(dir $@)
	@$(BASE_DIR)/tool/assetconvert.py -P $(GAME_PALETTE) $< $@

src/%.h: data/%.png
	@echo "[ASSET] $@"
	@mkdir -p $(dir $@)
	@$(BASE_DIR)/tool/assetconvert.py -P $(GAME_PALETTE) -H $< $@

$(BINDIR)/%.dat: %.json
	@echo "[ASSET] $@"
	@mkdir -p $(dir $@)
	@$(BASE_DIR)/tool/assetconvert.py $< $@

$(BINDIR)/%.dat: %.wav
	@echo "[ASSET] $@"
	@mkdir -p $(dir $@)
	@$(BASE_DIR)/tool/assetconvert.py $< $@

$(BINDIR)/%.dat: %.gpl
	@echo "[ASSET] $@"
	@mkdir -p $(dir $@)
	@$(BASE_DIR)/tool/assetconvert.py $< $@

$(BINDIR)/%.html: $(BASE_DIR)/misc/%.html
	@echo "[HTML]  $@"
	@cp $< $@

$(BINDIR)/%.wasm: $(BINDIR)/%.js
	@true

$(BINDIR)/%.data: $(BINDIR)/%.js
	@true

# misc

.PHONY: dist
$(DISTZIP): $(GAMEBIN) $(LOADERBIN) $(PLATTARGS) $(ASSETS)
	@echo "[DIST]"
	$(STRIP) $(LOADERBIN) $(GAMEBIN)
	@rm -rf dist
	@mkdir -p dist/$(GAME_DIR)
	@cp --parents $^ dist/$(GAME_DIR)
	@mv dist/$(GAME_DIR)/build/$(PLAT)/* dist/$(GAME_DIR)
	@rm -rf dist/$(GAME_DIR)/build
	@cd dist; zip -r $(GAME_DIR)_$(PLAT).zip $(GAME_DIR)

dist: $(DISTZIP)

run: $(LOADERBIN) $(GAMEBIN) $(ASSETS)
	ASAN_OPTIONS=detect_leaks=0 ./$<

debug: $(LOADERBIN) $(GAMEBIN) $(ASSETS)
	$(GDB) $(DEBUG_ARGS) $<

debugserver:
	openocd -f $(DEBUG_SERVERCFG)

itchupload: $(DISTZIP)
ifndef ITCHNAME
	$(error "need to define ITCHNAME")
endif
	butler push $(DISTZIP) $(ITCHNAME):$(PLAT)

.PHONY: size
size: $(LOADERBIN)
	$(SIZE) $^

.PHONY: clean
clean:
	@echo "[CLEAN]"
	@rm -rf $(BINDIR) $(GAMEOBJS) $(DEPS) $(ASSETS) $(ASSETTABLE) dist *.zip

format:
	find . -name "*.c" -o -name "*.cc" -o -name "*.h" | grep -v "lib" | xargs clang-format -i

wc:
	@echo "platform:"
	@find $(BASE_DIR)/src/plat* $(BASE_DIR)/src/arch* -type f -a \( -name "*.c" -o -name "*.cc" -o -name "*.h" \) | xargs wc -l | sort -n
	@echo "system:"
	@find $(BASE_DIR)/src/system -name "*.c" -o -name "*.cc" -o -name "*.h" | xargs wc -l | sort -n
	@echo "engine:"
	@find $(BASE_DIR)/src/engine -name "*.c" -o -name "*.cc" -o -name "*.h" | xargs wc -l | sort -n
	@echo "game:"
	@find $(GAME_DIR) -name "*.c" -o -name "*.cc" -o -name "*.h" | xargs wc -l | sort -n
