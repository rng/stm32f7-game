#include "engine/asset.h"
#include "engine/font.h"

typedef struct game_state_t {
    uint32_t screen_w, screen_h;
    bitmap_t sprite;
} game_state_t;

platform_api_t *platform;

struct game_state_t *game_init(platform_api_t *platform_api, platform_params_t *params) {
    platform = platform_api;
    struct game_state_t *game = (game_state_t *)platform->alloc(sizeof(game_state_t));

    game->screen_w = params->screen_w;
    game->screen_h = params->screen_h;

    asset_load_image("data/buttons_24x16.dat", &game->sprite);

    return game;
}

void game_term(game_state_t *game) {
}

void game_tick(game_state_t *game, input_state_t *input, uint8_t *framebuffer) {
    bitmap_t screen = BITMAP8(game->screen_w, game->screen_h, framebuffer);
    bitmap_fill(&screen, 0);

#define BUTTON(key, x, y, frame) \
    bitmap_blit_tile(&screen, &game->sprite, x, y, false, (key) ? (frame + 9) : (frame));

    BUTTON(input->c, 20, 24, 0);     // left shoulder
    BUTTON(input->d, 80, 24, 1);     // right shoulder
    BUTTON(input->up, 20, 50, 2);    // Up
    BUTTON(input->right, 36, 62, 3); // Right
    BUTTON(input->down, 20, 78, 4);  // Down
    BUTTON(input->left, 8, 62, 5);   // Left
    BUTTON(input->a, 100, 62, 6);    // A
    BUTTON(input->b, 80, 62, 7);     // B
    BUTTON(input->menu, 80, 82, 8);  // Menu

    font_print_col(&screen, font_system(), 40, 8, "Button Test", 254);
}

void game_audio(game_state_t *game, void *audio_buf, size_t audio_buf_len) {
}

extern "C" {
GAME_API_EXPORT
game_api_t get_game_api(void) {
    static const game_api_t game_api = {
        .init = game_init,
        .term = game_term,
        .tick = game_tick,
        .audio = game_audio,
    };
    return game_api;
}
}
