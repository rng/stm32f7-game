# AS OF YET UNNAMED GAME CONSOLE THING

This project aims to build a 2D game engine for small 32bit microcontrollers as well as an associated piece of (Gameboy Advance-like) hardware on which to run games. The primary goal is to serve as an educational piece of software & hardware.

![example game gif](https://bitbucket.org/rng/stm32f7-game/raw/master/doc/images/readme-example-game.gif)

## Status

The engine is currently under active development and APIs are expected to change. There is sufficient infrastructure to build a simple game, runnable on PC, Web, and two custom hardware platforms. For a detailed techncial dive into the design see the [RFCs][rfcs].

#### Platform support

| Platform                           | Status   | Input               | Notes |
| -----------------------------------|----------|---------------------|-------|
| **PC**                             |          |                     |       |
| Linux                              | Complete | Keyboard/Controller |       |
| Mac OS X                           | Complete | Keyboard/Controller | 1     |
| Windows (mingw)                    | Complete | Keyboard/Controller |       |
| Web (emscripten)                   | Complete | Keyboard            |       |
|                                    |          |                     |       |
| **Hardware**                       |          |                     |       |
| [Miniconsole][miniconsole]         | Complete | Dpad/buttons        |       |

Notes:

1. No app bundling, so needs to be launched from the commandline

#### Engine and Tooling

* Basic asset system for loading data from disk
* Support for clipping and blitting images to the framebuffer
* 8bit Palette loading
* Tilemap loader and renderer
* Bitmap font rendering
* Tile-entity collision system
* Asset converter tool to convert images, fonts, and tilemaps to native asset format

### Software Architecture

A game built with the game engine can be divided into 3 major components:

1. Platform layer. The platform layers is platform specific (PC or STM32) and abstracts away low-level hardware from the game engine. The primary interface to higher level code is the `game_api_t` structure which must be implemented by game code. The platform layer runs the main loop and calls a `game_tick` function every frame, passing in user input as well as a framebuffer for the game to render to.
2. Engine. The game engine is a collection of loosely coupled libraries that provide higher level abstractions on top of the platform `game_api_t` layer. For example the engine provides simple libraries to load images from disk and quickly draw them to the screen. The engine is platform agnostic and depends only on the platform API to interact with hardware.
3. Game logic. Platform agnostic game-specific logic is implemented on top of the engine. This logic is provided by developers (not part of this repo, other than the example game).

```
.----------------------.  -.
|     Game Logic       |   |
|      example/        |   |
'----------------------'   |
  engine/*.h interface     | Separate game binary
.----------------------.   | (dynamic library)
|        Engine        |   |
|      src/engine      |   |
'----------------------'  -'
  game_api_t interface   
.----------------------.  -.
|    Platform Layer    |   | Game "loader"
|     src/plat_*       |   | (main executable)
'----------------------'  -'
```

### Hardware Architecture

The custom board ("[miniconsole][miniconsole]") is based around the [STM32F722][stm32f722] which drives a 128x128 pixel LCD and a small onboard speaker. The engine should also run on many cortex-M series boards (it was initially tested on the [STM32F746-Discovery][stm32disco] development board).

## Getting Started

The game should build and run on Linux (tested on Ubuntu 17.04), Mac OS X (tested on 10.12), Windows (tested on 10), Web (using emscripten). Linux and Mac platforms are also capable of building/flashing the embedded (STM32) build.

### Setup

After cloning, make sure to check out the submodules:

    git submodule update --init --recursive

Next, install the Arm Toolchain from [developer.arm.com][arm]. 

### Build / Run / Debug

    # Build and run on PC
    make run

    # Build and debug on PC
    make debug

    # Build and run / debug on miniconsole hardware
    make PLAT=mini debugserver &
    make PLAT=mini debug

### Repo Layout

This repo contains the source for the engine, platform layers for each target platform, and an example game. The code is structured as follows:

    .
    ├── doc             # RFCs and other design documentation
    ├── example         # Example game
    ├── src             
    │   ├── engine      # Core engine 
    │   ├── lib         # 3rd party vendor code
    │   ├── arch_stm32  # Common code for STM32-based platforms
    │   ├── arch_pc     # Common code for PC platforms
    │   ├── plat_unix   # Platform layer for Mac/Linux
    │   ├── plat_web    # Platform layer for Web assembly
    │   ├── plat_win    # Platform layer for Windows
    │   ├── plat_mini   # Platform layer for Miniconsole board
    └── tool            # Tooling for data files used by the game

The intent (though not currently implemented) is for game developers to submodule this repo to be able to quickly start developing new games.

## Acknowledgements

This project makes use of the following:

- [Simple DirectMedia Layer][sdl]
- [jo gif][jogif]
- [FATFs][fatfs]
- [tinyprintf][tinyprintf]
- [Caves of Gallet tileset][caves]

## Contributing

One of the project goals is to make the process of building the console (hardware & software) well-documented as an educational resource. As such, we have an RFC process for big hardware and software decisions/changes. While this does increase the overhead of committing changes, the hope is that it provides an educational record for future readers of the project.

See the [Project Overview][overview] for more details on the RFC process and how to contribute, and the [Todo][todo] page for list of suggested starting points.


[rfcs]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/
[stm32disco]: https://www.st.com/en/evaluation-tools/32f746gdiscovery.html
[stm32f722]: https://www.st.com/en/microcontrollers-microprocessors/stm32f7x2.html
[miniconsole]: https://bitbucket.org/rng/miniconsole/src/master/
[overview]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/rfc-0001-project-overview.md
[todo]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/todo.md
[sdl]: https://www.libsdl.org/
[jogif]: https://www.jonolick.com/home/gif-writer
[fatfs]: http://elm-chan.org/fsw/ff/00index_e.html
[tinyprintf]: https://github.com/cjlano/tinyprintf
[caves]: https://adamatomic.itch.io/caves-of-gallet
[arm]: https://developer.arm.com/downloads
