#pragma once

#include <stddef.h>
#include <stdint.h>

#include "engine/bitmap.h"

// Debug stats logger

struct debug_frame_t;

typedef void (*debug_finish_fn_t)(void *context, debug_frame_t *frame);

// Mark the start of a game frame
void debug_start_frame(void);
// Mark the end of a game frame. `fn` will be called with the `debug_frame_t`
// provided context and the debug data collected during the frame.
void debug_finish_frame(debug_finish_fn_t fn, void *context);
// Add memory allocation statistics to the current debug frame
void debug_add_alloc_stats(size_t cur, size_t delta_alloc, size_t delta_free);
// Add named timing data to the current debug frame
void debug_add_timed_block(const char *name, uint64_t timestamp, uint32_t dt);

// Debug profile graph renderer. Draw a graph at the bottom of the screen with
// current and past debug frame data.

struct event_name_t {
    char v[16];
};

struct debug_profile_t {
    uint32_t w, h;
    uint8_t *events;
    event_name_t *names;
    uint32_t namecount;
    uint32_t framecount;
    uint32_t mem_current;
};

// Initialise debug renderer with given screen width and height
void debug_profile_init(debug_profile_t *prof, int w, int h);
// Render the current profile graph to the given framebuffer
void debug_profile_render(debug_profile_t *prof, bitmap_t *framebuffer, bool full);
// Callback function to be passed to `debug_finish_frame`
void debug_profile_finish(void *context, debug_frame_t *frame);
