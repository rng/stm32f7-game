#include "engine/common.h"
#include "engine/font.h"

#include "plat_common/debug.h"

#include <string.h>

#define MAX_DEBUG_EVENTS (8)

enum debug_event_type_t {
    DEBUG_EVENT_BLOCK,
    DEBUG_EVENT_MEM,
};

struct debug_event_t {
    debug_event_type_t type;
    union {
        struct {
            uint64_t t;
            uint32_t dt;
            char name[16];
        } block;
        struct {
            uint32_t delta_alloc, delta_free, cur;
        } mem;
    };
};

struct debug_frame_t {
    debug_event_t events[MAX_DEBUG_EVENTS];
    int event_count;
};

struct debug_state_t {
    debug_frame_t *curframe;
    uint32_t dropped_events;
};

debug_state_t debug_state;

static void debug_add_event(debug_event_t *event) {
    if (debug_state.curframe->event_count >= MAX_DEBUG_EVENTS) {
        debug_state.dropped_events++;
        return;
    }
    debug_state.curframe->events[debug_state.curframe->event_count++] = *event;
}

// public

void debug_start_frame(void) {
    debug_state.curframe = (debug_frame_t *)calloc(sizeof(debug_frame_t), 1);
    debug_state.curframe->event_count = 0;
}

void debug_add_alloc_stats(size_t cur, size_t delta_alloc, size_t delta_free) {
    debug_event_t evt = {};
    evt.type = DEBUG_EVENT_MEM;
    evt.mem.cur = cur;
    evt.mem.delta_alloc = delta_alloc;
    evt.mem.delta_free = delta_free;
    debug_add_event(&evt);
}

void debug_add_timed_block(const char *name, uint64_t timestamp, uint32_t dt) {
    debug_event_t evt = {};
    evt.type = DEBUG_EVENT_BLOCK;
    evt.block.t = timestamp;
    evt.block.dt = dt;
    strncpy(evt.block.name, name, sizeof(evt.block.name));
    debug_add_event(&evt);
}

void debug_finish_frame(debug_finish_fn_t fn, void *ctx) {
    fn(ctx, debug_state.curframe);
    debug_state.curframe = NULL;
}

// debug profile graph

// Profile graph parameters
#define PROF_STACK_MAX_BLOCKS (8)         // max number of timed blocks per frame
#define PROF_MEM_TOTAL (64 * 1024)        // total memory available to the game
#define PROF_STACK_SCALE_US_PER_PIX (100) // scaling factor for graph

// Profile graph colours, assuming system palette
#define COLOUR_DIVIDER (231)
#define COLOUR_MEM (160)
static const uint8_t debug_profile_cols[] = {160, 48, 32, 128, 64, 80, 192, 224};

void debug_profile_init(debug_profile_t *prof, int w, int h) {
    prof->w = w;
    prof->h = h;
    prof->events = (uint8_t *)calloc(w * PROF_STACK_MAX_BLOCKS, 1);
    prof->names = (event_name_t *)calloc(sizeof(event_name_t), PROF_STACK_MAX_BLOCKS);
    prof->framecount = 0;
}

#define PROF_STACK_END_MARKER (255)

void debug_profile_finish(void *ctx, debug_frame_t *frame) {
    debug_profile_t *prof = (debug_profile_t *)ctx;

    if (prof->framecount == prof->w) {
        // if we've filled up the full profile graph, shift all our frame records over 1
        memmove(prof->events, prof->events + PROF_STACK_MAX_BLOCKS,
                (prof->w - 1) * PROF_STACK_MAX_BLOCKS);
    } else {
        prof->framecount++;
    }

    uint8_t *stackptr = prof->events + ((prof->framecount - 1) * PROF_STACK_MAX_BLOCKS);
    prof->namecount = 0;
    // iterate through all the events in the frame, extracting data needed for the graph.
    for (int i = 0; i < frame->event_count; i++) {
        debug_event_t *event = &frame->events[i];
        switch (event->type) {
            case DEBUG_EVENT_BLOCK:
                if (strcmp(event->block.name, "waitframe") == 0) {
                    continue;
                }
                *stackptr++ = (event->block.dt / PROF_STACK_SCALE_US_PER_PIX);
                strncpy(prof->names[prof->namecount].v, event->block.name, sizeof(event_name_t));
                prof->namecount++;
                break;
            case DEBUG_EVENT_MEM:
                prof->mem_current = event->mem.cur;
                break;
        }
        *stackptr = PROF_STACK_END_MARKER;
    }
    free(frame);
}

void debug_profile_render(debug_profile_t *prof, bitmap_t *framebuffer, bool full) {
    int height = full ? framebuffer->height : prof->h;
    int scale = height / prof->h;

    int starty = framebuffer->height - height;
    // bitmap for the profile is placed at the bottom of the screen
    bitmap_t screen = BITMAP8(prof->w, height, framebuffer->data + (starty * prof->w));

    // profile stackup graph
    for (uint32_t i = 0; i < prof->framecount; i++) {
        uint8_t *stackptr = prof->events + (i * PROF_STACK_MAX_BLOCKS);
        int ypos = height - 1;
        for (int j = 0; j < PROF_STACK_MAX_BLOCKS; j++) {
            uint8_t timing_block = *stackptr++;
            if (timing_block == PROF_STACK_END_MARKER) {
                break;
            }
            uint8_t col = debug_profile_cols[j];
            int dy = timing_block * scale / 10;
            bitmap_draw_vline(&screen, i, ypos, ypos - dy, col);
            ypos -= dy;
        }
    }

    if (full) {
        // for full screen profile graph render event names
        uint8_t *stackptr = prof->events + (prof->framecount - 1) * PROF_STACK_MAX_BLOCKS;
        int ypos = height - 1;
        int nidx = 0;
        for (int j = 0; j < PROF_STACK_MAX_BLOCKS; j++) {
            uint8_t timing_block = *stackptr++;
            if (timing_block == PROF_STACK_END_MARKER) {
                break;
            }
            char *name = prof->names[nidx++].v;
            if (!timing_block) {
                continue;
            }
            int x = screen.width - (strlen(name) + 5) * 4;
            int dy = timing_block * scale / 10;
            font_printf_col(&screen, font_system(), x, ypos - dy, 0, "%s:%2dms", name,
                            timing_block / 10);
            ypos -= dy;
        }
    }

    // high water mark
    bitmap_draw_hline(&screen, 0, 4, 1, COLOUR_DIVIDER);
    bitmap_draw_hline(&screen, prof->w - 4 - 1, prof->w, 1, COLOUR_DIVIDER);
    // mem usage line
    int mem_w = (prof->w * prof->mem_current) / (PROF_MEM_TOTAL);
    bitmap_draw_hline(&screen, 0, prof->w, height - 1, 0);
    bitmap_draw_hline(&screen, 0, mem_w, height - 1, COLOUR_MEM);
}
