#pragma once

// Platform layer compile-time options

// Screen width (pixels)
#ifndef SCREEN_WIDTH
#define SCREEN_WIDTH (128)
#endif
// Screen height (pixels)
#ifndef SCREEN_HEIGHT
#define SCREEN_HEIGHT (128)
#endif
// Frame rate (frames / second)
#ifndef TARGET_FRAMERATE
#define TARGET_FRAMERATE (60)
#endif
// Audio sample rate (samples / second)
#ifndef AUDIO_SAMPLE_RATE
#define AUDIO_SAMPLE_RATE (8000)
#endif

// PC PLATFORM OPTIONS

// Display scaling factor
#ifndef DEFAULT_PIXEL_SCALE
#define DEFAULT_PIXEL_SCALE (3)
#endif

// GIF recording scaling factor
#ifndef DEFAULT_GIF_SCALE
#define DEFAULT_GIF_SCALE (1)
#endif
