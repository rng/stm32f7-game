#include "engine/asset.h"
#include "engine/platform.h"

#define ASSET_MAGIC (('G' << 0) | ('a' << 8) | ('m' << 16) | ('e' << 24))

typedef enum {
    ASSET_TYPE_NONE = 0,
    ASSET_TYPE_IMAGE,
    ASSET_TYPE_TILEMAP,
    ASSET_TYPE_PALETTE,
    ASSET_TYPE_WAVE,
} asset_type_t;

typedef struct {
    uint32_t magic;
    uint32_t version;
    uint32_t type;
} asset_header_t;

typedef struct {
    uint32_t width, height;
    uint32_t tile_width, tile_height;
    uint32_t format;
} asset_image_header_t;

typedef struct {
    uint32_t width, height, tilesize, layercount, etypecount, entcount;
} asset_tilemap_header_t;

typedef struct {
    uint32_t channel_count, sample_count;
} asset_sample_header_t;

static platform_handle_t asset_open(const char *name, asset_header_t *header, asset_type_t typ) {
    platform_handle_t f = platform->file_open(name, false);
    ASSERT_MSG(f, "Asset '%s' not found", name);
    platform->file_read(f, sizeof(asset_header_t), header);
    ASSERT_MSG(header->magic == ASSET_MAGIC, "Asset '%s' magic incorrect", name);
    ASSERT_MSG(header->type == typ, "Asset '%s' type incorrect", name);
    return f;
}

void asset_load_image(const char *name, bitmap_t *image) {
    asset_header_t header = {};
    asset_image_header_t imghdr = {};

    platform_handle_t f = asset_open(name, &header, ASSET_TYPE_IMAGE);
    platform->file_read(f, sizeof(imghdr), &imghdr);
    image->width = imghdr.width;
    image->height = imghdr.height;
    image->tile_width = imghdr.tile_width;
    image->tile_height = imghdr.tile_height;
    image->format = (bitmap_format_t)imghdr.format;
    uint32_t bpp = bitmap_format_bytes_per_pixel(image->format);
    ASSERT_MSG(imghdr.width <= 256 && imghdr.height <= 256, "Image asset too large");
    ASSERT_MSG(imghdr.tile_width <= imghdr.width && imghdr.tile_height <= imghdr.height,
               "Image tile size too large");
    uint32_t bytesize = imghdr.width * imghdr.height * bpp;
    image->data = (uint8_t *)platform->alloc(bytesize);
    platform->file_read(f, bytesize, image->data);
    platform->file_close(f);
}

void asset_load_palette(const char *name, palette_t *palette) {
    asset_header_t header = {};
    uint32_t colcount = 0;

    platform_handle_t f = asset_open(name, &header, ASSET_TYPE_PALETTE);
    platform->file_read(f, sizeof(colcount), &colcount);
    palette->count = colcount;
    ASSERT_MSG(palette->count <= 256, "Palette too large");
    uint32_t bytesize = palette->count * 3;
    palette->colours = (colour_t *)platform->alloc(bytesize);
    platform->file_read(f, bytesize, palette->colours);
    platform->file_close(f);
}

void asset_free_palette(palette_t *palette) {
    platform->free(palette->colours);
}

void asset_load_tilemap(const char *name, tilemap_t *map, entitylist_t *entlist) {
    asset_header_t header = {};
    asset_tilemap_header_t maphdr = {};

    platform_handle_t f = asset_open(name, &header, ASSET_TYPE_TILEMAP);
    platform->file_read(f, sizeof(maphdr), &maphdr);
    map->width = maphdr.width;
    map->height = maphdr.height;
    map->tilesize = maphdr.tilesize;
    map->layercount = maphdr.layercount;
    // layers
    ASSERT_MSG(map->layercount <= TILEMAP_MAX_LAYERS, "Map has too many layers");
    for (uint32_t i = 0; i < map->layercount; i++) {
        size_t bytesize = map->width * map->height;
        map->layers[i].data = (uint8_t *)platform->alloc(bytesize);
        platform->file_read(f, bytesize, map->layers[i].data);
    }

    entlist->etypecount = maphdr.etypecount;
    entlist->entcount = maphdr.entcount;
    // entity types
    entlist->enttypes = (entname_t *)platform->alloc(entlist->etypecount * sizeof(entname_t));
    platform->file_read(f, entlist->etypecount * sizeof(entname_t), entlist->enttypes);
    // entities
    entlist->entities = (entinstance_t *)platform->alloc(entlist->entcount * sizeof(entinstance_t));
    for (uint32_t i = 0; i < entlist->entcount; i++) {
        entinstance_t *inst = entlist->entities + i;
        platform->file_read(f, 10, inst);
        if (inst->propcount) {
            inst->props = (entprop_t *)platform->alloc(inst->propcount * sizeof(entprop_t));
            platform->file_read(f, inst->propcount * sizeof(entprop_t), inst->props);
        }
    }
    platform->file_close(f);
}

void asset_load_sample(const char *name, sound_sample_t *sample) {
    asset_header_t header = {};
    asset_sample_header_t smphdr = {};

    platform_handle_t f = asset_open(name, &header, ASSET_TYPE_WAVE);
    platform->file_read(f, sizeof(smphdr), &smphdr);
    sample->channel_count = smphdr.channel_count;
    sample->sample_count = smphdr.sample_count;
    uint32_t bytesize = smphdr.sample_count * smphdr.channel_count * 2;
    ASSERT_MSG(sample->sample_count <= 8 * 1024, "Sound sample too large");
    sample->data = (int16_t *)platform->alloc(bytesize);
    platform->file_read(f, bytesize, sample->data);
    platform->file_close(f);
}
