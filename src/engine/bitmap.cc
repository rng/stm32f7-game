#include <stdbool.h>
#include <string.h>

#include "engine/bitmap.h"
#include "engine/common.h"

#ifdef PLAT_HAVE_FAST_BLIT
#include "fast_blit.h"
#endif

// generic fallback (non-optimised) blitter code used below
#define BLITTER_FALLBACK(PIX_BLIT_OP)         \
    const int dnext = dest->width - width;    \
    if (hflip) {                              \
        /* horizontal flip */                 \
        const int snext = src->width + width; \
        for (int j = 0; j < height; j++) {    \
            for (int i = 0; i < width; i++) { \
                sp--;                         \
                PIX_BLIT_OP                   \
                dp++;                         \
            }                                 \
            dp += dnext;                      \
            sp += snext;                      \
        }                                     \
    } else {                                  \
        /* normal orientation */              \
        const int snext = src->width - width; \
        for (int j = 0; j < height; j++) {    \
            for (int i = 0; i < width; i++) { \
                PIX_BLIT_OP                   \
                dp++;                         \
                sp++;                         \
            }                                 \
            dp += dnext;                      \
            sp += snext;                      \
        }                                     \
    }

// clipping blit PAL8 -> PAL8
static void bitmap_blit_pal8(bitmap_t *dest, bitmap_t *src, int dx, int dy, int sx, int sy, int w,
                             int h, bool hflip) {
    // clip
    int starti = MAX(0, -dx);
    int startj = MAX(0, -dy);
    int endi = MIN(w, dest->width - dx);
    int endj = MIN(h, dest->height - dy);
    const int width = endi - starti;
    const int height = endj - startj;

    if ((width <= 0) || (height <= 0)) {
        return;
    }

    ASSERT(dest->width >= width);
    ASSERT(src->width >= width);
    uint8_t *dp = ((uint8_t *)dest->data) + ((dy + startj) * dest->width) + dx + starti;
    uint8_t *sp;
    if (hflip) {
        sp = ((uint8_t *)src->data) + ((sy + startj) * src->width) + sx + w - starti;
    } else {
        sp = ((uint8_t *)src->data) + ((sy + startj) * src->width) + sx + starti;
    }

#ifdef PLAT_HAVE_FAST_BLIT
    bitmap_fast_blit_pal8(dp, sp, width, height, dest->width, src->width, hflip);
#else
    BLITTER_FALLBACK(if (*sp != 255) { *dp = *sp; });
#endif
}

// clipping colour mask blit PAL8 -> PAL8
static void bitmap_blit_col_pal8(bitmap_t *dest, bitmap_t *src, int dx, int dy, int sx, int sy,
                                 int w, int h, bool hflip, uint32_t col) {
    // clip
    int starti = MAX(0, -dx);
    int startj = MAX(0, -dy);
    int endi = MIN(w, dest->width - dx);
    int endj = MIN(h, dest->height - dy);
    const int width = endi - starti;
    const int height = endj - startj;

    if ((width <= 0) || (height <= 0)) {
        return;
    }

    ASSERT(dest->width >= width);
    ASSERT(src->width >= width);
    uint8_t *dp = ((uint8_t *)dest->data) + ((dy + startj) * dest->width) + dx + starti;
    uint8_t *sp;
    if (hflip) {
        sp = ((uint8_t *)src->data) + ((sy + startj) * src->width) + sx + w - starti;
    } else {
        sp = ((uint8_t *)src->data) + ((sy + startj) * src->width) + sx + starti;
    }

#ifdef PLAT_HAVE_FAST_BLIT
    bitmap_fast_blit_col_pal8(dp, sp, width, height, dest->width, src->width, hflip, col);
#else
    BLITTER_FALLBACK(if (*sp != 255) { *dp = col; });
#endif
}

void bitmap_blit(bitmap_t *dest, bitmap_t *src, int dx, int dy, int sx, int sy, int w, int h,
                 bool hflip) {
    ASSERT(dest->format == BITMAP_FORMAT_PAL8);
    ASSERT(src->format == BITMAP_FORMAT_PAL8);

    bitmap_blit_pal8(dest, src, dx, dy, sx, sy, w, h, hflip);
}

void bitmap_blit_col(bitmap_t *dest, bitmap_t *src, int dx, int dy, int sx, int sy, int w, int h,
                     bool hflip, uint32_t col) {
    ASSERT(dest->format == BITMAP_FORMAT_PAL8);
    ASSERT(src->format == BITMAP_FORMAT_PAL8);

    bitmap_blit_col_pal8(dest, src, dx, dy, sx, sy, w, h, hflip, col);
}

void bitmap_blit_full(bitmap_t *dest, bitmap_t *src, int dx, int dy) {
    ASSERT(dest->format == BITMAP_FORMAT_PAL8);
    ASSERT(src->format == BITMAP_FORMAT_PAL8);

    bitmap_blit_pal8(dest, src, dx, dy, 0, 0, src->width, src->height, false);
}

void bitmap_blit_tile(bitmap_t *dest, bitmap_t *src, int dx, int dy, bool hflip, int tile) {
    const int tiles_per_row = src->width / src->tile_width;
    int sx = (tile % tiles_per_row) * src->tile_width;
    int sy = (tile / tiles_per_row) * src->tile_height;
    bitmap_blit(dest, src, dx, dy, sx, sy, src->tile_width, src->tile_height, hflip);
}

void bitmap_scale(bitmap_t *dest, bitmap_t *src, int scale) {
    uint8_t *dp = dest->data;
    ASSERT(dest->width == (src->width * scale));
    ASSERT(dest->height == (src->height * scale));
    // TODO: optimise
    for (int j = 0; j < src->height; j++) {
        for (int rowdup = 0; rowdup < scale; rowdup++) {
            for (int i = 0; i < src->width; i++) {
                uint8_t pix = *PIXEL_PTR(src, i, j, 1);
                for (int coldup = 0; coldup < scale; coldup++) {
                    *dp++ = pix;
                }
            }
        }
    }
}

void bitmap_fill(bitmap_t *dest, uint32_t colour) {
    switch (bitmap_format_bytes_per_pixel(dest->format)) {
        case 1:
            memset(dest->data, (uint8_t)colour, dest->width * dest->height);
            break;
        default:
            ASSERT(false);
    }
}

void bitmap_draw_vline(bitmap_t *dest, int x, int y1, int y2, uint32_t colour) {
    ASSERT(dest->format == BITMAP_FORMAT_PAL8);
    if (y1 > y2) {
        SWAP(y1, y2);
    }
    if (x < dest->width && x >= 0) {
        y1 = CLAMP(y1, 0, dest->height);
        y2 = CLAMP(y2, 0, dest->height);
        for (int y = y1; y < y2; y++) {
            *PIXEL_PTR(dest, x, y, 1) = colour;
        }
    }
}

void bitmap_draw_hline(bitmap_t *dest, int x1, int x2, int y, uint32_t colour) {
    ASSERT(dest->format == BITMAP_FORMAT_PAL8);
    if (x1 > x2) {
        SWAP(x1, x2);
    }
    if (y < dest->height && y >= 0) {
        x1 = CLAMP(x1, 0, dest->width);
        x2 = CLAMP(x2, 0, dest->width);
        for (int x = x1; x < x2; x++) {
            *PIXEL_PTR(dest, x, y, 1) = colour;
        }
    }
}

void bitmap_draw_line(bitmap_t *dest, int x1, int y1, int x2, int y2, uint32_t colour) {
    ASSERT(dest->format == BITMAP_FORMAT_PAL8);
    if (x1 == x2) {
        bitmap_draw_vline(dest, x1, y1, y2, colour);
    } else if (y1 == y2) {
        bitmap_draw_hline(dest, x1, x2, y1, colour);
    } else {
        float deltax = ABS(x2 - x1);
        float deltay = -ABS(y2 - y1);
        int sx = SIGN(x2 - x1);
        int sy = SIGN(y2 - y1);
        float error = deltax + deltay;
        uint8_t pattern = 0xFF; // 88;
        int pix = 0;
        while (1) {
            if (x1 >= 0 && x1 < dest->width && y1 >= 0 && y1 < dest->height) {
                if (pattern & (1 << pix)) {
                    *PIXEL_PTR(dest, x1, y1, 1) = colour;
                }
                pix = (pix + 1) % 8;
            }
            if (x1 == x2 && y1 == y2)
                break;
            float e2 = 2 * error;
            if (e2 >= deltay) {
                error += deltay;
                x1 += sx;
            }
            if (e2 <= deltax) {
                error += deltax;
                y1 += sy;
            }
        }
    }
}

void bitmap_draw_rect(bitmap_t *dest, int x1, int y1, int x2, int y2, uint32_t colour) {
    bitmap_draw_vline(dest, x1, y1, y2, colour);
    bitmap_draw_vline(dest, x2 - 1, y1, y2, colour);
    bitmap_draw_hline(dest, x1, x2, y1, colour);
    bitmap_draw_hline(dest, x1, x2, y2 - 1, colour);
}

void bitmap_draw_fillrect(bitmap_t *dest, int x1, int y1, int x2, int y2, uint32_t colour) {
    for (int y = y1; y < y2; y++) {
        bitmap_draw_hline(dest, x1, x2, y, colour);
    }
}

uint32_t bitmap_format_bytes_per_pixel(bitmap_format_t format) {
    switch (format) {
        case BITMAP_FORMAT_PAL8:
            return 1;
        default:
            ASSERT(false);
    }
}
