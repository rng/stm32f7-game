#pragma once

#include "engine/bitmap.h"
#include "engine/common.h"
#include "engine/entitymgr.h"
#include "engine/palette.h"
#include "engine/sound.h"
#include "engine/tilemap.h"

void asset_load_image(const char *name, bitmap_t *image);
void asset_load_palette(const char *name, palette_t *palette);
void asset_free_palette(palette_t *palette);
void asset_load_tilemap(const char *name, tilemap_t *map, entitylist_t *entlist);
void asset_load_sample(const char *name, sound_sample_t *sample);
