#include "engine/entitymgr.h"
#include "engine/debug.h"

#include <string.h>

static void entitymgr_process_free(entitymgr_t *em) {
    entity_t *ent = em->freelist_next;
    while (ent) {
        entity_t *next = ent->next;
        platform->free(ent);
        ent = next;
    }
    em->freelist_next = em->freelist;
    em->freelist = NULL;
}

void entitymgr_register_types(entitymgr_t *em, const entity_type_t *const types[], size_t count) {
    em->types = types;
    em->type_count = count;
}

void entitymgr_reset(entitymgr_t *em) {
    entity_t *e = em->entities;
    while (e) {
        entity_t *next = e->next;
        entitymgr_remove(em, e);
        e = next;
    }
    // call process free twice to flush the double buffer
    entitymgr_process_free(em);
    entitymgr_process_free(em);
}

void entitymgr_tick(entitymgr_t *em, input_state_t *input, void *ctx) {
    entity_t *ent = em->entities;
    while (ent) {
        // The entity might destroy itself as part of the tick, so save the
        // pointer to the next entity before we call tick.
        entity_t *next = ent->next;
        if (ent->type->tick) {
            ent->type->tick(ent, em, input, ctx);
        }
        ent = next;
    }
    entitymgr_process_free(em);
}

entity_t *entitymgr_new(entitymgr_t *em, size_t alloc_size, v2f pos, rect_t &bounds,
                        const entity_type_t *typ, int id) {
    ASSERT(alloc_size >= sizeof(entity_t));
    entity_t *e = (entity_t *)platform->alloc(alloc_size);
    e->pos = pos;
    e->bounds = bounds;
    e->id = id;
    e->type = typ;
    e->next = em->entities;
    em->entities = e;
    return e;
}

void entitymgr_remove(entitymgr_t *em, entity_t *ent) {
    // Since our entity structures don't have pointers to the previous entity,
    // search through the entity list to find it.
    entity_t *e = em->entities;
    entity_t *prev = NULL;
    while (e) {
        if (e == ent) {
            break;
        }
        prev = e;
        e = e->next;
    }
    ASSERT_MSG(e, "removal of unknown entity");
    if (prev) {
        prev->next = ent->next;
    } else {
        em->entities = ent->next;
    }
    ent->dead = true;
    ent->next = em->freelist;
    em->freelist = ent;
}

static const entity_type_t *entitymgr_type_from_name(entitymgr_t *em, char *name) {
    for (size_t i = 0; i < em->type_count; i++) {
        if (strcmp(name, em->types[i]->name) == 0) {
            return em->types[i];
        }
    }
    return NULL;
}

void entitymgr_spawnlist(entitymgr_t *em, entitylist_t *entlist) {
    for (size_t i = 0; i < entlist->entcount; i++) {
        entinstance_t *inst = entlist->entities + i;
        ASSERT(inst->nameid < entlist->etypecount);
        char *entname = entlist->enttypes[inst->nameid].v;
        const entity_type_t *enttype = entitymgr_type_from_name(em, entname);
        if (enttype) {
            entity_props_t props = {inst->propcount, inst->props};
            enttype->spawn(em, {(float)inst->x, (float)inst->y}, &props, inst->entid);
        } else {
            DEBUG_LOG("unhandled spawn %-10s : %3d,%3d\n", entname, inst->x, inst->y);
        }
    }
}

// Searching / iterating

entity_t *entitymgr_find_by_id(entitymgr_t *em, int id) {
    entity_t *e = em->entities;
    while (e) {
        if (e->id == id) {
            return e;
        }
        e = e->next;
    }
    return NULL;
}

entity_t *entitymgr_find_by_flag(entitymgr_t *em, int flag, entity_t *start) {
    entity_t *e = start ? start : em->entities;
    while (e) {
        if (ENTITY_FLAGGED(e, flag)) {
            return e;
        }
        e = e->next;
    }
    return NULL;
}

entity_t *entitymgr_iter_next(entitymgr_iter_t &iter, rect_t &bounds) {
    entity_t *e = iter.cur;
    while (e) {
        if (ENTITY_FLAGGED(e, ENTITY_FLAG_COLLIDE)) {
            rect_t e_bounds = e->bounds.translate(e->pos.x, e->pos.y);
            if (bounds.overlap(e_bounds)) {
                iter.cur = e->next;
                return e;
            }
        }
        e = e->next;
    }
    return NULL;
}

// Properties

static uint32_t hash_propname(const char *propname) {
    // 32 bit FNV hash (https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function)
    uint32_t h = 0x811c9dc5;
    while (*propname) {
        char ch = *propname++;
        h = (h * 16777619);
        h = h ^ ch;
    }
    return h;
}

uint32_t entity_props_t::get(const char *propname, uint32_t default_val) {
    uint32_t id = hash_propname(propname);
    for (uint32_t i = 0; i < this->count; i++) {
        if (this->p[i].key == id) {
            return this->p[i].val;
        }
    }
    return default_val;
}
