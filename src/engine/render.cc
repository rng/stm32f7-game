#include <engine/asset.h>
#include <engine/common.h>
#include <engine/platform.h>

#include "render.h"

#include <string.h>

#define RENDER_MAX_OPS (384)

typedef struct _render_op_t {
    uint8_t op : 4;
    uint8_t z : 4;
    bool screen;
    union {
        struct {
            int16_t x, y;
            render_sprite_t sprite;
            uint8_t tile;
            bool hflip;
        } s;
        struct {
            int16_t x1, y1, x2, y2;
            uint16_t colour;
        } rl;
        struct {
            tilemap_t *tiles;
            uint8_t layer;
            v2f parallax;
        } tm;
    };
} render_op_t;

enum {
    RENDER_OP_SPRITE,
    RENDER_OP_RECT,
    RENDER_OP_LINE,
    RENDER_OP_TILES,
};

void render_init(render_t *render, size_t max_sprites) {
    render->sprites = (bitmap_t *)platform->alloc(sizeof(bitmap_t) * max_sprites);
    render->max_sprites = max_sprites;
    render->ops = (render_op_t *)platform->alloc(sizeof(render_op_t) * RENDER_MAX_OPS);
    render->op_count = 0;
}

void render_term(render_t *render) {
    for (size_t i = 0; i < render->max_sprites; i++) {
        if (render->sprites[i].data) {
            platform->free(render->sprites[i].data);
        }
    }
    platform->free(render->sprites);
    platform->free(render->ops);
}

static void render_draw_sprite(render_t *render, bitmap_t *framebuffer, rect_t *view,
                               render_op_t *op) {
    bitmap_t *sprite = &render->sprites[op->s.sprite];
    if (op->screen) {
        bitmap_blit_tile(framebuffer, sprite, op->s.x, op->s.y, op->s.hflip, op->s.tile);
    } else {
        rect_t v = view->expand(-sprite->tile_width, -sprite->tile_height);
        if (v.inside(op->s.x, op->s.y)) {
            int px = op->s.x - view->x;
            int py = op->s.y - view->y;
            bitmap_blit_tile(framebuffer, sprite, px, py, op->s.hflip, op->s.tile);
#ifdef DEBUG_DRAW_SPRITE_BOUNDS
            bitmap_draw_rect(framebuffer, px, py, px + sprite->tile_width, py + sprite->tile_height,
                             11);
#endif
        }
    }
}

static void render_draw_rect(render_t *render, bitmap_t *framebuffer, rect_t *view,
                             render_op_t *op) {
    int x1 = op->rl.x1 - view->x;
    int y1 = op->rl.y1 - view->y;
    int x2 = op->rl.x2 - view->x;
    int y2 = op->rl.y2 - view->y;
    bitmap_draw_rect(framebuffer, x1, y1, x2, y2, op->rl.colour);
}

static void render_draw_line(render_t *render, bitmap_t *framebuffer, rect_t *view,
                             render_op_t *op) {
    int x1 = op->rl.x1, x2 = op->rl.x2, y1 = op->rl.y1, y2 = op->rl.y2;
    if (!op->screen) {
        x1 -= view->x;
        y1 -= view->y;
        x2 -= view->x;
        y2 -= view->y;
    }
    bitmap_draw_line(framebuffer, x1, y1, x2, y2, op->rl.colour);
}

static void render_draw_tiles(render_t *render, bitmap_t *framebuffer, rect_t *view,
                              render_op_t *op) {
    rect_t v = *view;
    if (op->tm.parallax.x || op->tm.parallax.y) {
        v2f world_sz = v2f(op->tm.tiles->width * op->tm.tiles->tilesize,
                           op->tm.tiles->height * op->tm.tiles->tilesize);
        v2f par_centre = world_sz / 2;
        v2f cam_pos = v2f(v.x + v.w / 2, v.y + v.h / 2);
        v2f offset = (par_centre - cam_pos);
        v.x += offset.x / op->tm.parallax.x;
        v.y += offset.y / op->tm.parallax.y;
    }
    tilemap_render(op->tm.tiles, framebuffer, op->tm.layer, v.x, v.y);
}

static int render_op_compare(const void *va, const void *vb) {
    const render_op_t *a = (const render_op_t *)va;
    const render_op_t *b = (const render_op_t *)vb;
    return a->z - b->z;
}

void render_draw(render_t *render, bitmap_t *framebuffer, rect_t *view) {
    qsort(render->ops, render->op_count, sizeof(render_op_t), render_op_compare);
    for (size_t i = 0; i < render->op_count; i++) {
        render_op_t *op = &render->ops[i];
        switch (op->op) {
            case RENDER_OP_SPRITE:
                render_draw_sprite(render, framebuffer, view, op);
                break;
            case RENDER_OP_RECT:
                render_draw_rect(render, framebuffer, view, op);
                break;
            case RENDER_OP_LINE:
                render_draw_line(render, framebuffer, view, op);
                break;
            case RENDER_OP_TILES:
                render_draw_tiles(render, framebuffer, view, op);
                break;
            default:
                ASSERT(false);
        }
    }
    render->op_count = 0;
}

void render_load_sprite(render_t *render, render_sprite_t id, const char *filename) {
    ASSERT(id < render->max_sprites);
    asset_load_image(filename, &render->sprites[id]);
}

void render_push_sprite_w(render_t *render, render_sprite_t sprite, v2f pos, uint8_t z, bool hflip,
                          uint8_t tile) {
    ASSERT(render->op_count < RENDER_MAX_OPS);
    render_op_t rop = {
        RENDER_OP_SPRITE, z, false, {.s = {(int16_t)pos.x, (int16_t)pos.y, sprite, tile, hflip}}};
    render->ops[render->op_count++] = rop;
}

void render_push_sprite_s(render_t *render, render_sprite_t sprite, v2f pos, uint8_t z, bool hflip,
                          uint8_t tile) {
    ASSERT(render->op_count < RENDER_MAX_OPS);
    render_op_t rop = {
        RENDER_OP_SPRITE, z, true, {.s = {(int16_t)pos.x, (int16_t)pos.y, sprite, tile, hflip}}};
    render->ops[render->op_count++] = rop;
}

void render_push_rect_s(render_t *render, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t z,
                        uint16_t colour) {
    ASSERT(render->op_count < RENDER_MAX_OPS);
    render_op_t rop = {RENDER_OP_RECT, z, true, {.rl = {x1, y1, x2, y2, colour}}};
    render->ops[render->op_count++] = rop;
}

void render_push_line_w(render_t *render, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t z,
                        uint16_t colour) {
    ASSERT(render->op_count < RENDER_MAX_OPS);
    render_op_t rop = {RENDER_OP_LINE, z, false, {.rl = {x1, y1, x2, y2, colour}}};
    render->ops[render->op_count++] = rop;
}

void render_push_tiles_w(render_t *render, tilemap_t *map, uint8_t layer, uint8_t z) {
    ASSERT(render->op_count < RENDER_MAX_OPS);
    render_op_t rop = {RENDER_OP_TILES, z, false, {.tm = {map, layer, v2f(0, 0)}}};
    render->ops[render->op_count++] = rop;
}

void render_push_tiles_parallax_w(render_t *render, tilemap_t *map, uint8_t layer, uint8_t z,
                                  v2f factor) {
    ASSERT(render->op_count < RENDER_MAX_OPS);
    render_op_t rop = {RENDER_OP_TILES, z, false, {.tm = {map, layer, factor}}};
    render->ops[render->op_count++] = rop;
}
