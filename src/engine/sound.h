#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

// Audio playback and mixer system.

// Max number of independent channels of audio that can be played back at once.
enum {
    SOUND_DEFAULT_CHANNELS = 4,
};

// Note values (MIDI numbering)
enum sound_note_t {
    SOUND_NOTE_C1 = 24,
    SOUND_NOTE_C2 = 36,
    SOUND_NOTE_C3 = 48,
    SOUND_NOTE_C4 = 60,
    SOUND_NOTE_C5 = 72,
    SOUND_NOTE_C6 = 84,
    SOUND_NOTE_C7 = 96,
    SOUND_NOTE_C8 = 108,
    SOUND_NOTE_C9 = 120,
};

// Instrument waveform types
enum sound_waveform_t {
    SOUND_WAVEFORM_PCM = 0, // PCM audio sample
    SOUND_WAVEFORM_SIN,     // Sine
    SOUND_WAVEFORM_ORG,     // "Organ" (Sine with 2nd and 4th harmonics)
    SOUND_WAVEFORM_SQR,     // Square
    SOUND_WAVEFORM_SAW,     // Sawtooth
    SOUND_WAVEFORM_NSE,     // Noise
};

// PCM sample data
struct sound_sample_t {
    uint32_t channel_count;
    uint32_t sample_count;
    int16_t *data;
};

// A unique id used to reference instrument in the mixer
typedef uint16_t sound_instrument_id_t;
struct sound_instrument_t;

// Private types
struct sound_instrument_t;
struct sound_channel_t;

// Mixer state
struct sound_mixer_t {
    float sample_rate;
    sound_channel_t *channels;
    sound_instrument_t *instruments;
    uint16_t max_instruments, max_channels;
};

// Initialise the mixer, allocating space for `max_instruments` number of instruments.
// `max_channels` defines the max number of instruments playing at once.
void sound_init(sound_mixer_t *mixer, uint32_t sample_rate, size_t max_instruments,
                size_t max_channels = SOUND_DEFAULT_CHANNELS);

// Terminate the mixer, freeing allocated memory
void sound_term(sound_mixer_t *mixer);

// Configure an instument using audio data from an asset file
void sound_instrument_pcm(sound_mixer_t *mixer, sound_instrument_id_t id, const char *filename);

// Configure an instrument using a (non-sample) waveform type and set its syntheisis parameters
void sound_instrument_synth(sound_mixer_t *mixer, sound_instrument_id_t id, sound_waveform_t type,
                            uint16_t attack, uint16_t decay, int16_t freq_slide = 0);

// Start playing a specified note at a given volume. The mixer will try to find an unused channel
// on which to play using the given instrument. Returns index of the channel used to play the note
// or -1 if no channels are available.
int sound_play_note(sound_mixer_t *mixer, sound_instrument_id_t id,
                    sound_note_t note = SOUND_NOTE_C4, uint8_t volume = 0xff);

// Stop the note playing on the given channel.
void sound_stop_channel(sound_mixer_t *mixer, int channel_idx);

// Mix all active channels into the provided audio buffer. Should be called as
// part of the platform audio callback.
void sound_mix(sound_mixer_t *mixer, void *buf, size_t buf_size);
