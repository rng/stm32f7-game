#pragma once

#include <stdbool.h>

typedef struct {
    float t;
} timeout_t;

static inline void timeout_init(timeout_t *timeout, float reset) {
    timeout->t = reset;
}

static inline bool timeout_expired(timeout_t *timeout, float dt, float reset) {
    timeout->t -= dt;
    if (timeout->t <= 0) {
        timeout->t = reset;
        return true;
    }
    return false;
}
