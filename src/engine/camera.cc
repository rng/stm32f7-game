#include "engine/camera.h"

void camera_init(camera_t &cam, v2i view_size, v2i world_size, float trackrate) {
    cam.pos = {0, 0};
    cam.view_size = view_size;
    cam.world_size = world_size;
    cam.trackrate = trackrate;
    cam.shake = 0;
}

void camera_set_pos(camera_t &cam, v2f pos) {
    cam.pos = pos - v2f(cam.view_size.x / 2, cam.view_size.y / 2);
}

rect_t camera_update(camera_t &cam, v2f target, float dt) {
    v2f cam_target = target - v2f(cam.view_size.x / 2, cam.view_size.y / 2);
    v2f cam_error = cam_target - cam.pos;
    v2f cam_vel = cam_error * (cam.trackrate * dt);
    cam.pos += cam_vel;
    cam.pos.x = cam_target.x; // FIXME: hack

    // camera shake
    if (cam.shake > 5) {
        cam.shake -= 50 * dt;
    } else if (cam.shake > 0) {
        cam.shake -= 25 * dt;
    } else {
        cam.shake = 0;
    }
    v2f shake_noise = {RANDFLOAT(-1, 1), RANDFLOAT(-1, 1)};
    cam.pos += shake_noise * cam.shake;

    // clipping
    v2f cam_clip = v2f(cam.world_size.x - cam.view_size.x, cam.world_size.y - cam.view_size.y);
    cam.pos = cam.pos.clamp(0, cam_clip.x, 0, cam_clip.y);
    return rect_t(cam.pos.x, cam.pos.y, cam.view_size.x, cam.view_size.y);
}
