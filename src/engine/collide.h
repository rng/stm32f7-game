#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "engine/rect.h"
#include "engine/vector.h"

// Tile types - game code can define user defined types > 2
enum {
    COLLIDE_TILE_IMPASSABLE = 0,
    COLLIDE_TILE_PASSABLE = 1,
    COLLIDE_TILE_ONEWAY = 2,
};

// Collision system with support for testing for collisions between rectangular
// axis-aligned objects against tile maps.

// Callback type for collision checks. Called with collide_conf_t.context and
// tile to check. Should return 0 for impassable tiles or invalid tile
// coordinates and > 0 for passable tiles.
typedef int (*collide_tile_fn_t)(void *ctx, int tx, int ty);

// Configuration for a collision check. Defines parameters for collision system.
typedef struct {
    uint32_t tilesize;      // size of the tiles in the tilemap tiles
    collide_tile_fn_t tile; // callback to check type of tilemap tile
    void *context;          // user provided context to pass to the passable callback
} collide_conf_t;

// Collision check result.
typedef struct {
    float rx, ry;      // result position (pixels)
    int tx, ty;        // result position (tiles)
    bool collx, colly; // true if we collided in either/both axes
} collide_res_t;

// Perform collision check.
// Checks motion of an object starting at `pos` to `pos+vel` of dimensions `size`.
// Tilemap details are specified in the `config`. `pos` is updated with final position.
collide_res_t collide_tilemap_check(collide_conf_t *config, v2f *pos, v2f vel, rect_t &bounds);

// Checks if a given `bounds` rectangle touches a tile of a given type (as returned by
// `collide_conf_t.tile`)
bool collide_tilemap_touching_tile(collide_conf_t *config, rect_t *bounds, int tile_type);
