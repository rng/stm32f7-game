#pragma once

#include <stddef.h>
#include <stdint.h>

#include "engine/input.h"

// Platform parameters passed from platform to game code
typedef struct {
    uint32_t screen_w, screen_h;
    uint32_t audio_rate;
} platform_params_t;

// Asset handle used by platform_api_t file functions
typedef void *platform_handle_t;

// Platform API: Interface to platform from game code.

typedef struct {
    // MEMORY
    // Allocate a block of `size` bytes and return a pointer to it
    void *(*alloc)(size_t size);
    // Free a block of memory allocated by `alloc`
    void (*free)(void *ptr);

    // FILE I/O
    // Open an file by name for reading or writing, return an file handle
    platform_handle_t (*file_open)(const char *name, bool write);
    // Read `n` bytes from a given file into `buf`. Return number of bytes read
    size_t (*file_read)(platform_handle_t h, size_t n, void *buf);
    // Write `n` bytes from `buf` into a given file. Return number of bytes written
    size_t (*file_write)(platform_handle_t h, size_t n, void *buf);
    // Close the file referenced by the given handle
    void (*file_close)(platform_handle_t h);
    // Return true if the named file exists
    bool (*file_exists)(const char *name);
    // Return the size of the given file in bytes
    size_t (*file_size)(platform_handle_t h);

    // GRAPHICS
    // Set colour palette used for display. `colours` should be an array of 3 byte RGB entries.
    // `count` is number of palette entries.
    void (*gfx_set_pallete)(uint8_t *colours, size_t count);

    // DEBUG
    // Return high resolution (usec) time (may not reflect "real" wall clock time)
    uint64_t (*debug_get_usec)(void);
    // Log a formatted string to the platform's debug output console
    void (*debug_log_printf)(const char *fmt, ...);
    // Log a timing block to the debug stream
    void (*debug_log_timed_block)(const char *name, uint64_t start, uint32_t delta);
} platform_api_t;

// Global instance of the platform struct to be defined in game code.
extern platform_api_t *platform;

// Game API: Interface to game from platform code.

struct game_state_t;

typedef struct {
    struct game_state_t *(*init)(platform_api_t *platform_api, platform_params_t *params);
    void (*term)(struct game_state_t *game);
    bool (*tick)(struct game_state_t *game, input_state_t *input, uint8_t *framebuffer);
    void (*audio)(struct game_state_t *game, void *audio_buf, size_t audio_buf_len);
} game_api_t;

typedef game_api_t (*get_game_api_fn_t)(void);

// Game Header: metadata for ARM versions of the game code binaries

typedef struct {
    uint32_t magic;
    uint32_t buildtime;
    char name[24];
} game_header_t;

#define STRINGIFY2(X) #X
#define STRINGIFY(X) STRINGIFY2(X)

#ifdef PLAT_STM
#define GAME_HEADER_MAGIC 0x656d6167
#define GAME_API_EXPORT                                                    \
    __attribute__((section(".game_header"))) game_header_t game_header = { \
        GAME_HEADER_MAGIC,                                                 \
        BUILD_TIME,                                                        \
        STRINGIFY(GAME_NAME),                                              \
    };                                                                     \
    __attribute__((section(".get_game_api")))
#else
#define GAME_API_EXPORT
#endif

// Private platform system API - not for use by games

game_api_t platform_sys_game_load(const char *filename);
void platform_sys_game_unload(void);
void platform_sys_games_list(const char *games[], size_t &games_max);
uint32_t platform_sys_battery_percent(void);
void platform_sys_set_default_palette(void);
void platform_sys_set_volume(uint8_t val);
void platform_sys_set_brightness(uint8_t val);
void platform_sys_set_card_presence(bool present);
void platform_sys_set_charging(bool charging);
void platform_sys_set_usb_connected(bool connected);
