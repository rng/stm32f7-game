#pragma once

#include "engine/platform.h"
#include "engine/rect.h"
#include "engine/vector.h"

// Basic game enitity management system with support for defining different
// entity types, creating, updating and destroying them.

struct entity_type_t;
struct entity_props_t;

// Base entity structure. Any user-defined entity should include an `entity_t`.
// Note the `entity_t` field MUST be the first field in any entity, eg:
//
// struct my_entity_t {
//     entity_t e;
//     int life;
//     ...
// }
struct entity_t {
    struct entity_t *next;
    v2f pos;
    rect_t bounds;
    const struct entity_type_t *type;
    int id;
    bool dead;
};

// Entity Manager
struct entitymgr_t {
    const struct entity_type_t *const *types;
    size_t type_count;
    entity_t *entities;
    entity_t *freelist, *freelist_next;
};

// ENTITY TYPES

// Each entity has a `type` (like a class) that defines it's behaviour.

// An entity type has an associated set of flags. These flags allow filtering
// entities by their flags. For example, "find all entities that are flagged as enemies".

// Entity creation function type - called to create a new instance of an entity
// at a given position
typedef void (*entity_spawn_fn_t)(entitymgr_t *em, v2f pos, struct entity_props_t *props, int id);
// Entity tick function type - called on each frame to update entity state
typedef void (*entity_tick_fn_t)(void *entity, entitymgr_t *em, input_state_t *input, void *ctx);
// Entity event function type - called when an entity receives an event from another entity
typedef void (*entity_event_fn_t)(void *entity, int event, void *param, void *ctx);

struct entity_type_t {
    const char *name;
    entity_spawn_fn_t spawn;
    entity_tick_fn_t tick;
    entity_event_fn_t event;
    uint32_t flags;
};

enum {
    ENTITY_FLAG_NONE = 0,
    ENTITY_FLAG_COLLIDE = FLAG(0),
    ENTITY_FLAG_ALL = 0xFFFFFFFF,
};
#define ENTITY_FLAGGED(ENT, FLAG) (ENT->type->flags & (FLAG))

// Helper macro to define an entity type
#define DEF_ENTITY_TYPE(NAME, SPAWN_FN, TICK_FN, EVENT_FN, FLAGS)                              \
    static void SPAWN_FN(entitymgr_t *em, v2f pos, struct entity_props_t *props, int id);      \
    const entity_type_t NAME##_type = {#NAME, (entity_spawn_fn_t)SPAWN_FN,                     \
                                       (entity_tick_fn_t)TICK_FN, (entity_event_fn_t)EVENT_FN, \
                                       FLAGS}

// ENTITY LISTS

// Tilemaps include a set of entities for the level. These are represented as
// entitylists". Passing an entitylist to `entitymgr_spawnlist` will spawn
// all the entities defined in the map.

struct entname_t {
    char v[12];
};

struct entprop_t {
    uint32_t key, val;
};

struct entinstance_t {
    uint16_t propcount, nameid, entid, x, y;
    entprop_t *props;
};

struct entity_props_t {
    size_t count;
    entprop_t *p;

    uint32_t get(const char *propname, uint32_t default_val = 0);
};

struct entitylist_t {
    uint32_t etypecount;
    uint32_t entcount;
    entname_t *enttypes;
    entinstance_t *entities;
};

// ENTITY MANAGER CORE

// Register a list of entity types with the entitymgr. Needed for calling
// `entitymgr_spawnlist`.
void entitymgr_register_types(entitymgr_t *em, const entity_type_t *const types[], size_t count);

// Remove all entities from the entity manager.
void entitymgr_reset(entitymgr_t *em);

// Called by the game tick function to update all entities (by calling their
// tick functions). The `input` and `ctx` are passed down to the entity tick
// functions. The ctx can be used to pass in game specific state that entities
// need to access.
void entitymgr_tick(entitymgr_t *em, input_state_t *input, void *ctx);

// ENTITY CREATION / DELETION

// Allocate and initialise a new entity, adding it to the entity manager. The
// entity type's tick function will be called with `ent` on the next call to
// `entitymgr_tick`. `entitymgr_new` should be called by an entity_type's spawn
// function.
entity_t *entitymgr_new(entitymgr_t *em, size_t alloc_size, v2f pos, rect_t &bounds,
                        const entity_type_t *typ, int id = -1);
template <typename T>
T *entitymgr_new(entitymgr_t *em, v2f pos, rect_t &bounds, const entity_type_t *typ, int id = -1) {
    return (T *)entitymgr_new(em, sizeof(T), pos, bounds, typ, id);
}

// Remove an entity `ent` from the entity manager.
void entitymgr_remove(entitymgr_t *em, entity_t *ent);

// Spawn all entities from a given entitylist.
void entitymgr_spawnlist(entitymgr_t *em, entitylist_t *entlist);

// ENTITY SEARCH / ITERATION

// Iterate through all entities and return the entity with the given id. Return
// NULL if no entity with the given id exists.
entity_t *entitymgr_find_by_id(entitymgr_t *em, int id);

// Iterate through all entities and return the first entity with the given
// flag. Return NULL if no entity with the given id exists.
// If `start` is provided will begin searching after the given entity (to
// allow for finding multiple entities that match a given flag).
entity_t *entitymgr_find_by_flag(entitymgr_t *em, int flag, entity_t *start = NULL);

// private: get the next entity in the sequence of the given iterator
typedef struct {
    entity_t *cur;
} entitymgr_iter_t;
entity_t *entitymgr_iter_next(entitymgr_iter_t &iter, rect_t &bounds);

// Iterate through all entities and for each entity that overlaps with
// bounding box `bounds` and has `flags` execute the following { } block.
#define FOREACH_ENTITY_IN_BOUNDS(ENT, EM, BOUNDS, FLAGS)                   \
    entitymgr_iter_t iter = {(EM)->entities};                              \
    for (entity_t *ENT = NULL; (ENT = entitymgr_iter_next(iter, BOUNDS));) \
        if (ENTITY_FLAGGED(ENT, FLAGS))

// MISCELLANEOUS

// Return the bounding box for the given entity `e`.
static inline rect_t entity_bounds(entity_t *e) {
    return rect_t(e->pos.x + e->bounds.x, e->pos.y + e->bounds.y, e->bounds.w, e->bounds.h);
}

static inline v2f entity_center(entity_t *e) {
    return e->pos + v2f(e->bounds.w / 2, e->bounds.h / 2);
}

// Send event `msg` with optional parameter and context to the given entity `e`.
static inline void entity_send_event(entity_t *e, uint32_t msg, void *param = NULL,
                                     void *ctx = NULL) {
    ASSERT_MSG(e->type->event, "%s has no event handler", e->type->name);
    e->type->event(e, msg, param, ctx);
}
