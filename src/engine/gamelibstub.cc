#include "engine/platform.h"

extern struct game_state_t *game_init(platform_api_t *platform_api, platform_params_t *params);
extern void game_term(game_state_t *game);
extern bool game_tick(game_state_t *game, input_state_t *input, uint8_t *framebuffer);
extern void game_audio(game_state_t *game, void *audio_buf, size_t audio_buf_len);

extern "C" {
GAME_API_EXPORT
game_api_t get_game_api(void) {
    static const game_api_t game_api = {
        .init = game_init,
        .term = game_term,
        .tick = game_tick,
        .audio = game_audio,
    };
    return game_api;
}
}
