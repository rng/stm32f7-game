#pragma once

#include "engine/platform.h"

#define TOKENPASTE(x, y) x##y
#define TOKENPASTE2(x, y) TOKENPASTE(x, y)

// Engine debug API

#if DEBUG == 1

// Print a (printf-style) formatted debug message to the platform debug console
#define DEBUG_LOG(...) platform->debug_log_printf(__VA_ARGS__)

// Create a unique timer block for the enclosing lexical scope (eg time between { and }).
#define DEBUG_TIMED_BLOCK(name) debug_timed_block_t TOKENPASTE2(_debug_block, __LINE__)(name)

#else

#define DEBUG_LOG(...)
#define DEBUG_TIMED_BLOCK(block_name)

#endif

// Helper class for DEBUG_TIMED_BLOCK. Creates a lexically scoped timed block. On
// leaving a code bloc ( { foo .. } ), will call into the platform layer with the
// start time, time spent in the block and given block name

struct debug_timed_block_t {
    debug_timed_block_t(const char *_name) : name(_name) {
        start_usec = platform->debug_get_usec();
    }

    ~debug_timed_block_t() {
        uint64_t end_usec = platform->debug_get_usec();
        uint32_t delta_usec = end_usec - start_usec;
        platform->debug_log_timed_block(name, start_usec, delta_usec);
    }

    uint64_t start_usec;
    const char *name;
};
