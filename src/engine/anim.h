#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef struct {
    const uint16_t *frames;
    uint16_t framecount;
    bool loop;
} anim_seq_t;

typedef struct {
    float t;
    uint16_t frame;
    uint16_t index;
    uint16_t sequence;
    float frametime;
    anim_seq_t seq0;
    const anim_seq_t *seqs;
} anim_t;

void anim_init_sequence(anim_t *self, float frametime, const uint16_t *frames, uint32_t framecount,
                        bool loop);
void anim_init_sequences(anim_t *self, float frametime, const anim_seq_t *sequences);
void anim_play(anim_t *self, uint32_t sequence);
bool anim_update(anim_t *anim, float dt);
