#include <stdlib.h>

#include "engine/common.h"

#include "anim.h"

void anim_init_sequence(anim_t *self, float frametime, const uint16_t *frames, uint32_t framecount,
                        bool loop) {
    self->seq0.frames = frames;
    self->seq0.framecount = framecount;
    self->seq0.loop = loop;
    anim_init_sequences(self, frametime, &self->seq0);
}

void anim_init_sequences(anim_t *self, float frametime, const anim_seq_t *sequences) {
    self->t = 0;
    self->frame = 0;
    self->index = 0;
    self->sequence = 0;
    self->frametime = frametime;
    self->seqs = sequences;
}

void anim_play(anim_t *self, uint32_t sequence) {
    if (sequence != self->sequence) {
        self->index = 0;
        self->sequence = sequence;
        self->t = 0;
        self->frame = self->seqs[self->sequence].frames[0];
    }
}

bool anim_update(anim_t *self, float dt) {
    self->t += dt;
    const anim_seq_t *seq = self->seqs + self->sequence;
    self->frame = seq->frames[self->index];
    if (self->t > self->frametime) {
        self->t = 0;
        if (((self->index + 1) == seq->framecount) && !seq->loop) {
            return true;
        } else {
            self->index = (self->index + 1) % seq->framecount;
        }
    }
    return false;
}
