#pragma once

#include <stdbool.h>

#include "engine/vector.h"

#define BUTTON_NAME_A "Z"
#define BUTTON_NAME_B "X"

typedef struct {
    bool up, down, left, right, a, b, c, d, menu;
    float dt;
} input_state_t;
static_assert(sizeof(input_state_t) == 16, "Unexpected input state size");

// Given an input state, return a vector where x&y are [1,0,-1] depending on D-pad
// button state (eg up pressed = (0,-1), down and right pressed = (1,1)).
static inline v2i input_to_vector(input_state_t &input) {
    v2i r = {0, 0};
    if (input.up) {
        r.y = -1;
    } else if (input.down) {
        r.y = 1;
    }
    if (input.left) {
        r.x = -1;
    } else if (input.right) {
        r.x = 1;
    }
    return r;
}
