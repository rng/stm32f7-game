#pragma once

#include "engine/bitmap.h"
#include "engine/vector.h"

// Return builtin system font.
bitmap_t *font_system(void);

void font_print(bitmap_t *fb, bitmap_t *font, int x, int y, const char *text);
void font_printf(bitmap_t *fb, bitmap_t *font, int x, int y, const char *fmt, ...);

// Draw `text to bitmap `fb`, starting at pixel (x,y) using provided font.
// `font` must be a tileset bitmap where the tile size matches font character size.
// `col` is a colour to use in the correct format for the bitmap.
void font_print_col(bitmap_t *fb, bitmap_t *font, int x, int y, const char *text, uint32_t col);
void font_printf_col(bitmap_t *fb, bitmap_t *font, int x, int y, uint32_t col, const char *fmt,
                     ...);

// return x/y dimensions for string rendered in the given font
v2i font_size(bitmap_t *font, const char *str);
