#pragma once

#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

__attribute__((__noreturn__)) void fatal_error(const char *msg, ...);
int32_t pcg32_rand(void);
void pcg32_seed(uint64_t seed);

#ifdef __cplusplus
}
#endif

// Assertions

#ifdef PLAT_ASSERT_PRINT

#define ASSERT_MSG(expr, ...)                           \
    if (!(expr)) {                                      \
        printf("ASSERT %s:%d - (", __FILE__, __LINE__); \
        printf(#expr);                                  \
        printf(") - ");                                 \
        printf(__VA_ARGS__);                            \
        printf("\n");                                   \
        fflush(stdout);                                 \
        fatal_error(NULL);                              \
    }

#else

#define ASSERT_MSG(expr, ...)     \
    if (!(expr)) {                \
        fatal_error(__VA_ARGS__); \
    }

#endif

#define ASSERT(expr) ASSERT_MSG(expr, "failed")

// Miscellaneous helpers

#define SIGN(v) ((v) >= 0 ? 1 : -1)
#define CLAMP(v, min, max) ((v) > (min) ? ((v) < (max) ? (v) : (max)) : (min))
#define RANDINT(min, max) ((min) + (pcg32_rand() % ((max) - (min))))
#define RANDFLOAT(min, max) ((min) + (pcg32_rand() / (float)INT32_MAX) * ((max) - (min)))
#define NELEMS(arr) (sizeof(arr) / sizeof(arr[0]))
#define FLAG(V) (1 << V)

#ifdef __cplusplus
template <class T> void SWAP(T &a, T &b) {
    T c(a);
    a = b;
    b = c;
}

template <class T> T ABS(T v) {
    return v > 0 ? v : -v;
}

#ifndef MIN
template <class T> T MIN(T a, T b) {
    return a < b ? a : b;
}
#endif

#ifndef MIN
template <class T> T MAX(T a, T b) {
    return a > b ? a : b;
}
#endif
#endif
