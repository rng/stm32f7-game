#pragma once

#include <stdint.h>

typedef struct {
    uint8_t r, g, b;
} colour_t;

typedef struct {
    uint32_t count;
    colour_t *colours;
} palette_t;
