#pragma once

// https://easings.net/#easeOutBack
static inline float ease_out_back(float p, float c1) {
    float c3 = c1 + 1;
    return 1 + c3 * powf(p - 1, 3) + c1 * powf(p - 1, 2);
}
