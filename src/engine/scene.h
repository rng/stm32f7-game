#pragma once

#include "engine/engine.h"

typedef void (*transition_func_t)(bitmap_t *screen, float cur, float end, bool inout, void *param);
typedef void (*scene_func_t)(void *ctx, input_state_t *input, bitmap_t *screen, bool first);

typedef struct {
    int state, state_next, scene_count;
    float transition_time, transition_cur;
    transition_func_t transition;
    const scene_func_t *funcs;
    bool first, inout, transition_first;
    void *param;
} scenes_t;

void scene_init(scenes_t *scenes, const scene_func_t *funcs, size_t nfuncs, int start_scene);
void scene_switch(scenes_t *scenes, int next, transition_func_t transition, float time, bool first,
                  void *param = NULL);
void scene_tick(scenes_t *scenes, void *ctx, input_state_t *input, bitmap_t *screen);

// transitions
void transition_fade(bitmap_t *fb, float cur, float end, bool inout, void *param);
void transition_iris(bitmap_t *fb, float cur, float end, bool inout, void *param);
