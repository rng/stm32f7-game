#include "engine/tilemap.h"
#include "engine/common.h"

v2i tilemap_pixel_size(tilemap_t *map) {
    return v2i(map->width * map->tilesize, map->height * map->tilesize);
}

int tilemap_tile(tilemap_t *map, uint32_t layer, int tx, int ty) {
    ASSERT(layer < map->layercount);
    return map->layers[layer].data[tx + ty * map->width];
}

bool tilemap_inside(tilemap_t *map, int tx, int ty) {
    return (tx >= 0) && (ty >= 0) && (tx < (int)map->width) && (ty < (int)map->height);
}

void tilemap_render(tilemap_t *map, bitmap_t *buffer, uint32_t layer, int px, int py) {
    ASSERT(layer < map->layercount);

    const int tilew = map->tileset.tile_width;
    const int tileh = map->tileset.tile_height;
    ASSERT(tilew && tileh);
    // clamp pixel offsets to map bounds
    px = CLAMP(px, 0, (int)map->width * tilew);
    py = CLAMP(py, 0, (int)map->height * tileh);
    // number of tiles we need to render
    int tilecountx = buffer->width / tilew;
    int tilecounty = buffer->height / tileh;
    // start position on the tilemap, in tiles
    int offset_tx = px / tilew;
    int offset_ty = py / tileh;
    // offset into the start tile, in pixels
    int offset_px = px % tilew;
    int offset_py = py % tileh;
    // if we're starting aligned directly to a tile, we need to draw one extra tile
    if (offset_px != 0) {
        tilecountx++;
    }
    if (offset_py != 0) {
        tilecounty++;
    }
    // number of tiles per row of the tileset bitmap
    int tiles_per_row = map->tileset.width / tilew;

    for (int j = 0; j < tilecounty; j++) {
        int ty = j + offset_ty;
        for (int i = 0; i < tilecountx; i++) {
            int tx = i + offset_tx;
            if (tx < (int)map->width && ty < (int)map->height) {
                // tiled maps use 0 as an empty tile, then all tile indices start at 1.
                int tile = tilemap_tile(map, layer, tx, ty);
                if (tile) {
                    tile--;
                    // calculate x,y position of the tile in the tileset bitmap
                    int tsx = (tile % tiles_per_row) * tilew;
                    int tsy = (tile / tiles_per_row) * tileh;
                    // calculate x,y position of the rendered tile in the destination bitmap
                    int dx = i * tilew - offset_px;
                    int dy = j * tileh - offset_py;
                    bitmap_blit(buffer, &map->tileset, dx, dy, tsx, tsy, tilew, tileh, false);
                }
            }
        }
    }
}
