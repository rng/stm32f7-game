#pragma once

#include <math.h>

template <typename T> struct v2 {
    T x = 0, y = 0;
    v2() {
    }
    v2(T _x, T _y) {
        x = _x;
        y = _y;
    }
    v2 operator+(v2 b) {
        return {x + b.x, y + b.y};
    }
    void operator+=(v2 b) {
        x += b.x;
        y += b.y;
    }
    v2 operator-(v2 b) {
        return {x - b.x, y - b.y};
    }
    v2 operator*(T v) {
        return {x * v, y * v};
    }
    v2 operator/(T v) {
        return {x / v, y / v};
    }
    bool operator==(v2 v) {
        return (x == v.x && y == v.y);
    }
    T dot(v2 v) {
        return (x * v.x) + (y * v.y);
    }
    v2 offset(T xo, T yo) {
        return {x + xo, y + yo};
    }
    float len() {
        return sqrtf((x * x) + (y * y));
    }
    v2 norm() {
        float l = len();
        return {x / l, y / l};
    }
    inline v2 clamp(T minx, T maxx, T miny, T maxy) {
        T _x = (x < minx) ? minx : (x > maxx) ? maxx : x;
        T _y = (y < miny) ? miny : (y > maxy) ? maxy : y;
        return {_x, _y};
    }
};

typedef v2<float> v2f;
typedef v2<int> v2i;
