#include "engine/sound.h"
#include "engine/asset.h"
#include "engine/common.h"
#include "engine/platform.h"
#include "engine/sintab.h"
#include "plat_common/config.h"

#include <string.h>

// notes A0 - G#8 (midi 21 - 116)
static const float notetable[] = {
    27.5,   29.1,   30.9,   32.7,   34.6,   36.7,   38.9,   41.2,   43.7,   46.2,   49.0,   51.9,
    55.0,   58.3,   61.7,   65.4,   69.3,   73.4,   77.8,   82.4,   87.3,   92.5,   98.0,   103.8,
    110.0,  116.5,  123.5,  130.8,  138.6,  146.8,  155.6,  164.8,  174.6,  185.0,  196.0,  207.7,
    220.0,  233.1,  246.9,  261.6,  277.2,  293.7,  311.1,  329.6,  349.2,  370.0,  392.0,  415.3,
    440.0,  466.2,  493.9,  523.3,  554.4,  587.3,  622.3,  659.3,  698.5,  740.0,  784.0,  830.6,
    880.0,  932.3,  987.8,  1046.5, 1108.7, 1174.7, 1244.5, 1318.5, 1396.9, 1480.0, 1568.0, 1661.2,
    1760.0, 1864.7, 1975.5, 2093.0, 2217.5, 2349.3, 2489.0, 2637.0, 2793.8, 2960.0, 3136.0, 3322.4,
    3520.0, 3729.3, 3951.1, 4186.0, 4434.9, 4698.6, 4978.0, 5274.0, 5587.7, 5919.9, 6271.9, 6644.9,
};

#define MAX_NOTE (NELEMS(notetable))
#define NOTE_FREQ(N) (notetable[N - 21])

struct sound_instrument_t {
    sound_waveform_t type;
    union {
        sound_sample_t sample;
        struct {
            int32_t attack, decay, freq_slide;
        };
    };
};

struct sound_channel_t {
    sound_instrument_t *instrument;
    uint32_t synth_pos, env_pos, synth_incr;
    uint16_t volume, state;
    bool active;
};

void sound_init(sound_mixer_t *mixer, uint32_t sample_rate, size_t max_instruments,
                size_t max_channels) {
    mixer->sample_rate = sample_rate;
    mixer->max_instruments = max_instruments;
    mixer->max_channels = max_channels;
    mixer->instruments =
        (sound_instrument_t *)platform->alloc(sizeof(sound_instrument_t) * max_instruments);
    mixer->channels = (sound_channel_t *)platform->alloc(sizeof(sound_channel_t) * max_channels);
}

void sound_term(sound_mixer_t *mixer) {
    platform->free(mixer->instruments);
}

void sound_instrument_pcm(sound_mixer_t *mixer, sound_instrument_id_t id, const char *filename) {
    ASSERT_MSG(id < mixer->max_instruments, "invalid instrument id");
    asset_load_sample(filename, &mixer->instruments[id].sample);
    mixer->instruments[id].type = SOUND_WAVEFORM_PCM;
    ASSERT_MSG(mixer->instruments[id].sample.channel_count == 1, "Stereo samples unsupported")
}

void sound_instrument_synth(sound_mixer_t *mixer, sound_instrument_id_t id, sound_waveform_t type,
                            uint16_t attack, uint16_t decay, int16_t freq_slide) {
    ASSERT_MSG(id < mixer->max_instruments, "invalid instrument id");
    ASSERT_MSG(type != SOUND_WAVEFORM_PCM, "invalid waveform type");
    mixer->instruments[id].type = type;
    mixer->instruments[id].attack = attack;
    mixer->instruments[id].decay = decay;
    mixer->instruments[id].freq_slide = freq_slide;
}

static uint32_t mix_envelope(sound_instrument_t &ins, int tick, int volume) {
    uint32_t r = 0;
    if (ins.type == SOUND_WAVEFORM_PCM) {
        r = USHRT_MAX; // no evelope for PCM
    } else if (tick <= ins.attack) {
        r = ins.attack == 0 ? USHRT_MAX : (USHRT_MAX * tick) / ins.attack;
    } else if (tick <= (ins.attack + ins.decay)) {
        r = USHRT_MAX - (USHRT_MAX * (tick - ins.attack)) / ins.decay;
    }

    return (r * (volume * 257)) >> 17;
}

// XORShift psuedo-random noise (https://en.wikipedia.org/wiki/Xorshift)
static int16_t noise(uint16_t &state) {
    state ^= state >> 7;
    state ^= state << 9;
    state ^= state >> 13;
    return state;
}

static int32_t mix_synth(sound_instrument_t &ins, uint16_t pos, uint16_t &state) {
    switch (ins.type) {
        case SOUND_WAVEFORM_PCM:
            return pos < ins.sample.sample_count ? ins.sample.data[pos] : 0;
        case SOUND_WAVEFORM_SIN:
            return sinfx16_i(pos);
        case SOUND_WAVEFORM_ORG:
            return (sinfx16_i(pos) / 2) + (sinfx16_i(pos * 2) / 4) + (sinfx16_i(pos * 4) / 8);
        case SOUND_WAVEFORM_SQR:
            return (pos >= (USHRT_MAX / 2)) ? SHRT_MAX : SHRT_MIN;
        case SOUND_WAVEFORM_SAW:
            return pos - SHRT_MAX;
        case SOUND_WAVEFORM_NSE:
            return noise(state);
        default:
            return 0;
    }
}

void sound_mix(sound_mixer_t *mixer, void *buf, size_t buf_size) {
    memset(buf, 0, buf_size);
    size_t dst_samples = buf_size / 2; // buf_size is in bytes - convert to samples

    // core mixing loop - iterate through each active channel, run synthesis for the channel,
    // and accumulate resulting samples into the output buffer.
    for (int chidx = 0; chidx < mixer->max_channels; chidx++) {
        sound_channel_t *ch = &mixer->channels[chidx];
        sound_instrument_t &ins = *ch->instrument;
        if (!ch->active) {
            continue;
        }
        // set up synth parameters for this channel
        int16_t *dst = (int16_t *)buf;
        bool is_sample = ins.type == SOUND_WAVEFORM_PCM;
        uint32_t end_pos = is_sample ? ins.sample.sample_count : ins.attack + ins.decay;
        int slide_rate = is_sample ? 0 : 1024 * ins.freq_slide;
        // run synthesis for this channel, accumulating into the destination buffer
        for (size_t i = 0; i < dst_samples; i++) {
            int32_t val = mix_synth(ins, ch->synth_pos >> 16, ch->state);
            int32_t env = mix_envelope(ins, ch->env_pos, ch->volume);
            *dst++ += (val * env) >> 15;
            ch->env_pos++;
            ch->synth_incr += slide_rate;
            ch->synth_pos += ch->synth_incr;
        }
        // check if we're finished playing this instrument
        ch->active = ch->env_pos < end_pos;
    }
}

int sound_play_note(sound_mixer_t *mixer, sound_instrument_id_t id, sound_note_t note,
                    uint8_t volume) {
    ASSERT_MSG(note < MAX_NOTE, "invalid note");
    ASSERT_MSG(id < mixer->max_instruments, "invalid instrument");
    for (int i = 0; i < mixer->max_channels; i++) {
        if (mixer->channels[i].active) {
            continue;
        }
        sound_channel_t *ch = &mixer->channels[i];
        ch->instrument = &mixer->instruments[id];

        // calculate increment per sample - this number reflects how much of a synth waveform
        // we progress through each output sample. It depends on the note frequency we're
        // playing (high not frequency => faster progression through the waveform), and output
        // sample rate.
        if (ch->instrument->type == SOUND_WAVEFORM_PCM) {
            // for PCM data, we behave as if the source recording is a C4 note
            float src_sample_rate = 8000.f; // FIXME: assumes fixed source sample rate
            ch->synth_incr = 65536 * (NOTE_FREQ(note) / NOTE_FREQ(SOUND_NOTE_C4)) *
                             (src_sample_rate / mixer->sample_rate);
        } else {
            ch->synth_incr = UINT_MAX * (NOTE_FREQ(note) / mixer->sample_rate);
        }

        ch->synth_pos = 0;
        ch->env_pos = 0;
        ch->volume = volume;
        ch->active = true;
        ch->state = RANDINT(1, USHRT_MAX);
        return i;
    }
    return -1;
}

void sound_stop_channel(sound_mixer_t *mixer, int channel_idx) {
    ASSERT_MSG((channel_idx < mixer->max_channels) && (channel_idx >= 0), "invalid channel id");
    mixer->channels[channel_idx].active = false;
}
