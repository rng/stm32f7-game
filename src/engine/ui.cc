#include "engine/ui.h"
#include "engine/ease.h"
#include "engine/font.h"

#include <string.h>

#define RELEASED(key) (!ui.input.key && ui.prev_input.key)
#define PRESSED(key) (ui.input.key && !ui.prev_input.key)
#define DOWN(key) (ui.input.key)

// Default configuration

const ui_config_t ui_default_config = {
    254, // foreground
    0,   // background
    240, // disabled
    3,   // padding
    1,   // row margin
};

// Cursor transition logic

enum { TRANSITION_IDLE = 0, TRANSITION_START, TRANSITION_RUNNING };

static void ui_txn_position(ui_transition_t &txn, rect_t &r, v2i pos) {
    if (txn.state == TRANSITION_START) {
        txn.bounds = r;
        txn.end = pos;
        txn.state = TRANSITION_RUNNING;
    } else if (txn.state == TRANSITION_IDLE) {
        txn.start = pos;
    }
}

static void ui_txn_start(ui_transition_t &txn) {
    txn.len = 0.15f;
    txn.t = 0.f;
    txn.state = TRANSITION_START;
}

static bool ui_txn_update(ui_transition_t &txn, float dt, rect_t &r) {
    if (txn.state == TRANSITION_RUNNING) {
        txn.t += dt;

        // calculate position of the transition using an ease
        v2i delta = txn.end - txn.start;
        float p = ease_out_back(txn.t / txn.len, 1.7f);
        v2i pos = txn.start.offset((float)delta.x * p, (float)delta.y * p);

        // return rectangle for transition bounds
        r.x = pos.x - txn.bounds.w;
        r.y = pos.y;
        r.w = txn.bounds.w;
        r.h = txn.bounds.h;

        // check if we're done with the transition
        if (txn.t >= txn.len) {
            txn.state = TRANSITION_IDLE;
        }
        return true;
    }
    return false;
}

// Container stack

static ui_container_t *push_container(ui_t &ui, ui_container_type_t type) {
    ASSERT(ui.container_count < UI_MAX_CONTAINER_NEST);
    auto c = &ui.containers[ui.container_count];
    c->parent = ui.container;
    c->t = type;
    ui.container_count++;
    ui.container = c;
    ui.col = 0;
    return c;
}

static void pop_container(ui_t &ui) {
    ui.container_count--;
    ui.container = ui.container->parent;
}

// Column system

static rect_t get_bounding_rect(ui_t &ui) {
    ASSERT(ui.col < ui.container->columns);
    int w = ui.container->rect.w / ui.container->columns;
    return rect_t(ui.pos.x, ui.pos.y, w, ui.container->rect.h);
}

static void next_column(ui_t &ui, bool selectable = false) {
    rect_t r = get_bounding_rect(ui);
    ASSERT(ui.container->t == UI_CONTAINER_ROW);
    int col_width = ui.container->rect.w / ui.container->columns;
    ui.pos.x += col_width;
    // update selectable count if necessary
    if (selectable) {
        if ((ui.selectable.x == ui.cursor.x) && (ui.selectable.y == ui.cursor.y)) {
            ui_txn_position(ui.txn, r, ui.pos);
        }
        // update selectable count
        if (ui.selectable.y == ui.row) {
            ui.selectable.x++;
            ui.max_selectable.x = MAX(ui.max_selectable.x, ui.selectable.x);
        } else {
            ui.selectable.y++;
            ui.max_selectable.y = MAX(ui.max_selectable.y, ui.selectable.y);
        }
    }
}

static bool check_selectable(ui_t &ui) {
    return (ui.selectable.x == ui.cursor.x) && (ui.selectable.y == ui.cursor.y);
}

static v2i ui_rect_align(int bounds_w, int bounds_h, int r_w, int r_h, int padding,
                         ui_alignment_t align) {
    int ox;
    int oy = (bounds_h - r_h) / 2; // alway vertically centre

    if (align == UI_ALIGN_LEFT) {
        ox = padding;
    } else if (align == UI_ALIGN_CENTRE) {
        ox = (bounds_w - r_w) / 2;
    } else if (align == UI_ALIGN_RIGHT) {
        ox = bounds_w - r_w - padding;
    } else {
        ASSERT(false);
    }
    return {ox, oy};
}

static v2i ui_text_align(int bounds_w, int bounds_h, const char *str, int padding,
                         ui_alignment_t align) {
    v2i text_size = font_size(font_system(), str);

    return ui_rect_align(bounds_w, bounds_h, text_size.x, text_size.y - 1, padding, align);
}

// public API

void ui_start(ui_t &ui, bitmap_t &fb, input_state_t &input, const ui_config_t &cfg) {
    ui.fb = &fb;
    ui.cfg = cfg;
    ui.prev_input = ui.input;
    ui.input = input;
    ui.container_count = 0;
    ui.container = NULL;
    ui.pos = {0, 0};
    ui.selectable = {0, 0};
    ui.max_selectable = {0, 0};
    auto c = push_container(ui, UI_CONTAINER_PANEL);
    c->rect = rect_t(0, 0, fb.width, fb.height);
    ui.firstrow = true;
    ui.row = 0;
}

bool repeat_check(ui_t &ui) {
    // held button press
    uint32_t mask = 0;
    if (DOWN(up)) {
        mask |= 1;
    }
    if (DOWN(down)) {
        mask |= 2;
    }
    if (DOWN(left)) {
        mask |= 4;
    }
    if (DOWN(right)) {
        mask |= 8;
    }

    if (mask && (ui.held_mask == 0)) {
        ui.held_mask = mask;
    } else if (mask && (mask == ui.held_mask)) {
        ui.held_time += ui.input.dt;
        if (ui.held_time > 0.3f) {
            if (timeout_expired(&ui.repeater, ui.input.dt, 0.1f)) {
                if (mask & 1) {
                    ui.cursor.y--;
                } else if (mask & 2) {
                    ui.cursor.y++;
                }
                return true;
            }
        }
    } else {
        ui.held_mask = 0;
        ui.held_time = 0;
    }
    return false;
}

bool ui_end(ui_t &ui) {
    auto oldcursor = ui.cursor;

    if (!repeat_check(ui)) {
        // normal button press
        if (PRESSED(up)) {
            ui.cursor.y--;
        } else if (PRESSED(down)) {
            ui.cursor.y++;
        }
        if (PRESSED(left)) {
            ui.cursor.x--;
        } else if (PRESSED(right)) {
            ui.cursor.x++;
        }
    }
    ui.cursor.x = CLAMP(ui.cursor.x, 0, ui.max_selectable.x);
    ui.cursor.y = CLAMP(ui.cursor.y, 0, ui.max_selectable.y - 1);

    // update and draw the transition if necessary
    rect_t txnrect;
    if (ui_txn_update(ui.txn, ui.input.dt, txnrect)) {
        bitmap_draw_fillrect(ui.fb, txnrect.x, txnrect.y, txnrect.x + txnrect.w,
                             txnrect.y + txnrect.h, ui.cfg.col_fg);
    }
    // if the cursor moved, start a transition
    if ((oldcursor.x != ui.cursor.x) || (oldcursor.y != ui.cursor.y)) {
        ui_txn_start(ui.txn);
        return true;
    }
    return false;
}

void ui_setcursor(ui_t &ui, v2i pos) {
    ui.cursor = pos;
}

void ui_container_row(ui_t &ui, int columns, int height) {
    ui.pos.x = ui.container->rect.x;
    if (!ui.firstrow) {
        ui.pos.y += ui.container->rect.h + ui.cfg.row_margin;
        pop_container(ui);
    }
    ui.firstrow = false;
    ui.row++;
    ui.selectable.x = 0;
    auto c = push_container(ui, UI_CONTAINER_ROW);
    int h = height == UI_DIM_FILL ? c->parent->rect.h : height;
    c->rect = rect_t(ui.pos.x, ui.pos.y, c->parent->rect.w, h);
    c->columns = columns;
}

void ui_container_panel(ui_t &ui, v2i pad) {
    auto c = push_container(ui, UI_CONTAINER_PANEL);
    ui.pos += pad;
    c->rect = rect_t(ui.pos.x, ui.pos.y, c->parent->rect.w - pad.x * 2,
                     c->parent->rect.h - ui.pos.y - pad.y * 2);
    ui.firstrow = true;
}

void ui_label(ui_t &ui, const char *str, ui_alignment_t align, ui_style_t style) {
    rect_t r = get_bounding_rect(ui);
    v2i ofs = ui_text_align(r.w, r.h, str, ui.cfg.padding, align);
    uint8_t col = style == UI_STYLE_NORMAL ? ui.cfg.col_fg : ui.cfg.col_dis;
    font_print_col(ui.fb, font_system(), r.x + ofs.x, r.y + ofs.y, str, col);
    next_column(ui);
}

void ui_sprite(ui_t &ui, bitmap_t sprite, int frame, ui_alignment_t align) {
    rect_t r = get_bounding_rect(ui);
    v2i ofs = ui_rect_align(r.w, r.h, sprite.tile_width, sprite.tile_height, 0, align);
    bitmap_blit_tile(ui.fb, &sprite, r.x + ofs.x, r.y + ofs.y, false, frame);
    next_column(ui);
}

bool ui_button(ui_t &ui, const char *str, ui_alignment_t align) {
    rect_t r = get_bounding_rect(ui);
    v2i ofs = ui_text_align(r.w, r.h, str, ui.cfg.padding, align);

    bool selected = check_selectable(ui);

    // draw
    if (selected && ui.txn.state == TRANSITION_IDLE) {
        if (DOWN(a)) {
            bitmap_draw_rect(ui.fb, r.x, r.y, r.x + r.w, r.y + r.h, ui.cfg.col_fg);
            font_print_col(ui.fb, font_system(), r.x + ofs.x, r.y + ofs.y, str, ui.cfg.col_fg);
        } else {
            bitmap_draw_fillrect(ui.fb, r.x, r.y, r.x + r.w, r.y + r.h, ui.cfg.col_fg);
            font_print_col(ui.fb, font_system(), r.x + ofs.x, r.y + ofs.y, str, ui.cfg.col_bg);
        }
    } else {
        font_print_col(ui.fb, font_system(), r.x + ofs.x, r.y + ofs.y, str, ui.cfg.col_fg);
    }

    next_column(ui, true);
    return selected && RELEASED(a);
}

bool ui_slider(ui_t &ui, const char *str, int &value, int min, int max) {
    rect_t r = get_bounding_rect(ui);
    v2i ofs = ui_text_align(r.w, r.h, str, ui.cfg.padding, UI_ALIGN_LEFT);

    bool selected = check_selectable(ui);
    int bgcol = ui.cfg.col_bg;
    int txcol = ui.cfg.col_fg;

    // update state
    int oldvalue = value;
    if (selected && ui.txn.state == TRANSITION_IDLE) {
        SWAP(bgcol, txcol);

        if (RELEASED(a)) {
            value = (value + 1) % (max + 1);
        } else if (RELEASED(right)) {
            value = CLAMP(value + 1, min, max);
        } else if (RELEASED(left)) {
            value = CLAMP(value - 1, min, max);
        }
    }

    // draw
    bitmap_draw_fillrect(ui.fb, r.x, r.y, r.x + r.w, r.y + r.h, bgcol);
    for (int i = 0; i < max; i++) {
        const int block_width = 5;
        int sx = r.x + r.w + (block_width + 1) * (i - max) - ui.cfg.padding + 1;
        int ex = sx + block_width;
        int sy = r.y + ui.cfg.padding;
        int ey = sy + r.h - (2 * ui.cfg.padding);
        if (value > i) {
            bitmap_draw_fillrect(ui.fb, sx, sy, ex, ey, txcol);
        }
    }
    font_print_col(ui.fb, font_system(), r.x + ofs.x, r.y + ofs.y, str, txcol);

    next_column(ui, true);
    return oldvalue != value;
}

bool ui_checkbox(ui_t &ui, const char *str, int &value) {
    rect_t r = get_bounding_rect(ui);
    v2i ofs = ui_text_align(r.w, r.h, str, ui.cfg.padding, UI_ALIGN_LEFT);

    bool selected = check_selectable(ui);
    int bgcol = ui.cfg.col_bg;
    int txcol = ui.cfg.col_fg;

    // update state
    bool change = false;
    if (selected && ui.txn.state == TRANSITION_IDLE) {
        SWAP(bgcol, txcol);
        if (RELEASED(a) || RELEASED(left) || RELEASED(right)) {
            value = !value;
            change = true;
        }
    }

    // draw
    bitmap_draw_fillrect(ui.fb, r.x, r.y, r.x + r.w, r.y + r.h, bgcol);
    int dim = r.h - (2 * ui.cfg.padding);
    int sx = r.x + r.w - dim - ui.cfg.padding;
    int sy = r.y + ui.cfg.padding;
    if (value) {
        bitmap_draw_fillrect(ui.fb, sx, sy, sx + dim, sy + dim, txcol);
    } else {
        bitmap_draw_rect(ui.fb, sx, sy, sx + dim, sy + dim, txcol);
    }
    font_print_col(ui.fb, font_system(), r.x + ofs.x, r.y + ofs.y, str, txcol);

    next_column(ui, true);
    return change;
}
