#pragma once

#include <stdbool.h>
#include <stdint.h>

// Bitmap pixel formats
typedef enum {
    BITMAP_FORMAT_RGBA8888 = 1,
    BITMAP_FORMAT_RGB565 = 2,
    BITMAP_FORMAT_RGBA5551 = 3,
    BITMAP_FORMAT_PAL8 = 4,
} bitmap_format_t;

typedef struct {
    uint16_t width, height;
    uint16_t tile_width, tile_height;
    bitmap_format_t format;
    uint8_t *data;
} bitmap_t;

// Helper macro to get a pointer to a position in a bitmap's data
#define PIXEL_PTR(BITMAP, X, Y, STRIDE) \
    ((BITMAP)->data + ((X)*STRIDE) + ((Y) * (BITMAP)->width * STRIDE))

// Helper macro to create a PAL8 bitmap
#define BITMAP8(W, H, DATA) \
    { (uint16_t)(W), (uint16_t)(H), 0, 0, BITMAP_FORMAT_PAL8, DATA }

// Copy all pixels from bitmap `src` into bitmap `dest` at position (dx,dy)
// Performs pixel format conversion and clipping as necessary.
void bitmap_blit_full(bitmap_t *dest, bitmap_t *src, int dx, int dy);

// Copy a tile from tilesheet bitmap `src` into bitmap `dest` at position (dx,dy)
// `tile` refers to tile index in the tilesheet, with tile size specified in the
// bitmap (`tile_width` / `tile_height` fields).
// Performs pixel format conversion and clipping as necessary.
void bitmap_blit_tile(bitmap_t *dest, bitmap_t *src, int dx, int dy, bool hflip, int tile);

// Copy (w x h) pixels from bitmap `src` at position (sx,sy) into bitmap `dest` at position (dx,dy)
// Note, this does not use source pixel colour, instead it uses the specified `col` whenever the
// source pixel is not transparent.
void bitmap_blit_col(bitmap_t *dest, bitmap_t *src, int dx, int dy, int sx, int sy, int w, int h,
                     bool hflip, uint32_t col);

// Copy (w x h) pixels from bitmap `src` at position (sx,sy) into bitmap `dest` at position (dx,dy)
// Performs pixel format conversion and clipping as necessary.
void bitmap_blit(bitmap_t *dest, bitmap_t *src, int dx, int dy, int sx, int sy, int w, int h,
                 bool hflip);

// Copy image from `src` into `dest`, scaling by integer `scale` factor. Assumes `src` and `dest`
// bitmaps are exactly the correct size for the scaling (ie no clipping is performed).
void bitmap_scale(bitmap_t *dest, bitmap_t *src, int scale);

// Fill a bitmap with a pixel of the given colour
// Note pixel must be packed in the correct format for the given bitmap.
void bitmap_fill(bitmap_t *dest, uint32_t colour);

// Draw a 1px wide vertical line in the given colour
void bitmap_draw_vline(bitmap_t *dest, int x, int y1, int y2, uint32_t colour);

// Draw a 1px wide horizontal line in the given colour
void bitmap_draw_hline(bitmap_t *dest, int x1, int x2, int y, uint32_t colour);

// Draw a 1px wide line in the given colour
void bitmap_draw_line(bitmap_t *dest, int x1, int y1, int x2, int y2, uint32_t colour);

// Draw a 1px wide rectangle in the given colour
void bitmap_draw_rect(bitmap_t *dest, int x1, int y1, int x2, int y2, uint32_t colour);

void bitmap_draw_fillrect(bitmap_t *dest, int x1, int y1, int x2, int y2, uint32_t colour);

// Return bytes per pixel for a given format
uint32_t bitmap_format_bytes_per_pixel(bitmap_format_t format);
