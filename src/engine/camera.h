#pragma once

#include "engine/engine.h"

struct camera_t {
    v2f pos;
    v2i view_size;
    v2i world_size;
    float shake;
    float trackrate;
};

void camera_init(camera_t &cam, v2i view_size, v2i world_size, float trackrate);
void camera_set_pos(camera_t &cam, v2f pos);
rect_t camera_update(camera_t &cam, v2f target, float dt);
