#pragma once

#include "engine/common.h"

#include <limits.h>

#define MAX_RECT rect_t(INT_MIN, INT_MIN, INT_MAX, INT_MAX)
#define ZERO_RECT rect_t(0, 0, 0, 0)

struct rect_t {
    int x, y, w, h;

    rect_t() {
        x = y = w = h = 0;
    }

    rect_t(int x, int y, int w, int h) {
        this->x = x;
        this->y = y;
        this->w = w;
        this->h = h;
    }

    // Return a new rectangle translated by (dx,dy)
    inline rect_t translate(int dx, int dy) {
        return {x + dx, y + dy, w, h};
    }

    // Return a new rectangle (dx,dy) larger than this rectangle.
    // If dx/dy are positive, grows the rectangle from the right/bottom
    // If dx/dy are negative, grows the rectangle from the left/top
    inline rect_t expand(int dx, int dy) {
        rect_t ret = *this;
        ret.w += ABS(dx);
        ret.h += ABS(dy);
        if (dx < 0) {
            ret.x += dx;
        }
        if (dy < 0) {
            ret.y += dy;
        }
        return ret;
    }

    // Return a new rectangle grown in both positive and negative (dx,dy)
    // Resulting rectangle is (2*dx, 2*dy) larger.
    inline rect_t expand2(int dx, int dy) {
        rect_t ret = *this;
        ret.w += dx * 2;
        ret.h += dy * 2;
        ret.x -= dx;
        ret.y -= dy;
        return ret;
    }

    // Returns true if rectangle `o` overlaps this rectangle (that is if these
    // rectangles were drawn to the screen would some of their pixels overwrite
    // each other).
    inline bool overlap(rect_t &o) {
        if ((this->x >= (o.x + o.w)) || (o.x >= (this->x + this->w)) || (this->y >= (o.y + o.h)) ||
            (o.y >= (this->y + this->h))) {
            return false;
        }
        return true;
    }

    // Returns true if (x,y) is within the bounds of this rectangle
    inline bool inside(int x, int y) {
        return ((x >= this->x) && (y >= this->y) && (x < (this->x + this->w)) &&
                (y < (this->y + this->h)));
    }

    inline bool equals(rect_t o) {
        return (o.x == x) && (o.y == y) && (o.w == w) && (o.h == h);
    }
};
