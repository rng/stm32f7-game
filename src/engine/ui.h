#pragma once

#include "engine/bitmap.h"
#include "engine/input.h"
#include "engine/rect.h"
#include "engine/timeout.h"
#include "engine/vector.h"

#include <stdbool.h>
#include <stdint.h>

// Immediate mode user interface library.
// Support for simple row/column structured UIs with dpad-controlled cursor.
//
// Example code:
//
//     ui_t ui;
//     ui_start(ui, fb, input, &ui_default_config) // start the UI
//     ui_container_row(ui, 2, 10);                // first row 2 columns, 10px high
//     ui_label(ui, "hello");                      // hello text
//     ui_bitmap(ui, bitmap);                      // followed by a sprite
//     ui_container_row(ui, 1);                    // new row, 1 column
//     if (ui_button(ui, "push me"))               // "push me" button
//        printf("button pressed!\n");             // action to perform if button pressed
//     ui_end(ui);                                 // finish the UI
//
// Approximate resulting UI:
//
//    +----------+----------+
//    | hello    | <sprite> |
//    +----------+----------+
//    |     [ push me ]     |
//    +---------------------+

#define UI_MAX_CONTAINER_NEST (8)

enum ui_container_type_t {
    UI_CONTAINER_ROW,
    UI_CONTAINER_PANEL,
};

struct ui_container_t {
    ui_container_type_t t;
    ui_container_t *parent;
    rect_t rect;
    int columns;
};

struct ui_config_t {
    uint8_t col_fg;
    uint8_t col_bg;
    uint8_t col_dis;
    uint8_t padding;
    uint8_t row_margin;
};

extern const ui_config_t ui_default_config;

struct ui_transition_t {
    rect_t bounds;
    int state;
    v2i start, end;
    float t, len;
};

struct ui_t {
    // caller provided
    bitmap_t *fb;
    input_state_t input, prev_input;
    ui_config_t cfg;
    // reset on `ui_start`
    ui_container_t containers[UI_MAX_CONTAINER_NEST];
    ui_container_t *container;
    int container_count;
    int col, row;
    v2i pos, selectable, max_selectable;
    bool firstrow;
    // persistent across multiple calls
    v2i cursor;
    ui_transition_t txn;
    uint32_t held_mask;
    float held_time;
    timeout_t repeater;
};

// Horizontal alignment of text in widgets
enum ui_alignment_t {
    UI_ALIGN_LEFT,
    UI_ALIGN_CENTRE,
    UI_ALIGN_RIGHT,
};

// Style for widgets
enum ui_style_t {
    UI_STYLE_NORMAL = 0,
    UI_STYLE_DISABLED,
};

// For row height parameters, pass `UI_DIM_FILL` to expand the row height to fill the parent
// container.
enum ui_dim_t {
    UI_DIM_FILL = -1,
};

// Start/finish a UI. Passed a framebuffer to render to, the current input state, as well
// as the UI config (widget colours, padding sizes, etc). All `ui_*` calls should be
// between a `ui_start` and `ui_end` calls.
void ui_start(ui_t &ui, bitmap_t &fb, input_state_t &input, const ui_config_t &cfg);
bool ui_end(ui_t &ui);

// Cursor position is saved between multiple calls to `ui_start`, this function allows
// moving or resetting the cursor position
void ui_setcursor(ui_t &ui, v2i pos);

// * Layout containers *

// Add a new row with a specificed number of columns. Column widths will be the parent
// widget width / num columns.
void ui_container_row(ui_t &ui, int columns = 1, int height = UI_DIM_FILL);

// Add a new scrollable panel view
void ui_container_panel(ui_t &ui, v2i pad = {0, 0});

// * Widgets *
// All widgets will be added to the latest (empty) row/column.

// Add a text label
void ui_label(ui_t &ui, const char *str, ui_alignment_t align = UI_ALIGN_LEFT,
              ui_style_t = UI_STYLE_NORMAL);
// Add a bitmaps sprite
void ui_sprite(ui_t &ui, bitmap_t sprite, int frame, ui_alignment_t align);

// Add a button, return true if clicked
bool ui_button(ui_t &ui, const char *str, ui_alignment_t align = UI_ALIGN_LEFT);
// Add a slider, return true if value changed, and update the passed in value
bool ui_slider(ui_t &ui, const char *str, int &value, int min, int max);
// Add a checkbox, return true if value changed, and update the passed in value
bool ui_checkbox(ui_t &ui, const char *str, int &value);
