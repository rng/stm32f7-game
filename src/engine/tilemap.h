#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "engine/bitmap.h"
#include "engine/vector.h"

// Support for multi-layer tile maps using a spritesheet for tile bitmaps.

enum {
    TILEMAP_MAX_LAYERS = 5,
};

typedef struct {
    uint8_t *data;
} tilemap_layer_t;

typedef struct {
    uint32_t width, height, tilesize, layercount;
    tilemap_layer_t layers[TILEMAP_MAX_LAYERS];
    bitmap_t tileset;
} tilemap_t;

// Return dimensions of the map in pixels
v2i tilemap_pixel_size(tilemap_t *map);

// Return the tile at position `tx`,`ty` on `layer`.
// NOTE: no bounds checking is performed so before calling ensure:
// 0 <= layer < map.layercount, 0 <= tx < map.width, 0 <= ty < map.height
int tilemap_tile(tilemap_t *map, uint32_t layer, int tx, int ty);

// Return true if position `tx,`ty` is within the bounds of the map
bool tilemap_inside(tilemap_t *map, int tx, int ty);

// Draw layer of tilemap `map` to the bitmap `buffer`. Top left point of buffer
// starts at tilemap tile (px,py) (in pixel coordinates, not tiles).
void tilemap_render(tilemap_t *map, bitmap_t *buffer, uint32_t layer, int px, int py);
