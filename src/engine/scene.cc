#include "engine/scene.h"

static float distance_sq(v2f p1, v2f p2) {
    float dx = p1.x - p2.x;
    float dy = p1.y - p2.y;
    return (dx * dx) + (dy * dy);
}

// Scene transition is called twice. Once for in and then once for out.
// inout is true for in and false for out.
// Iris transition assumes the next scene has a black background.
void transition_iris(bitmap_t *fb, float cur, float end, bool inout, void *param) {
    if (inout) {
        // First transition
        ASSERT_MSG(param, "Valid param required.");
        float n = (end - cur) / end * 48.0f;
        n *= n;
        for (int i = 0; i < fb->width; i++) {
            for (int j = 0; j < fb->height; j++) {
                if (distance_sq(v2f(i, j), *(v2f *)param) > n) {
                    *PIXEL_PTR(fb, i, j, 1) = 255;
                }
            }
        }
    } else {
        // Second transition
        for (int i = 0; i < fb->width; i++) {
            for (int j = 0; j < fb->height; j++) {
                *PIXEL_PTR(fb, i, j, 1) = 255;
            }
        }
    }
}

void transition_fade(bitmap_t *fb, float cur, float end, bool inout, void *param) {
    int n = cur / end * 12.0f;
    if (!inout) {
        n = 11 - n;
    }
    for (int i = 0; i < fb->width; i++) {
        for (int j = 0; j < fb->height; j++) {
            int v = *PIXEL_PTR(fb, i, j, 1);
            v = v + (n * 16);
            *PIXEL_PTR(fb, i, j, 1) = CLAMP(v, 0, 255);
        }
    }
}

void scene_init(scenes_t *scenes, const scene_func_t *funcs, size_t nfuncs, int start_scene) {
    scenes->funcs = funcs;
    scenes->state = start_scene;
    scenes->first = true;
    scenes->scene_count = nfuncs;
}

void scene_switch(scenes_t *scenes, int next, transition_func_t transition, float time, bool first,
                  void *param) {
    if (scenes->state_next == next) {
        return;
    }
    ASSERT_MSG(next < scenes->scene_count, "Invalid scene");
    scenes->state_next = next;
    scenes->transition_time = time / 2;
    scenes->transition_cur = 0;
    scenes->transition = transition;
    scenes->transition_first = first;
    scenes->inout = true;
    scenes->param = param;
}

static bool scene_handle_transition(scenes_t *scenes, bitmap_t *screen, float dt) {
    bool r = false;
    if (scenes->transition) {
        scenes->transition_cur += dt;
        if (scenes->transition_cur >= scenes->transition_time) {
            scenes->state = scenes->state_next;
            if (scenes->inout) {
                scenes->inout = false;
                scenes->transition_cur -= scenes->transition_time;
                r = scenes->transition_first;
            } else {
                scenes->transition = NULL;
                scenes->state_next = -1;
                return r;
            }
        }
        scenes->transition(screen, scenes->transition_cur, scenes->transition_time, scenes->inout,
                           scenes->param);
    }
    return r;
}

void scene_tick(scenes_t *scenes, void *ctx, input_state_t *input, bitmap_t *screen) {
    scenes->funcs[scenes->state](ctx, input, screen, scenes->first);
    scenes->first = scene_handle_transition(scenes, screen, input->dt);
}
