#include "engine/font.h"
#include "engine/fontsystem_4x6.h"
#include "tinyprintf.h"

#include <stdarg.h>
#include <string.h>

bitmap_t *font_system(void) {
    return (bitmap_t *)&fontsystem;
}

void font_print(bitmap_t *fb, bitmap_t *font, int x, int y, const char *text) {
    const int tiles_per_row = font->width / font->tile_width;
    while (*text) {
        int char_idx = (*text++) - ' ';
        int sx = (char_idx % tiles_per_row) * font->tile_width;
        int sy = (char_idx / tiles_per_row) * font->tile_height;
        bitmap_blit(fb, font, x, y, sx, sy, font->tile_width, font->tile_height, false);
        x += font->tile_width;
    }
}

void font_printf(bitmap_t *fb, bitmap_t *font, int x, int y, const char *fmt, ...) {
    char buffer[64];
    va_list args;
    va_start(args, fmt);
    tfp_vsnprintf(buffer, sizeof(buffer), fmt, args);
    font_print(fb, font, x, y, buffer);
    va_end(args);
}

void font_print_col(bitmap_t *fb, bitmap_t *font, int x, int y, const char *text, uint32_t col) {
    const int tiles_per_row = font->width / font->tile_width;
    while (*text) {
        int char_idx = (*text++) - ' ';
        int sx = (char_idx % tiles_per_row) * font->tile_width;
        int sy = (char_idx / tiles_per_row) * font->tile_height;
        bitmap_blit_col(fb, font, x, y, sx, sy, font->tile_width, font->tile_height, false, col);
        x += font->tile_width;
    }
}

void font_printf_col(bitmap_t *fb, bitmap_t *font, int x, int y, uint32_t col, const char *fmt,
                     ...) {
    char buffer[64];
    va_list args;
    va_start(args, fmt);
    tfp_vsnprintf(buffer, sizeof(buffer), fmt, args);
    font_print_col(fb, font, x, y, buffer, col);
    va_end(args);
}

v2i font_size(bitmap_t *font, const char *str) {
    return {(int)strlen(str) * font->tile_width, font->tile_height};
}
