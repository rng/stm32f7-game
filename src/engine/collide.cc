#include <math.h>
#include <stdio.h>

#include "engine/collide.h"
#include "engine/common.h"

// Core collision checker assumptions:
// - res rx & ry are initialised with object position.
// - sx & sy are smaller than tile size
static void _step(collide_res_t *res, collide_conf_t *conf, float sx, float sy, int w, int h) {
    float ftilesize = (float)(conf->tilesize);

    // step x axis
    res->rx += sx;
    int pxoffsetx = sx > 0 ? w : 0;
    int tilex = floorf((res->rx + pxoffsetx) / conf->tilesize);
    // get min/max tiles on opposite (y) axis
    int starty = floorf(res->ry / ftilesize);
    int endy = ceilf((res->ry + h) / ftilesize);
    // iterate over tiles along y axis
    for (int tiley = starty; tiley < endy; tiley++) {
        if (conf->tile(conf->context, tilex, tiley) == COLLIDE_TILE_IMPASSABLE) {
            int tileoffsetx = sx < 0 ? conf->tilesize : 0;
            res->collx = true;
            res->tx = tilex;
            res->rx = (tilex * conf->tilesize) - pxoffsetx + tileoffsetx;
            break;
        }
    }

    // step y axis
    res->ry += sy;
    int pxoffsety = sy > 0 ? h : 0;
    int tiley = floorf((res->ry + pxoffsety) / conf->tilesize);
    // get min/max tiles on opposite (x) axis
    int startx = floorf(res->rx / ftilesize);
    int endx = ceilf((res->rx + w) / ftilesize);
    // iterate over tiles along x axis
    for (int tilex = startx; tilex < endx; tilex++) {
        int passable = conf->tile(conf->context, tilex, tiley);
        if (passable == COLLIDE_TILE_IMPASSABLE) {
            int tileoffsety = sy < 0 ? conf->tilesize : 0;
            res->colly = true;
            res->ty = tiley;
            res->ry = (tiley * conf->tilesize) - pxoffsety + tileoffsety;
            break;
        } else if (passable == COLLIDE_TILE_ONEWAY) {
            // one way tiles are only collidable vertically (y axis)
            // and only _impassible_ if the collider was entirely above the tile before
            unsigned int old_y = res->ry - sy;
            if ((old_y + h) <= (tiley * conf->tilesize)) {
                int tileoffsety = sy < 0 ? conf->tilesize : 0;
                res->colly = true;
                res->ty = tiley;
                res->ry = (tiley * conf->tilesize) - pxoffsety + tileoffsety;
                break;
            }
        }
    }
}

collide_res_t collide_tilemap_check(collide_conf_t *conf, v2f *pos, v2f vel, rect_t &bounds) {
    collide_res_t res = {};

    int steps = ceilf(MAX(ABS(vel.x), ABS(vel.y)) / (float)(conf->tilesize));
    if (steps == 0) {
        steps = 1;
    }
    // split velocity into a per step velocity
    float sx = vel.x / steps;
    float sy = vel.y / steps;
    res.rx = pos->x + bounds.x;
    res.ry = pos->y + bounds.y;
    for (int i = 0; i < steps; i++) {
        _step(&res, conf, sx, sy, bounds.w, bounds.h);
        if (res.collx || res.colly) {
            break;
        }
    }
    pos->x = res.rx - bounds.x;
    pos->y = res.ry - bounds.y;
    return res;
}

bool collide_tilemap_touching_tile(collide_conf_t *config, rect_t *bounds, int tile_type) {
    float ts = config->tilesize;
    int sx = floorf(bounds->x / ts);
    int ex = ceilf((bounds->x + bounds->w) / ts);
    int sy = floorf(bounds->y / ts);
    int ey = ceilf((bounds->y + bounds->h) / ts);
    for (int i = sx; i < ex; i++) {
        for (int j = sy; j < ey; j++) {
            if (config->tile(config->context, i, j) == tile_type) {
                return true;
            }
        }
    }
    return false;
}
