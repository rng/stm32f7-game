#pragma once

#include "engine/bitmap.h"
#include "engine/rect.h"
#include "engine/tilemap.h"
#include "engine/vector.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

// Display-list based rendering system.
// The renderer maintains a list of render operations such as drawing sprites.
// The display list is constructed during a frame with the `render_push_*`
// calls. Once all render operations have been pushed, calling `render_draw`
// will render the operations to a given bitmap (eg draw the sprites).
//
// The renderer has a concept of 'screen' and 'world'. When a render op is
// requested in screen space, it is drawn relative to the top left of the
// physical screen. When an op is requested in world space, it is drawn relative
// to an arbitrary (much larger) game world origin. The physical screen's
// position in the game world is defined by the `view` parameter passed to
// `render_draw`.

// A unique id used to reference sprites in the renderer.
typedef uint16_t render_sprite_t;

struct _render_op_t;

typedef struct {
    bitmap_t *sprites;
    size_t max_sprites;
    struct _render_op_t *ops;
    size_t op_count;
} render_t;

// Initialise the renderer, allocating space for `max_sprites` number of sprites.
void render_init(render_t *render, size_t max_sprites);
// Terminate the renderer, freeing allocated memory
void render_term(render_t *render);

// Load a sprite asset into the renderer tagging it with the given sprite ID
void render_load_sprite(render_t *render, render_sprite_t id, const char *filename);

// Render operations:
// Push a sprite to be rendered at position `pos` (in world space) on layer `z` using tile `tile`.
void render_push_sprite_w(render_t *render, render_sprite_t sprite, v2f pos, uint8_t z, bool hflip,
                          uint8_t tile);
// Push a sprite to be rendered at position `pos` (in screen space) on layer `z` using tile `tile`.
void render_push_sprite_s(render_t *render, render_sprite_t sprite, v2f pos, uint8_t z, bool hflip,
                          uint8_t tile);

// Push a rectangle to rendered in screen space, using pixel `colour`
void render_push_rect_s(render_t *render, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t z,
                        uint16_t colour);

// Push a line to rendered in world space, using pixel `colour`
void render_push_line_w(render_t *render, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t z,
                        uint16_t colour);

// Push a tilemap layer to be rendered in world space
void render_push_tiles_w(render_t *render, tilemap_t *map, uint8_t layer, uint8_t z);

// Push a tilemap layer to be rendered in world space with a given parallax factor
void render_push_tiles_parallax_w(render_t *render, tilemap_t *map, uint8_t layer, uint8_t z,
                                  v2f factor);

// Render all queued render operations to `framebuffer`, then reset the display
// list.  Render operations will be rendered in their z order. Additionally
// only the operations whose positions fall into the `view` will be rendered.
void render_draw(render_t *render, bitmap_t *framebuffer, rect_t *view);
