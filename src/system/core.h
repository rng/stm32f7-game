#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <engine/platform.h>
#include <engine/sound.h>
#include <engine/ui.h>

#include "plat_common/debug.h"

enum core_state_t {
    CORE_STATE_HOME = 0,
    CORE_STATE_SETTINGS,
    CORE_STATE_IN_GAME,
};

enum core_event_t {
    CORE_EVENT_NOP,
    CORE_EVENT_CARD_IN,
    CORE_EVENT_CARD_OUT,
    CORE_EVENT_USB_CONNECT,
    CORE_EVENT_USB_DISCONNECT,
    CORE_EVENT_CHARGING,
    CORE_EVENT_DISCHARGING,
    CORE_EVENT_START,
};

enum {
    CORE_MAX_EVENTS = 8,
};

struct core_t {
    game_api_t game_api;
    struct game_state_t *game_state;
    platform_api_t platform;
    platform_params_t params;

    //
    core_state_t state;
    ui_t ui;
    sound_mixer_t mixer;
    debug_profile_t dbg_profile;
    size_t menu_idx, settings_idx;
    int settings_val[3];
    bool settings_changed;
    size_t game_count;
    const char *game_list[16];
    float dt_avg;
    int menu_hold;
    input_state_t prev_input;
    core_event_t events[CORE_MAX_EVENTS];
    int event_count;
    bool card_present, charging;
    bool show_debug;
};

void core_init(core_t &core, platform_api_t &platform, platform_params_t &params);
bool core_tick(core_t &core, input_state_t &input, uint8_t *framebuffer);
void core_audio(core_t &core, uint8_t *buf, size_t len);
void core_term(core_t &core);
void core_push_event(core_t &core, core_event_t event);
