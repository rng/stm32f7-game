#include <string.h>

#include <system/core.h>

#include "engine/common.h"
#include "engine/debug.h"
#include "engine/font.h"

#include "system/battery_16x8.h"

#define CORE_SETTINGS_FN "settings.dat"

enum settings_type_t {
    SETTINGS_SLIDER,
    SETTINGS_CHECKBOX,
};

struct settings_entry_t {
    const char *name;
    settings_type_t type;
    void (*on_change)(core_t &core, const settings_entry_t *s, int val);
};

#define PRESSED(key) (input.key && !core.prev_input.key)

static void setting_change_brightness(core_t &core, const settings_entry_t *s, int val) {
    int brightness = 15 + 48 * val; // translate 0-5 -> 15-255 (pwm values for backlight)
    platform_sys_set_brightness(brightness);
}

static void setting_change_volume(core_t &core, const settings_entry_t *s, int val) {
    int volume[] = {0, 64, 128, 192, 216, 255};
    platform_sys_set_volume(volume[val]);
}

static void setting_change_debug(core_t &core, const settings_entry_t *s, int val) {
    core.show_debug = val;
}

static const settings_entry_t settings[] = {
    {"Brightness", SETTINGS_SLIDER, setting_change_brightness},
    {"Volume", SETTINGS_SLIDER, setting_change_volume},
    {"Debug", SETTINGS_CHECKBOX, setting_change_debug},
};

#define SETTINGS_MAGIC_VALUE (0x676e7453) // 'Stng'
#define SETTINGS_VERSION (1)

struct saved_settings_t {
    uint32_t magic;
    uint32_t version;
    int32_t values[NELEMS(settings)];
};

static void settings_save(core_t &core) {
    saved_settings_t saved;
    saved.magic = SETTINGS_MAGIC_VALUE;
    saved.version = SETTINGS_VERSION;

    for (size_t i = 0; i < NELEMS(settings); i++) {
        saved.values[i] = core.settings_val[i];
    }

    platform_handle_t f = platform->file_open(CORE_SETTINGS_FN, true);
    platform->file_write(f, sizeof(saved), &saved);
    platform->file_close(f);
}

static void notify_settings_update(core_t &core) {
    // Notify system that values have changed
    for (size_t i = 0; i < NELEMS(settings); i++) {
        settings[i].on_change(core, &settings[i], core.settings_val[i]);
    }
}

static void settings_load(core_t &core) {
    // Check if settings file exists - try to load if it does
    if (platform->file_exists(CORE_SETTINGS_FN)) {
        saved_settings_t saved;
        platform_handle_t f = platform->file_open(CORE_SETTINGS_FN, false);
        platform->file_read(f, sizeof(saved), &saved);
        platform->file_close(f);
        // only load if magic value matches and we understand the file version
        if ((saved.magic == SETTINGS_MAGIC_VALUE) && (saved.version <= SETTINGS_VERSION)) {
            for (size_t i = 0; i < NELEMS(settings); i++) {
                core.settings_val[i] = saved.values[i];
            }
            notify_settings_update(core);
        }
    }
}

static void menu_choose_game(core_t &core, const char *name) {
    core.game_api = platform_sys_game_load(name);
    core.game_state = core.game_api.init(&core.platform, &core.params);
    core.state = CORE_STATE_IN_GAME;
    core.menu_hold = 0;
}

static void menu_leave_game(core_t &core) {
    core.state = CORE_STATE_HOME;
    core.prev_input.menu = true;
    core.game_api.term(core.game_state);
    core.game_state = NULL;
    platform_sys_game_unload();
    platform_sys_set_default_palette();
}

static int battery_sprite_frame(core_t &core) {
    uint32_t batt_pct = platform_sys_battery_percent();
    if (core.charging) {
        return 4;
    } else if (batt_pct >= 75) {
        return 0;
    } else if (batt_pct >= 50) {
        return 1;
    } else if (batt_pct >= 25) {
        return 2;
    } else {
        return 3;
    }
}

static bool menu_tick(core_t &core, input_state_t &input, uint8_t *framebuffer) {
    bitmap_t screen = BITMAP8(core.params.screen_w, core.params.screen_h, framebuffer);
    bitmap_fill(&screen, 0);

    ui_start(core.ui, screen, input, ui_default_config);

    // title bar
    ui_container_row(core.ui, 3, 9);
    ui_label(core.ui, " +");
    ui_label(core.ui, core.state == CORE_STATE_HOME ? "Games" : "Settings", UI_ALIGN_CENTRE);
    ui_sprite(core.ui, battery, battery_sprite_frame(core), UI_ALIGN_RIGHT);

    // main content
    ui_container_row(core.ui);
    ui_container_panel(core.ui, {10, 10});

    if (core.state == CORE_STATE_HOME) {
        // home / games screen
        if (core.game_count) {
            for (size_t i = 0; i < core.game_count; i++) {
                ui_container_row(core.ui, 1, 14);
                if (ui_button(core.ui, core.game_list[i], UI_ALIGN_CENTRE)) {
                    menu_choose_game(core, core.game_list[i]);
                }
            }
        } else {
            ui_container_row(core.ui, 1, 14);
            const char *msg = core.card_present ? "NO GAMES FOUND" : "NO SD CARD";
            ui_label(core.ui, msg, UI_ALIGN_CENTRE, UI_STYLE_DISABLED);
        }
        // settings button
        ui_container_row(core.ui, 1, 14);
        if (ui_button(core.ui, "Settings", UI_ALIGN_CENTRE)) {
            core.state = CORE_STATE_SETTINGS;
            ui_setcursor(core.ui, {0, 0});
        }

    } else if (core.state == CORE_STATE_SETTINGS) {
        // settings screen
        for (size_t i = 0; i < NELEMS(settings); i++) {
            const settings_entry_t *setting = &settings[i];
            ui_container_row(core.ui, 1, 14);
            bool change = false;
            switch (setting->type) {
                case SETTINGS_SLIDER:
                    change = ui_slider(core.ui, setting->name, core.settings_val[i], 0, 5);
                    break;
                case SETTINGS_CHECKBOX:
                    change = ui_checkbox(core.ui, setting->name, core.settings_val[i]);
                    break;
            }
            if (change) {
                setting->on_change(core, setting, core.settings_val[i]);
                sound_play_note(&core.mixer, 0, SOUND_NOTE_C5, 192);
            }
        }
        // build id
        ui_container_row(core.ui, 2, 14);
        ui_label(core.ui, "Build id");
        ui_label(core.ui, BUILD_SHA, UI_ALIGN_RIGHT, UI_STYLE_DISABLED);
        // back button
        ui_container_row(core.ui, 1, 14);
        if (ui_button(core.ui, "< Back")) {
            settings_save(core);
            core.state = CORE_STATE_HOME;
            ui_setcursor(core.ui, {0, 0});
            settings_save(core);
        }
    }

    if (ui_end(core.ui)) {
        sound_play_note(&core.mixer, 0, SOUND_NOTE_C4, 192);
    }

    if (PRESSED(menu)) {
        return true;
    }

    core.prev_input = input;
    return false;
}

static int string_sort_func(const void *a, const void *b) {
    const char *ac = *(const char **)a;
    const char *bc = *(const char **)b;
    return strcmp(ac, bc);
}

void core_init(core_t &core, platform_api_t &platform, platform_params_t &params) {
    core.platform = platform;
    core.params = params;
    core.state = CORE_STATE_HOME;
    core.game_state = NULL;
    core.card_present = false;
    core.charging = false;
    core.show_debug = false;
    core.menu_idx = 0;

    // default settings
    core.settings_val[0] = 3; // brightness
    core.settings_val[1] = 3; // volume
    core.settings_val[2] = 1; // debug

    sound_init(&core.mixer, params.audio_rate, 1);
    sound_instrument_synth(&core.mixer, 0, SOUND_WAVEFORM_ORG, 50, 500);

    debug_profile_init(&core.dbg_profile, params.screen_w, 16);
}

static void core_handle_event(core_t &core, core_event_t event) {
    switch (event) {
        case CORE_EVENT_CARD_IN:
            core.card_present = true;
            core.game_count = NELEMS(core.game_list);
            settings_load(core);
            platform_sys_games_list(core.game_list, core.game_count);
            qsort(core.game_list, core.game_count, sizeof(const char *), string_sort_func);

            if (core.game_count == 1) {
                menu_choose_game(core, core.game_list[0]);
            }
            break;
        case CORE_EVENT_CARD_OUT:
            core.game_count = 0;
            core.card_present = false;
            if (core.state == CORE_STATE_IN_GAME) {
                menu_leave_game(core);
            }
            break;
        case CORE_EVENT_CHARGING:
            core.charging = true;
            break;
        case CORE_EVENT_DISCHARGING:
            core.charging = false;
            break;
        case CORE_EVENT_START:
            notify_settings_update(core);
            break;
        default:
            break;
    }
}

static bool core_inner_tick(core_t &core, input_state_t &input, uint8_t *framebuffer) {
    DEBUG_TIMED_BLOCK("tick");
    while (core.event_count) {
        core_handle_event(core, core.events[--core.event_count]);
    }

    if (core.state == CORE_STATE_IN_GAME) {
        if (input.menu) {
            core.menu_hold++;
            if (core.menu_hold > 30) {
                menu_leave_game(core);
                return false;
            }
        } else {
            core.menu_hold = 0;
        }
        return core.game_api.tick(core.game_state, &input, framebuffer);
    } else {
        return menu_tick(core, input, framebuffer);
    }
}

bool core_tick(core_t &core, input_state_t &input, uint8_t *framebuffer) {
    bitmap_t screen = BITMAP8(core.params.screen_w, core.params.screen_h, framebuffer);

    bool ret = core_inner_tick(core, input, framebuffer);
    if (core.show_debug) {
        DEBUG_TIMED_BLOCK("profgraph");
        // profile graph
        bool fullmode = input.c && input.d;
        debug_profile_render(&core.dbg_profile, &screen, fullmode);
        // FPS counter
        const float alpha = 0.95;
        core.dt_avg = (core.dt_avg * alpha) + (input.dt * (1 - alpha));
        int fps = 1.f / core.dt_avg;
        font_printf_col(&screen, font_system(), 1, screen.height - 7, 254, "%d", fps);
    }
    return ret;
}

void core_audio(core_t &core, uint8_t *buf, size_t buf_len) {
    if (core.state == CORE_STATE_IN_GAME) {
        core.game_api.audio(core.game_state, buf, buf_len);
    } else {
        sound_mix(&core.mixer, buf, buf_len);
    }
}

void core_term(core_t &core) {
    if (core.game_state) {
        core.game_api.term(core.game_state);
    }
}

void core_push_event(core_t &core, core_event_t event) {
    int pos = __sync_fetch_and_add(&core.event_count, 1);
    ASSERT_MSG(pos < CORE_MAX_EVENTS, "core event overflow");
    core.events[pos] = event;
}
