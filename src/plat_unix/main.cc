#include <dlfcn.h>
#include <stdlib.h>
#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "engine/engine.h"

// defined in plat_pc/sdl.cc
extern void sdl_main_init(int argc, char *argv[]);
extern void sdl_main_tick();
extern void sdl_main_term();
bool sdl_main_done();

static void *game_lib;

const char *os_game_lib_name(void) {
    return "game.so";
}

size_t os_alloc_size(void *ptr) {
#ifdef __APPLE__
    return malloc_size(ptr);
#else
    return malloc_usable_size(ptr);
#endif
}

game_api_t os_load_game_code(const char *game_fn) {
    game_lib = dlopen(game_fn, RTLD_NOW);
    char *error = dlerror();
    ASSERT_MSG(error == NULL, "Error loading game shared lib: %s", error);

    get_game_api_fn_t get_game_api = (get_game_api_fn_t)dlsym(game_lib, "get_game_api");
    error = dlerror();
    ASSERT_MSG(error == NULL, "Error getting game API from game");
    return get_game_api();
}

void os_unload_game_code(void) {
    if (game_lib) {
        dlclose(game_lib);
    }
}

uint64_t os_get_usec(void) {
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return (t.tv_sec * 1000000) + (t.tv_nsec / 1000);
}

int main(int argc, char *argv[]) {
    sdl_main_init(argc, argv);

    while (!sdl_main_done()) {
        sdl_main_tick();
    }

    sdl_main_term();
    return EXIT_SUCCESS;
}
