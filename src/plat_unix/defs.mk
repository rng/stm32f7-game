# vim: set ft=make:

OPT?=g
SDL_CFLAGS := $(shell sdl2-config --cflags)
SDL_LDFLAGS := $(shell sdl2-config --libs)
PLATFLAGS := $(SDL_CFLAGS) -fPIC -DPLAT_ASSERT_PRINT=1 -DTINYPRINTF_OVERRIDE_LIBC=0
PLATFLAGS += -fsanitize=address
LDFLAGS := -lm -ldl -lpthread $(SDL_LDFLAGS) $(PLATFLAGS)
GAMELDFLAGS := -shared
GDB = gdb
SIZE := size
STRIP := strip

SOEXT=.so

# tinyprintf library
TINYPRINTF_SRCS:= \
    $(BASE_DIR)/src/lib/tinyprintf/tinyprintf.c \

# Platform and startup code
PLATSRCS:= \
    $(BASE_DIR)/src/arch_pc/sdl.cc \
    $(BASE_DIR)/src/plat_unix/main.cc \
    $(BASE_DIR)/src/system/core.cc \
    $(BASE_DIR)/src/engine/bitmap.cc \
    $(BASE_DIR)/src/engine/font.cc \
    $(TINYPRINTF_SRCS) \

INCS:=\
    -I$(BASE_DIR)/src/lib/jo_gif \
    -I$(BASE_DIR)/src/lib/tinyprintf \
    -I$(BASE_DIR)/src
