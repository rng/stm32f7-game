# vim: set ft=make:

OPT?=1

SDL_CFLAGS := -s USE_SDL=2
SDL_LDFLAGS := -s USE_SDL=2
PLATFLAGS = $(SDL_CFLAGS) -DPLAT_ASSERT_PRINT=1
LDFLAGS := -lm $(SDL_LDFLAGS) -g
PLATLDFLAGS := --preload-file build/web/$(GAME_DIR)@./$(GAME_DIR)
GAMELDFLAGS := --no-entry

CC:=emcc
CXX:=em++
STRIP:=true

SOEXT=.wasm
BINEXT=.js

# tinyprintf library
TINYPRINTF_SRCS:= \
    $(BASE_DIR)/src/lib/tinyprintf/tinyprintf.c \

# Platform and startup code
PLATSRCS:= \
    $(BASE_DIR)/src/arch_pc/sdl.cc \
    $(BASE_DIR)/src/plat_web/main.cc \
    $(BASE_DIR)/src/system/core.cc \
    $(BASE_DIR)/src/engine/bitmap.cc \
    $(BASE_DIR)/src/engine/font.cc \
    $(TINYPRINTF_SRCS) \

INCS:=\
    -I$(BASE_DIR)/src/lib/jo_gif \
    -I$(BASE_DIR)/src/lib/tinyprintf \
    -I$(BASE_DIR)/src

PLATTARGETS := index.html gameloader.wasm gameloader.data
