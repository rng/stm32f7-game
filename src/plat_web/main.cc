#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "engine/engine.h"
#include "plat_common/config.h"

#include "emscripten.h"

extern "C" game_api_t get_game_api(void);

const char *os_game_lib_name(void) {
    return "game.so";
}

size_t os_alloc_size(void *ptr) {
    return malloc_usable_size(ptr);
}

game_api_t os_load_game_code(const char *game_fn) {
    return get_game_api();
}

void os_unload_game_code(void) {
}

uint64_t os_get_usec(void) {
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return (t.tv_sec * 1000000) + (t.tv_nsec / 1000);
}

extern void sdl_main_tick(void);
extern void sdl_main_init(int argc, char *argv[]);
extern void sdl_main_tick();
extern void sdl_main_term();
bool sdl_main_done();

int main(int argc, char *argv[]) {
    sdl_main_init(argc, argv);

    emscripten_set_main_loop(sdl_main_tick, TARGET_FRAMERATE, 1);

    sdl_main_term();
    return EXIT_SUCCESS;
}
