#include "usbd_storage_if.h"
#include "bsp_driver_sd.h"

#define SD_TIMEOUT 30 * 1000

// clang-format off
const int8_t STORAGE_Inquirydata_HS[] = {
    0x00, 0x80, 0x02, 0x02, (STANDARD_INQUIRY_DATA_LEN - 5), 0x00, 0x00, 0x00, 
    'S',  'T', 'M',  ' ',  ' ',  ' ',  ' ', ' ', /* Manufacturer : 8 bytes  */
    'P',  'r', 'o',  'd',  'u', 'c',  't',  ' ', /* Product      : 16 Bytes */
    ' ',  ' ', ' ',  ' ',  ' ', ' ',  ' ',  ' ', /*                         */
    '0',  '.', '0',  '1'                         /* Version      : 4 Bytes  */
};
// clang-format on

extern USBD_HandleTypeDef hUsbDeviceHS;

int8_t STORAGE_Init_HS(uint8_t lun) {
    return (USBD_OK);
}

int8_t STORAGE_GetCapacity_HS(uint8_t lun, uint32_t *block_num, uint16_t *block_size) {
    HAL_SD_CardInfoTypeDef info;
    int8_t ret = -1;

    if (BSP_SD_IsDetected() != SD_NOT_PRESENT) {
        BSP_SD_GetCardInfo(&info);

        *block_num = info.LogBlockNbr - 1;
        *block_size = info.LogBlockSize;
        ret = 0;
    }
    return ret;
}

int8_t STORAGE_IsReady_HS(uint8_t lun) {
    static int8_t prev_status = 0;
    int8_t ret = -1;

    if (BSP_SD_IsDetected() != SD_NOT_PRESENT) {
        if (prev_status < 0) {
            BSP_SD_Init();
            prev_status = 0;
        }
        if (BSP_SD_GetCardState() == SD_TRANSFER_OK) {
            ret = 0;
        }
    } else if (prev_status == 0) {
        prev_status = -1;
    }

    return ret;
}

int8_t STORAGE_IsWriteProtected_HS(uint8_t lun) {
    return (USBD_OK);
}

int8_t STORAGE_Read_HS(uint8_t lun, uint8_t *buf, uint32_t blk_addr, uint16_t blk_len) {
    int8_t ret = -1;

    if (BSP_SD_IsDetected() != SD_NOT_PRESENT) {
        BSP_SD_ReadBlocks((uint32_t *)buf, blk_addr, blk_len, SD_TIMEOUT);

        /* Wait until SD card is ready to use for new operation */
        while (BSP_SD_GetCardState() != SD_TRANSFER_OK) {
        }

        ret = 0;
    }
    return ret;
}

int8_t STORAGE_Write_HS(uint8_t lun, uint8_t *buf, uint32_t blk_addr, uint16_t blk_len) {
    int8_t ret = -1;

    if (BSP_SD_IsDetected() != SD_NOT_PRESENT) {
        BSP_SD_WriteBlocks((uint32_t *)buf, blk_addr, blk_len, SD_TIMEOUT);

        /* Wait until SD card is ready to use for new operation */
        while (BSP_SD_GetCardState() != SD_TRANSFER_OK) {
        }

        ret = 0;
    }
    return ret;
}

int8_t STORAGE_GetMaxLun_HS(void) {
    return 0;
}

USBD_StorageTypeDef USBD_Storage_Interface_fops = {
    STORAGE_Init_HS, STORAGE_GetCapacity_HS, STORAGE_IsReady_HS,   STORAGE_IsWriteProtected_HS,
    STORAGE_Read_HS, STORAGE_Write_HS,       STORAGE_GetMaxLun_HS, (int8_t *)STORAGE_Inquirydata_HS,
};
