#include "hw.h"
#include "engine/common.h"
#include "faulthandler.h"
#include "main.h"

#include "usbd_msc.h"

#if BOARD_revd
extern PCD_HandleTypeDef hpcd_USB_OTG_FS;
#else
extern PCD_HandleTypeDef hpcd_USB_OTG_HS;
#endif

extern DMA_HandleTypeDef hdma_dac1;
extern DAC_HandleTypeDef hdac;
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim6;
extern ADC_HandleTypeDef hadc1;
extern DMA_HandleTypeDef hdma_spi1_tx;
extern RNG_HandleTypeDef hrng;

uint32_t batt_volt_avg;
uint8_t usbd_heap[sizeof(USBD_MSC_BOT_HandleTypeDef)];

extern void app_fault_handler(fault_info_t *fault_info);

void fault_handler(exception_regs_t *regs) {
    fault_info_t fault = {};
    fault.core_regs = *regs;
    fault.sys_regs.sp = (uint32_t)regs;
    fault.sys_regs.hfsr = SCB->HFSR;
    fault.sys_regs.cfsr = SCB->CFSR;
    fault.sys_regs.mmfar = SCB->MMFAR;
    fault.sys_regs.bfar = SCB->BFAR;

    app_fault_handler(&fault);
    __asm volatile(" bkpt 0 \n");
}

__attribute__((naked)) void fault_handler_common(void) {
    __asm volatile(" tst lr, #4        \n"
                   " ite eq            \n"
                   " mrseq r0, msp     \n"
                   " mrsne r0, psp     \n"
                   " ldr r1, [r0, #24] \n"
                   " b fault_handler   \n");
}

void NMI_Handler(void) {
}

void HardFault_Handler(void) {
    fault_handler_common();
}

void MemManage_Handler(void) {
    fault_handler_common();
}

void BusFault_Handler(void) {
    fault_handler_common();
}

void UsageFault_Handler(void) {
    fault_handler_common();
}

void SVC_Handler(void) {
}

void DebugMon_Handler(void) {
}

void PendSV_Handler(void) {
}

void SysTick_Handler(void) {
    HAL_IncTick();
    HAL_SYSTICK_IRQHandler();
}

#if BOARD_revd
void OTG_FS_IRQHandler(void) {
    HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS);
}
#else
void OTG_HS_IRQHandler(void) {
    HAL_PCD_IRQHandler(&hpcd_USB_OTG_HS);
}
#endif

void DMA1_Stream5_IRQHandler(void) {
    HAL_DMA_IRQHandler(&hdma_dac1);
}

void TIM6_DAC_IRQHandler(void) {
    HAL_DAC_IRQHandler(&hdac);
    HAL_TIM_IRQHandler(&htim6);
}

void DMA2_Stream3_IRQHandler(void) {
    HAL_DMA_IRQHandler(&hdma_spi1_tx);
}

void Error_Handler(void) {
    ASSERT(false);
}

void *usbd_alloc(size_t size) {
    ASSERT(size <= sizeof(usbd_heap));
    return usbd_heap;
}

void usbd_free(void *ptr) {
}

static void dwt_init(void) {
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
    __DSB();
    DWT->LAR = 0xC5ACCE55;
    __DSB();
    DWT->CTRL &= ~DWT_CTRL_CYCCNTENA_Msk;
    DWT->CYCCNT = 0;
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
}

bool hw_debugger_attached(void) {
    return CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk;
}

__attribute__((naked)) void hw_semihosting_cmd(int cmd, void *arg) {
    __asm volatile(" bkpt 0xab \n"
                   " bx lr     \n");
}

void hw_speaker_enable(bool en) {
    HAL_GPIO_WritePin(GPIOA, SPK_EN_Pin, en ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void hw_led_on(int which, int val) {
#if BOARD_revb
    if (which == LED_GREEN) {
        HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, val ? GPIO_PIN_SET : GPIO_PIN_RESET);
    }
#elif BOARD_revc
    if (which == LED_GREEN) {
        HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, val ? GPIO_PIN_RESET : GPIO_PIN_SET);
    } else if (which == LED_BLUE) {
        HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, val ? GPIO_PIN_RESET : GPIO_PIN_SET);
    }
#elif BOARD_revd
    if (which == LED_GREEN) {
        HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, val ? GPIO_PIN_SET : GPIO_PIN_RESET);
    }
#else
#error "unhandled board revision"
#endif
}

uint32_t hw_battery_volt(void) {
    const float alpha = 0.1;
    HAL_ADC_Start(&hadc1);
    HAL_ADC_PollForConversion(&hadc1, 1000);
    uint32_t raw = HAL_ADC_GetValue(&hadc1);
    // Raw value from 12-bit ADC is measuring against a 3.3V reference
    // through a /2 resistor divider.
    batt_volt_avg = (batt_volt_avg * alpha) + (raw * (1 - alpha));
    return (2 * 3300 * batt_volt_avg) / (1 << 12);
}

void hw_backlight_set(uint8_t val) {
    TIM_OC_InitTypeDef sConfigOC = {};
    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = val;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
    sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
    ASSERT(HAL_TIM_OC_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) == HAL_OK);
    HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
}

void hw_audio_start(uint16_t *buf, size_t buf_len) {
    HAL_TIM_Base_Start(&htim6);
    HAL_DAC_Start(&hdac, DAC_CHANNEL_1);
    // HAL_DAC_Start_DMA takes size in number of uint16 values
    HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t *)buf, buf_len / 2, DAC_ALIGN_12B_L);
}

uint32_t hw_rand(void) {
    uint32_t val;
    HAL_RNG_GenerateRandomNumber(&hrng, &val);
    return val;
}

extern void SystemClock_Config(void);
extern void MX_GPIO_Init(void);
extern void MX_DMA_Init(void);
extern void MX_SDMMC1_SD_Init(void);
extern void MX_ADC1_Init(void);
extern void MX_DAC_Init(void);
extern void MX_SPI1_Init(void);
extern void MX_TIM1_Init(void);
extern void MX_TIM6_Init(void);
extern void MX_USART1_UART_Init(void);
extern void MX_NVIC_Init(void);

void hw_init(void) {
    SCB_InvalidateICache();
    SCB_EnableICache();
    SCB_EnableDCache();

    HAL_Init();
    SystemClock_Config();

    MX_GPIO_Init();
    MX_DMA_Init();
    MX_SDMMC1_SD_Init();
    MX_ADC1_Init();
    MX_DAC_Init();
    MX_SPI1_Init();
    MX_TIM1_Init();
    MX_TIM6_Init();
    MX_USART1_UART_Init();
    MX_NVIC_Init();
    dwt_init();
}
