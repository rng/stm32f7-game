#include "bsp_driver_lcd.h"
#include "main.h"

#include <stdbool.h>

#define ST_CMD_DELAY 0x80 // special signifier for command lists

#define ST77XX_SWRESET 0x01

#define ST77XX_SLPIN 0x10
#define ST77XX_SLPOUT 0x11
#define ST77XX_PTLON 0x12
#define ST77XX_NORON 0x13

#define ST77XX_INVOFF 0x20
#define ST77XX_INVON 0x21
#define ST77XX_DISPOFF 0x28
#define ST77XX_DISPON 0x29
#define ST77XX_CASET 0x2A
#define ST77XX_RASET 0x2B
#define ST77XX_RAMWR 0x2C
#define ST77XX_RAMRD 0x2E

#define ST77XX_COLMOD 0x3A
#define ST77XX_MADCTL 0x36

#define ST7735_FRMCTR1 0xB1
#define ST7735_FRMCTR2 0xB2
#define ST7735_FRMCTR3 0xB3
#define ST7735_INVCTR 0xB4

#define ST7735_PWCTR1 0xC0
#define ST7735_PWCTR2 0xC1
#define ST7735_PWCTR3 0xC2
#define ST7735_PWCTR4 0xC3
#define ST7735_PWCTR5 0xC4
#define ST7735_VMCTR1 0xC5

#define ST7735_GMCTRP1 0xE0
#define ST7735_GMCTRN1 0xE1

extern SPI_HandleTypeDef hspi1;

// clang-format off
static const uint8_t st7735r_init_cmds[] = {
    19,                           // command count

    ST77XX_SLPOUT, ST_CMD_DELAY,  //  Out of sleep mode
    150,                          //  | 150 ms delay
    ST7735_FRMCTR1, 3,            //  Framerate ctrl - normal mode
    0x01, 0x2C, 0x2D,             //  | Rate ~= 71hz
    ST7735_FRMCTR2, 3,            //  Framerate ctrl - idle mode
    0x01, 0x2C, 0x2D,             //  | Rate ~= 71hz
    ST7735_FRMCTR3, 6,            //  Framerate - partial mode
    0x01, 0x2C, 0x2D,             //  | Dot inversion mode
    0x01, 0x2C, 0x2D,             //  | Line inversion mode
    ST7735_INVCTR, 1,             //  Display inversion ctrl
    0x07,                         //  | No inversion
    ST7735_PWCTR1, 3,             //  Power control
    0xA2, 0x02,                   //  | -4.6V
    0x84,                         //  | AUTO mode
    ST7735_PWCTR2, 1,             //  Power control
    0xC5,                         //  | VGH25=2.4C VGSEL=-10 VGH=3 * AVDD
    ST7735_PWCTR3, 2,             //  Power control
    0x0A,                         //  | Opamp current small
    0x00,                         //  | Boost frequency
    ST7735_PWCTR4, 2,             //  Power control
    0x8A,                         //  | BCLK/2,
    0x2A,                         //  | opamp current small & medium low
    ST7735_PWCTR5, 2,             //  Power control
    0x8A, 0xEE,                   //  |
    ST7735_VMCTR1, 1,             //  Power control
    0x0E,                         //  |
    ST77XX_INVOFF, 0,             //  Don't invert display
    ST77XX_MADCTL, 1,             //  Mem access ctl (directions)
    0xC8,                         //  | row/col addr, bottom-top refresh
    ST77XX_COLMOD, 1,             //  set color mode
    0x05,                         //  | 16-bit color
    ST77XX_CASET, 4,              //  Column addr set
    0x00, 0x00,                   //  | XSTART = 0
    0x00, 0x7F,                   //  | XEND = 127
    ST77XX_RASET, 4,              //  Row addr set
    0x00, 0x00,                   //  | XSTART = 0
    0x00, 0x7F,                   //  | XEND = 127
    ST7735_GMCTRP1, 16,           //  Gamma Adjustments (pos. polarity)
    0x02, 0x1c, 0x07, 0x12,       //  |
    0x37, 0x32, 0x29, 0x2d,       //  |
    0x29, 0x25, 0x2B, 0x39,       //  | 
    0x00, 0x01, 0x03, 0x10,       //  | 
    ST7735_GMCTRN1, 16,           //  Gamma Adjustments (neg. polarity)
    0x03, 0x1d, 0x07, 0x06,       //  |
    0x2E, 0x2C, 0x29, 0x2D,       //  |
    0x2E, 0x2E, 0x37, 0x3F,       //  | 
    0x00, 0x00, 0x02, 0x10,       //  | 
    ST77XX_NORON, ST_CMD_DELAY,   //  Normal display on
    10,                           //  | 10 ms delay
};
// clang-format on

#define CS(v) HAL_GPIO_WritePin(DISP_CS_GPIO_Port, DISP_CS_Pin, v ? GPIO_PIN_SET : GPIO_PIN_RESET);
#define DC(v) HAL_GPIO_WritePin(DISP_DC_GPIO_Port, DISP_DC_Pin, v ? GPIO_PIN_SET : GPIO_PIN_RESET);
#define RST(v) \
    HAL_GPIO_WritePin(DISP_RST_GPIO_Port, DISP_RST_Pin, v ? GPIO_PIN_SET : GPIO_PIN_RESET);

static void write_cmd(uint8_t cmd) {
    DC(0);
    HAL_SPI_Transmit(&hspi1, &cmd, 1, HAL_MAX_DELAY);
}

static void write_data(uint8_t *data, size_t len) {
    DC(1);
    HAL_SPI_Transmit(&hspi1, data, len, HAL_MAX_DELAY);
}

volatile int dma_spi_busy;

void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi) {
    dma_spi_busy = 0;
    CS(1);
}

static void write_data_dma(uint8_t *data, size_t len) {
    DC(1);
    dma_spi_busy = 1;
    HAL_SPI_Transmit_DMA(&hspi1, data, len);
}

void lcd_init(void) {
    CS(0);
    RST(0);
    HAL_Delay(10);
    RST(1);
    HAL_Delay(150);

    // Send init commands
    const uint8_t *cmds = st7735r_init_cmds;
    uint8_t cmd_count = *cmds++;
    while (cmd_count--) {
        uint8_t cmd = *cmds++;                  // Read command
        uint8_t arg_count = *cmds++;            // Number of args to follow
        uint16_t ms = arg_count & ST_CMD_DELAY; // If hibit set, delay follows args
        arg_count &= ~ST_CMD_DELAY;             // Mask out delay bit
        write_cmd(cmd);
        if (arg_count) {
            write_data((uint8_t *)cmds, arg_count);
        }
        cmds += arg_count;

        if (ms) {
            ms = *cmds++; // Read post-command delay time (ms)
            HAL_Delay(ms);
        }
    }
    CS(1);
    // clear LCD RAM
    lcd_erase();
    // turn display on
    CS(0)
    write_cmd(ST77XX_DISPON);
    CS(1);
    HAL_Delay(100);
}

static void set_window(int x, int y, int w, int h) {
    uint8_t valx[] = {0, x, 0, x + w - 1};
    uint8_t valy[] = {0, y, 0, y + h - 1};
    write_cmd(ST77XX_CASET);
    write_data(valx, sizeof(valx));
    write_cmd(ST77XX_RASET);
    write_data(valy, sizeof(valy));
    write_cmd(ST77XX_RAMWR);
}

void lcd_erase(void) {
    uint16_t blank[66] = {};
    CS(0);
    set_window(0, 0, 132, 132);
    for (int i = 0; i < (132 * 132 / 66); i++) {
        write_data((uint8_t *)blank, sizeof(blank));
    }
    CS(1);
}

void lcd_draw_bitmap(uint16_t *data, int x, int y, int w, int h) {
    while (dma_spi_busy) {
    }
    CS(0);
    set_window(x, y, w, h);
    write_data_dma((uint8_t *)data, h * w * 2);
}
