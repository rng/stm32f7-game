#include <malloc.h>

#include "main.h"
#include "stm32f7xx_hal.h"
#include "usb_device.h"

#include "ff.h"
#include "ff_gen_drv.h"
#include "sd_diskio.h"

#include "bsp_driver_lcd.h"
#include "bsp_driver_sd.h"
#include "faulthandler.h"
#include "hw.h"
#include "tinyprintf.h"

#include "plat_common/config.h"
#include "plat_common/debug.h"

#include "engine/bitmap.h"
#include "engine/common.h"
#include "engine/debug.h"
#include "engine/input.h"
#include "engine/platform.h"
#include "engine/syspalette.h"
#include "plat_common/debug.h"
#include "system/core.h"

#include "arch_stm32/flash.h"

#include <stdarg.h>

const float TICK_INTERVAL = 1000 / (float)TARGET_FRAMERATE;

#define GAME_CODE_LOAD_ADDR 0x8020000 // location in flash of game code
#define LCD_SCALE (1)
#define AUDIO_SAMPLES (2 * 1024)

enum dma_bufpos_t {
    NONE,
    HALF,
    FULL,
};

USBD_HandleTypeDef hUsbDeviceHS;

#define FP_POOL_SIZE 2

struct fp_item_t {
    FIL fp;
    bool inuse;
};

struct game_info_t {
    char name[16];
};

struct main_priv_t {
    uint16_t framebuffer[SCREEN_WIDTH * LCD_SCALE * SCREEN_HEIGHT * LCD_SCALE];
    uint8_t backbuffer[SCREEN_WIDTH * SCREEN_HEIGHT];
    uint16_t audiobuf[AUDIO_SAMPLES];
    volatile dma_bufpos_t bufpos;
    char game_name[32];
    game_info_t games[8];
    core_t core;

    char SDPath[4];
    FATFS SDFatFs;
    fp_item_t fp_pool[FP_POOL_SIZE];
    uint16_t palette[256];
    uint32_t battery_tick, battery_mv;
    uint32_t audio_pop;
    uint8_t volume;
    float next_time;

    struct {
        size_t cur, frame_alloc, frame_free;
    } allocs;
};

main_priv_t main_priv;
platform_api_t *platform;

static float get_ticks() {
    return HAL_GetTick();
}

static int fp_pool_alloc() {
    for (int i = 0; i < FP_POOL_SIZE; i++) {
        if (!main_priv.fp_pool[i].inuse) {
            main_priv.fp_pool[i].inuse = true;
            return i + 1;
        }
    }
    ASSERT_MSG(false, "Cannot alloc fp item");
}

static void fp_pool_free(int idx) {
    main_priv.fp_pool[idx - 1].inuse = false;
}

static void basedir_filename(char *fn, size_t fn_size, const char *filename) {
    strncat(fn, main_priv.game_name, fn_size - strlen(fn) - 1);
    strncat(fn, "/", fn_size - strlen(fn) - 1);
    strncat(fn, filename, fn_size - strlen(fn) - 1);
}

void *platform_mem_alloc(size_t size) {
    main_priv.allocs.cur += size;
    main_priv.allocs.frame_alloc += size;
    return calloc(size, 1);
}

void platform_mem_free(void *ptr) {
    size_t size = malloc_usable_size(ptr);
    main_priv.allocs.cur -= size;
    main_priv.allocs.frame_free += size;
    free(ptr);
}

bool platform_file_exists(const char *filename) {
    char fn[128] = {0};
    FILINFO file_info;
    basedir_filename(fn, sizeof(fn), filename);
    return (f_stat(fn, &file_info) == FR_OK);
}

platform_handle_t platform_file_open(const char *filename, bool write) {
    char fn[128] = {0};
    basedir_filename(fn, sizeof(fn), filename);
    int idx = fp_pool_alloc();
    FIL *fp = &main_priv.fp_pool[idx - 1].fp;
    FRESULT res = f_open(fp, fn, write ? FA_CREATE_ALWAYS | FA_WRITE : FA_READ);
    ASSERT_MSG(res == FR_OK, "Cannot open asset file %s", fn);
    return (platform_handle_t)idx;
}

size_t platform_file_read(platform_handle_t h, size_t n, void *b) {
    ASSERT(h);
    FIL *fp = &main_priv.fp_pool[(int)h - 1].fp;
    size_t nread = 0;
    FRESULT res = f_read(fp, b, n, &nread);
    ASSERT_MSG(res == FR_OK, "Cannot read asset file");
    return nread;
}

size_t platform_file_write(platform_handle_t h, size_t n, void *b) {
    ASSERT(h);
    FIL *fp = &main_priv.fp_pool[(int)h - 1].fp;
    size_t nwritten;
    FRESULT res = f_write(fp, b, n, &nwritten);
    ASSERT_MSG(res == FR_OK, "Cannot write asset file");
    return nwritten;
}

void platform_file_close(platform_handle_t h) {
    FIL *fp = &main_priv.fp_pool[(int)h - 1].fp;
    FRESULT res = f_close(fp);
    fp_pool_free((int)h);
    ASSERT_MSG(res == FR_OK, "Cannot close asset file");
}

size_t platform_file_size(platform_handle_t h) {
    FIL *fp = &main_priv.fp_pool[(int)h - 1].fp;
    return f_size(fp);
}

void platform_gfx_set_palette(uint8_t *colours, size_t count) {
    ASSERT_MSG(count <= 256, "palette too large");
    for (size_t i = 0; i < count; i++) {
        uint8_t r = colours[i * 3 + 0];
        uint8_t g = colours[i * 3 + 1];
        uint8_t b = colours[i * 3 + 2];
        uint16_t col = ((r >> 3) << 11) | ((g >> 2) << 5) | (b >> 3);
        main_priv.palette[i] = __builtin_bswap16(col);
    }
}

uint64_t platform_debug_get_usec(void) {
    return DWT->CYCCNT / (SystemCoreClock / 1000000L);
}

void platform_debug_printf(const char *fmt, ...) {
    char line[256];
    va_list arglist;

    va_start(arglist, fmt);
    int len = tfp_vsnprintf(line, sizeof(line), fmt, arglist);
    if (hw_debugger_attached()) {
        // semihosting cmd buffer to write to stdout
        uint32_t cmd[3] = {1, (uint32_t)line, (uint32_t)len};
        hw_semihosting_cmd(SEMIHOSTING_CMD_WRITE, (void *)cmd);
    }
    va_end(arglist);
}

static bool is_gamedir(const char *dirname) {
    char path[256] = {0};
    strncat(path, dirname, sizeof(path) - strlen(dirname) - 1);
    strncat(path, "/", sizeof(path) - strlen(dirname) - 1);
    strncat(path, "game.bin", sizeof(path) - strlen(dirname) - 1);

    FILINFO fno;
    return f_stat(path, &fno) == FR_OK;
}

void platform_sys_games_list(const char *games[], size_t &games_max) {
    DIR dj;
    FILINFO fno;
    FRESULT fr = f_findfirst(&dj, &fno, "", "*");

    size_t game_count = 0;
    while ((fr == FR_OK) && (fno.fname[0]) && (game_count < games_max)) {
        if (is_gamedir(fno.fname)) {
            game_info_t *game = &main_priv.games[game_count];
            strncpy(game->name, fno.fname, sizeof(game->name));
            games[game_count++] = game->name;
        }
        fr = f_findnext(&dj, &fno);
    }

    f_closedir(&dj);
    games_max = game_count;
}

static void flash_game_code_progress(int percent) {
}

static game_api_t load_game_code(void) {
#ifndef SKIP_GAME_FLASH
    flash_check_code_from_sd(GAME_CODE_LOAD_ADDR, "game.bin", flash_game_code_progress);
#endif
    // skip header and set low bit for a thumb call
    uintptr_t game_addr = (GAME_CODE_LOAD_ADDR + sizeof(game_header_t)) | 0x1;
    get_game_api_fn_t get_game_api = (get_game_api_fn_t)(game_addr);
    return get_game_api();
}

game_api_t platform_sys_game_load(const char *filename) {
    strncpy(main_priv.game_name, filename, sizeof(main_priv.game_name));
    return load_game_code();
}

void platform_sys_game_unload(void) {
    strncpy(main_priv.game_name, "", sizeof(main_priv.game_name));
}

void platform_sys_set_default_palette(void) {
    platform_gfx_set_palette((uint8_t *)syspalette.colours, syspalette.count);
}

uint32_t platform_sys_battery_percent(void) {
    if ((HAL_GetTick() - main_priv.battery_tick) > 100) {
        main_priv.battery_tick = HAL_GetTick();
        main_priv.battery_mv = hw_battery_volt();
    }
    // FIXME: semi-abritrary max and min battery voltages to use for 100 and 0%.
    const uint32_t max_mv = 3750;
    const uint32_t min_mv = 3500;
    uint32_t mv = CLAMP(main_priv.battery_mv, min_mv, max_mv);
    return 100 * (mv - min_mv) / (max_mv - min_mv);
}

void platform_sys_set_brightness(uint8_t val) {
    hw_backlight_set(val);
}

void platform_sys_set_volume(uint8_t val) {
    main_priv.volume = val;
}

void platform_sys_set_card_presence(bool present) {
    core_push_event(main_priv.core, present ? CORE_EVENT_CARD_IN : CORE_EVENT_CARD_OUT);
}

void platform_sys_set_charging(bool charging) {
    core_push_event(main_priv.core, charging ? CORE_EVENT_CHARGING : CORE_EVENT_DISCHARGING);
}

void platform_sys_set_usb_connected(bool connected) {
    core_event_t evt = connected ? CORE_EVENT_USB_CONNECT : CORE_EVENT_USB_DISCONNECT;
    core_push_event(main_priv.core, evt);
}

static void process_input(input_state_t *input) {
#define CHECK_BTN(n) HAL_GPIO_ReadPin(SW_##n##_GPIO_Port, SW_##n##_Pin) == GPIO_PIN_RESET
    input->up = CHECK_BTN(UP);
    input->down = CHECK_BTN(DN);
    input->left = CHECK_BTN(LF);
    input->right = CHECK_BTN(RT);
    input->menu = CHECK_BTN(MENU);
    input->a = CHECK_BTN(A);
    input->b = CHECK_BTN(B);
    input->c = CHECK_BTN(X);
    input->d = CHECK_BTN(Y);
#undef CHECK_BTN
}

static void hardware_poll() {
    bool charging = (HAL_GPIO_ReadPin(CHG_STAT_GPIO_Port, CHG_STAT_Pin) == GPIO_PIN_RESET);
    if (charging != main_priv.core.charging) {
        platform_sys_set_charging(charging);
    }
    bool sd_present = BSP_SD_IsDetected();
    if (sd_present != main_priv.core.card_present) {
        FRESULT res;
        if (sd_present) {
            uint8_t status = BSP_SD_Init();
            ASSERT_MSG(status == MSD_OK, "SD init fail");
            res = f_mount(&main_priv.SDFatFs, (TCHAR const *)main_priv.SDPath, 0);
            ASSERT_MSG(res == FR_OK, "SD mount fail");
        } else {
            res = f_mount(NULL, (TCHAR const *)main_priv.SDPath, 0);
            ASSERT_MSG(res == FR_OK, "SD unmount fail");
        }
        platform_sys_set_card_presence(sd_present);
    }
}

static float time_left(float next_time) {
    float now = HAL_GetTick();
    return CLAMP(next_time - now, 0, TICK_INTERVAL);
}

static void wait_for_next_frame() {
    DEBUG_TIMED_BLOCK("waitframe");
    HAL_Delay(time_left(main_priv.next_time));
    main_priv.next_time += TICK_INTERVAL;
}

static void blit_back(uint16_t *dst, uint8_t *src, uint32_t w, uint32_t h) {
    for (size_t i = 0; i < w * h; i++) {
        dst[i] = main_priv.palette[src[i]];
    }
}

volatile int audio_dropped = 0;

extern "C" void HAL_DAC_ConvCpltCallbackCh1(DAC_HandleTypeDef *hdac) {
    if (main_priv.bufpos == NONE) {
        main_priv.bufpos = FULL;
    } else {
        // else we're dropping audio frames
        audio_dropped++;
    }
}

extern "C" void HAL_DAC_ConvHalfCpltCallbackCh1(DAC_HandleTypeDef *hdac) {
    if (main_priv.bufpos == NONE) {
        main_priv.bufpos = HALF;
    } else {
        // else we're dropping audio frames
        audio_dropped++;
    }
}

static void audio_update(void) {
    DEBUG_TIMED_BLOCK("audio_mix");
    if (main_priv.bufpos == NONE) {
        return;
    }
    uint32_t ofs = (main_priv.bufpos == FULL) ? AUDIO_SAMPLES / 2 : 0;
    uint16_t *buf = main_priv.audiobuf + ofs;

    core_audio(main_priv.core, (uint8_t *)buf, sizeof(main_priv.audiobuf) / 2);

    if (main_priv.audio_pop == 0) {
        // de-pop: hold input low initially
        for (size_t i = 0; i < AUDIO_SAMPLES / 2; i++) {
            buf[i] = 0;
        }
        main_priv.audio_pop = 1;
    } else if (main_priv.audio_pop == 1) {
        // de-pop: ramp up to idle state
        for (size_t i = 0; i < AUDIO_SAMPLES / 2; i++) {
            buf[i] = i * 32;
        }
        main_priv.audio_pop = 2;
    } else {
        // normal: convert synth audio int16 -> uint16, adjusting volume
        int16_t *s16 = (int16_t *)buf;
        for (size_t i = 0; i < AUDIO_SAMPLES / 2; i++) {
            buf[i] = 0x8000 + (int16_t)(((int32_t)s16[i] * main_priv.volume) / 255);
        }
    }
    main_priv.bufpos = NONE;
}

extern void draw_fault_screen(bitmap_t *fb, fault_info_t *fault);

extern "C" void app_fault_handler(fault_info_t *fault) {
    bitmap_t fb = BITMAP8(SCREEN_WIDTH, SCREEN_HEIGHT, main_priv.backbuffer);

    bitmap_fill(&fb, 0);
    draw_fault_screen(&fb, fault);
    blit_back(main_priv.framebuffer, main_priv.backbuffer, SCREEN_WIDTH, SCREEN_HEIGHT);
    lcd_draw_bitmap(main_priv.framebuffer, 2, 2, SCREEN_WIDTH, SCREEN_HEIGHT);
}

static platform_api_t platform_ = {
    platform_mem_alloc,       platform_mem_free,       platform_file_open,    platform_file_read,
    platform_file_write,      platform_file_close,     platform_file_exists,  platform_file_size,
    platform_gfx_set_palette, platform_debug_get_usec, platform_debug_printf, debug_add_timed_block,
};

int main(void) {
    platform_params_t params = {
        .screen_w = SCREEN_WIDTH,
        .screen_h = SCREEN_HEIGHT,
        .audio_rate = AUDIO_SAMPLE_RATE,
    };

    hw_init();
    lcd_init();
    hw_led_on(LED_GREEN, 255);

    platform_gfx_set_palette((uint8_t *)syspalette.colours, syspalette.count);

    int res = FATFS_LinkDriver(&SD_Driver, main_priv.SDPath);
    ASSERT_MSG(res == 0, "fatfs link fail");

    MX_USB_DEVICE_Init();

    platform = &platform_;

    main_priv.volume = 255;
    hw_audio_start(main_priv.audiobuf, sizeof(main_priv.audiobuf));
    HAL_Delay(50);
    hw_speaker_enable(true);

    pcg32_seed(hw_rand());

    core_init(main_priv.core, platform_, params);
    hw_led_on(LED_GREEN, 0);

    input_state_t input = {};
    float last_tick = get_ticks() - 16;
    bool first = true;
    while (1) {
        main_priv.allocs.frame_free = main_priv.allocs.frame_alloc = 0;
        float now_tick = get_ticks();
        debug_start_frame();
        {
            DEBUG_TIMED_BLOCK("input");
            input.dt = (now_tick - last_tick) / 1000.0f;
            last_tick = now_tick;
            process_input(&input);
            hardware_poll();
            if (first) {
                core_push_event(main_priv.core, CORE_EVENT_START);
                first = false;
            }
        }
        core_tick(main_priv.core, input, main_priv.backbuffer);
        audio_update();
        {
            DEBUG_TIMED_BLOCK("plat_blit");
            blit_back(main_priv.framebuffer, main_priv.backbuffer, SCREEN_WIDTH, SCREEN_HEIGHT);
            lcd_draw_bitmap(main_priv.framebuffer, 2, 2, SCREEN_WIDTH, SCREEN_HEIGHT);
        }
        wait_for_next_frame();
        debug_add_alloc_stats(main_priv.allocs.cur, main_priv.allocs.frame_alloc,
                              main_priv.allocs.frame_free);
        debug_finish_frame(debug_profile_finish, &main_priv.core.dbg_profile);
    }
}
