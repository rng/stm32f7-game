/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SD_DETECT_Pin GPIO_PIN_1
#define SD_DETECT_GPIO_Port GPIOA
#define BATSENSE_Pin GPIO_PIN_2
#define BATSENSE_GPIO_Port GPIOA
#define SPK_EN_Pin GPIO_PIN_3
#define SPK_EN_GPIO_Port GPIOA
#define SPK_PWM_Pin GPIO_PIN_4
#define SPK_PWM_GPIO_Port GPIOA
#define DISP_SCK_Pin GPIO_PIN_5
#define DISP_SCK_GPIO_Port GPIOA
#define DISP_RST_Pin GPIO_PIN_6
#define DISP_RST_GPIO_Port GPIOA
#define DISP_MOSI_Pin GPIO_PIN_7
#define DISP_MOSI_GPIO_Port GPIOA
#define SW_A_Pin GPIO_PIN_0
#define SW_A_GPIO_Port GPIOB
#define SW_Y_Pin GPIO_PIN_1
#define SW_Y_GPIO_Port GPIOB
#define SW_RT_Pin GPIO_PIN_2
#define SW_RT_GPIO_Port GPIOB
#define DISP_CS_Pin GPIO_PIN_10
#define DISP_CS_GPIO_Port GPIOB
#define DISP_DC_Pin GPIO_PIN_11
#define DISP_DC_GPIO_Port GPIOB
#define SW_B_Pin GPIO_PIN_12
#define SW_B_GPIO_Port GPIOB
#define SW_MENU_Pin GPIO_PIN_13
#define SW_MENU_GPIO_Port GPIOB
#define LED1_Pin GPIO_PIN_6
#define LED1_GPIO_Port GPIOC
#define CHG_STAT_Pin GPIO_PIN_7
#define CHG_STAT_GPIO_Port GPIOC
#define DISP_PWM_Pin GPIO_PIN_8
#define DISP_PWM_GPIO_Port GPIOA
#define SW_LF_Pin GPIO_PIN_15
#define SW_LF_GPIO_Port GPIOA
#define SW_DN_Pin GPIO_PIN_4
#define SW_DN_GPIO_Port GPIOB
#define SW_X_Pin GPIO_PIN_5
#define SW_X_GPIO_Port GPIOB
#define SW_UP_Pin GPIO_PIN_7
#define SW_UP_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
