# vim: set ft=make:

BOARDREV?=revd
OPT?=s
ARCHFLAGS:= -mthumb -march=armv7e-m -mfpu=fpv4-sp-d16 -mfloat-abi=hard
PLATFLAGS:= $(ARCHFLAGS) -DPLAT_STM=1 -DSTM32F722xx -DTINYPRINTF_OVERRIDE_LIBC=0 -ffunction-sections -fdata-sections -ffast-math -DBOARD_$(BOARDREV) -DPLAT_HAVE_FAST_BLIT=1
LDFLAGS = $(ARCHFLAGS) -lm -L$(BASE_DIR)/src/plat_mini --static --specs=nosys.specs -Wl,--gc-sections
PLATLDFLAGS = -Tstm32f722rc.ld
GAMELDFLAGS = -Tgamelib.ld

CC:=arm-none-eabi-gcc
CXX:=arm-none-eabi-g++
AS:=arm-none-eabi-as
GDB:=arm-none-eabi-gdb
SIZE:=arm-none-eabi-size
OBJCOPY:=arm-none-eabi-objcopy
OBJDUMP:=arm-none-eabi-objdump

DEBUG_ARGS=-x $(BASE_DIR)/src/plat_mini/gdb.cmd
DEBUG_SERVERCFG=$(BASE_DIR)/src/plat_mini/openocd.cfg
BINEXT=.elf
SOEXT=.elf

STM32CUBE_DIR=$(BASE_DIR)/src/lib/STM32CubeF7

# STM32Cube HAL peripheral driver library
HAL_SRCS:= \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_adc.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_cortex.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_pwr_ex.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_flash.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_flash_ex.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_rcc.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_rcc_ex.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_dac.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_dac_ex.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_gpio.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_spi.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_dma.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_rng.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_sd.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_tim.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_tim_ex.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_pcd.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_pcd_ex.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_uart.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_ll_sdmmc.c \
    $(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Src/stm32f7xx_ll_usb.c \

# STM USB Device Library
USB_SRCS:= \
    $(STM32CUBE_DIR)/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_core.c \
    $(STM32CUBE_DIR)/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_ctlreq.c \
    $(STM32CUBE_DIR)/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_ioreq.c \
    $(STM32CUBE_DIR)/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Src/usbd_msc.c \
    $(STM32CUBE_DIR)/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Src/usbd_msc_bot.c \
    $(STM32CUBE_DIR)/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Src/usbd_msc_scsi.c \
    $(STM32CUBE_DIR)/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Src/usbd_msc_data.c \

# FatFs library
FATFS_SRCS:= \
    $(STM32CUBE_DIR)/Middlewares/Third_Party/FatFs/src/ff.c \
    $(STM32CUBE_DIR)/Middlewares/Third_Party/FatFs/src/ff_gen_drv.c \
    $(STM32CUBE_DIR)/Middlewares/Third_Party/FatFs/src/diskio.c \
    $(STM32CUBE_DIR)/Middlewares/Third_Party/FatFs/src/option/ccsbcs.c \

# tinyprintf library
TINYPRINTF_SRCS:= \
    $(BASE_DIR)/src/lib/tinyprintf/tinyprintf.c \

# Platform and startup code
PLATSRCS:= \
    $(BASE_DIR)/src/plat_mini/main.cc \
    $(BASE_DIR)/src/plat_mini/hw.c \
    $(BASE_DIR)/src/plat_mini/usbd_storage_if.c \
    $(BASE_DIR)/src/plat_mini/bsp_driver_lcd.c \
    $(BASE_DIR)/src/plat_mini/mx/$(BOARDREV)/usbd_conf.c \
    $(BASE_DIR)/src/plat_mini/mx/$(BOARDREV)/usbd_desc.c \
    $(BASE_DIR)/src/plat_mini/mx/$(BOARDREV)/usb_device.c \
    $(BASE_DIR)/src/plat_mini/mx/fatfs_platform.c \
    $(BASE_DIR)/src/plat_mini/mx/sd_diskio.c \
    $(BASE_DIR)/src/plat_mini/mx/bsp_driver_sd.c \
    $(BASE_DIR)/src/plat_mini/mx/system_stm32f7xx.c \
    $(BASE_DIR)/src/plat_mini/mx/$(BOARDREV)/main.c \
    $(BASE_DIR)/src/plat_mini/mx/$(BOARDREV)/stm32f7xx_hal_msp.c \
    $(BASE_DIR)/src/arch_stm32/faulthandler.cc \
    $(BASE_DIR)/src/arch_stm32/flash.cc \
    $(HAL_SRCS) \
    $(USB_SRCS) \
    $(FATFS_SRCS) \
    $(TINYPRINTF_SRCS) \

# Assembly sources
PLATASRCS:= \
    $(BASE_DIR)/src/plat_mini/startup.s \

INCS:= \
    -I$(BASE_DIR)/src \
    -I$(BASE_DIR)/src/plat_mini/mx \
    -I$(BASE_DIR)/src/plat_mini/mx/$(BOARDREV) \
    -I$(BASE_DIR)/src/plat_mini \
    -I$(BASE_DIR)/src/arch_stm32 \
    -I$(BASE_DIR)/src/lib/tinyprintf \
    -I$(STM32CUBE_DIR)/Middlewares/Third_Party/FatFs/src \
    -I$(STM32CUBE_DIR)/Middlewares/ST/STM32_USB_Device_Library/Core/Inc \
    -I$(STM32CUBE_DIR)/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc \
    -I$(STM32CUBE_DIR)/Drivers/STM32F7xx_HAL_Driver/Inc \
    -I$(STM32CUBE_DIR)/Drivers/CMSIS/Device/ST/STM32F7xx/Include \
    -I$(STM32CUBE_DIR)/Drivers/CMSIS/Include \

PLATTARGETS := gameloader.lst gameloader.bin $(GAME_DIR)/game.lst $(GAME_DIR)/game.bin
