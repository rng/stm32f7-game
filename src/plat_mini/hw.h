#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

enum {
    SEMIHOSTING_CMD_WRITE = 0x05,
    LED_GREEN = 1,
    LED_BLUE = 2,
};

void hw_init(void);
void hw_speaker_enable(bool en);
void hw_led_on(int which, int val);
uint32_t hw_battery_volt(void);
void hw_backlight_set(uint8_t val);
bool hw_debugger_attached(void);
void hw_semihosting_cmd(int cmd, void *arg);
void hw_audio_start(uint16_t *buf, size_t buf_len);
uint32_t hw_rand(void);

#ifdef __cplusplus
}
#endif
