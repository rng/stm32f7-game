#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "usbd_msc.h"

extern USBD_StorageTypeDef USBD_Storage_Interface_fops;

#ifdef __cplusplus
}
#endif
