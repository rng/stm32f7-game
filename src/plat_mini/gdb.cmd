define hook-quit
    set confirm off
end
set pagination off

target remote :3333
mon reset halt
mon arm semihosting enable
load build/mini/example/game.elf
load build/mini/gameloader.elf
add-symbol-file build/mini/example/game.elf 0x08020000

set breakpoint pending on
b HardFault_Handler
b fatal_error
c
