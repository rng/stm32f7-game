#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f7xx_hal.h"

void lcd_init(void);
void lcd_erase(void);
void lcd_draw_bitmap(uint16_t *data, int x, int y, int w, int h);

#ifdef __cplusplus
}
#endif
