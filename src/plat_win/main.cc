#include <windows.h>

#include "engine/engine.h"

// defined in plat_pc/sdl.cc
extern void sdl_main_init(int argc, char *argv[]);
extern void sdl_main_tick();
extern void sdl_main_term();
bool sdl_main_done();

HINSTANCE game_lib;

const char *os_game_lib_name(void) {
    return "game.dll";
}

size_t os_alloc_size(void *ptr) {
    return _msize(ptr);
}

game_api_t os_load_game_code(const char *game_fn) {
    game_lib = LoadLibrary(game_fn);
    DWORD err = GetLastError();
    ASSERT_MSG(game_lib, "Error loading game shared lib: 0x%lx", err);

    get_game_api_fn_t get_game_api = (get_game_api_fn_t)GetProcAddress(game_lib, "get_game_api");
    err = GetLastError();
    ASSERT_MSG(get_game_api, "Error getting game API from game: 0x%lx", err);
    return get_game_api();
}

void os_unload_game_code(void) {
    if (game_lib) {
        FreeLibrary(game_lib);
    }
}

uint64_t os_get_usec(void) {
    __int64 win_time;
    GetSystemTimeAsFileTime((FILETIME *)&win_time);
    win_time -= 116444736000000000; // win epoch is 1601, translate to 1970
    return win_time / 10;           // win time is 100ns, convert to us
}

int main(int argc, char *argv[]) {
    sdl_main_init(argc, argv);

    while (!sdl_main_done()) {
        sdl_main_tick();
    }

    sdl_main_term();
    return EXIT_SUCCESS;
}
