# vim: set ft=make:

# Cross compiled Windows build using mingw

OPT?=g
# Path to the SDL2 development libraries (https://www.libsdl.org/download-2.0.php)
SDL_PATH := $(BASE_DIR)/src/lib/SDL2-2.0.10/x86_64-w64-mingw32
SDL_CFLAGS := -I$(SDL_PATH)/include
SDL_LDFLAGS := -static -L$(SDL_PATH)/lib -lSDL2 -mwindows -lm -luser32 -lwinmm \
	           -limm32 -lole32 -loleaut32 -lversion -lsetupapi -fstack-protector
PLATFLAGS = $(SDL_CFLAGS) -fPIC -DPLAT_ASSERT_PRINT=1 -D__USE_MINGW_ANSI_STDIO=1
LDFLAGS = -lm $(SDL_LDFLAGS) $(PLATFLAGS)
GAMELDFLAGS = -shared

CC:=x86_64-w64-mingw32-gcc
CXX:=x86_64-w64-mingw32-g++
STRIP:=x86_64-w64-mingw32-strip

SOEXT=.dll
BINEXT=.exe

# tinyprintf library
TINYPRINTF_SRCS:= \
    $(BASE_DIR)/src/lib/tinyprintf/tinyprintf.c \

# Platform and startup code
PLATSRCS:= \
    $(BASE_DIR)/src/arch_pc/sdl.cc \
    $(BASE_DIR)/src/plat_win/main.cc \
    $(BASE_DIR)/src/system/core.cc \
    $(BASE_DIR)/src/engine/bitmap.cc \
    $(BASE_DIR)/src/engine/font.cc \
    $(TINYPRINTF_SRCS) \


INCS:=\
    -I$(BASE_DIR)/src/lib/jo_gif \
    -I$(BASE_DIR)/src/lib/tinyprintf \
    -I$(BASE_DIR)/src
