#define SDL_MAIN_HANDLED

#include <SDL.h>
#include <dirent.h>
#include <libgen.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "engine/engine.h"
#include "engine/syspalette.h"
#include "system/core.h"

#include "plat_common/config.h"
#include "plat_common/debug.h"

#include "jo_gif.h"

const int JOY_DEADZONE = 8000;
const float TICK_INTERVAL = 1000 / (float)TARGET_FRAMERATE;

typedef struct {
    int gifscale;
    int pixscale;
} gameopts_t;

typedef struct main_priv_t {
    SDL_Window *Window;
    SDL_Surface *buffer, *buffer32, *gifframe, *screen;
    SDL_Joystick *game_controller;
    SDL_AudioStream *stream;
    float start_time, next_time;
    uint32_t framecount;
    bool gif_recording, snd_recording, quit;
    jo_gif_t gif;
    FILE *sndfile;
    uint32_t sndlen;
    char *base_dir;
    const char *game_name;
    gameopts_t gameopts;
    input_state_t input;
    float last_tick;
    core_t core;
    uint8_t volume;
    struct {
        size_t cur, frame_alloc, frame_free;
    } allocs;
} main_priv_t;
main_priv_t main_priv;

#if UNIFIED_BUILD == 1
extern platform_api_t *platform;
#else
platform_api_t *platform;
#endif

// Defined in the appropriate plat_* layer
uint64_t os_get_usec(void);
game_api_t os_load_game_code(const char *);
void os_unload_game_code(void);
const char *os_game_lib_name(void);
size_t os_alloc_size(void *);

static float time_left(float next_time) {
    float now = SDL_GetTicks();
    return (next_time <= now) ? 0 : (next_time - now);
}

static void wait_for_next_frame() {
    SDL_Delay(time_left(main_priv.next_time));
    float cur_time = (float)SDL_GetTicks();
    if (cur_time > (main_priv.start_time + 250.0f)) {
        char titlebuf[32];
        float fps = (main_priv.framecount / ((cur_time - main_priv.start_time) / 1000.0f));
        const char *gifrec = main_priv.gif_recording ? " gifrec" : "";
        const char *sndrec = main_priv.snd_recording ? " sndrec" : "";
        snprintf(titlebuf, sizeof(titlebuf), "Game [%0.1ffps%s%s]", (double)fps, gifrec, sndrec);
        SDL_SetWindowTitle(main_priv.Window, titlebuf);
        main_priv.start_time = cur_time;
        main_priv.framecount = 0;
    }
    main_priv.next_time += TICK_INTERVAL;
    main_priv.framecount++;
}

static void toggle_gifrecord(void) {
    main_priv.gif_recording = !main_priv.gif_recording;
    if (main_priv.gif_recording) {
        char buf[64];
        time_t rawtime;
        time(&rawtime);
        struct tm *info = localtime(&rawtime);
        strftime(buf, sizeof(buf), "recording-%d%m%y-%H%M.gif", info);
        int gifscale = main_priv.gameopts.gifscale;
        main_priv.gif =
            jo_gif_start(buf, SCREEN_WIDTH * gifscale, SCREEN_HEIGHT * gifscale, 0, 255);
        // set palette
        SDL_Palette *pal = main_priv.buffer->format->palette;
        ASSERT(pal->ncolors == 256);
        for (int i = 0; i < pal->ncolors; i++) {
            SDL_Color *col = &pal->colors[i];
            main_priv.gif.palette[i * 3 + 0] = col->r;
            main_priv.gif.palette[i * 3 + 1] = col->g;
            main_priv.gif.palette[i * 3 + 2] = col->b;
        }
    } else {
        jo_gif_end(&main_priv.gif);
    }
}

static void toggle_sndrecord(void) {
    if (!main_priv.snd_recording) {
        char buf[64];
        time_t rawtime;
        time(&rawtime);
        struct tm *info = localtime(&rawtime);
        strftime(buf, sizeof(buf), "recording-%d%m%y-%H%M.wav", info);
        main_priv.sndfile = fopen(buf, "wb");
        main_priv.sndlen = 0;

        const uint8_t wav_header[] = {
            0x52, 0x49, 0x46, 0x46, // 00 chunk id: 'RIFF'
            0x00, 0x00, 0x00, 0x00, // 00 chunk size
            0x57, 0x41, 0x56, 0x45, // 00 format: 'WAVE'
            0x66, 0x6d, 0x74, 0x20, // 12 chunk id: 'fmt '
            0x10, 0x00, 0x00, 0x00, // 16 sub chunk size
            0x01, 0x00,             // 20 audio format (pcm)
            0x01, 0x00,             // 22 numer of channels (1)
            0x40, 0x1f, 0x00, 0x00, // 24 sample rate (8KHz)
            0x80, 0x3e, 0x00, 0x00, // 28 byte rate (8000*2)
            0x02, 0x00,             // 32 block alignment (2)
            0x10, 0x00,             // 34 bits per sample (16)
            0x64, 0x61, 0x74, 0x61, // 36 chunk id: 'data'
            0x00, 0x00, 0x00, 0x00, // 40 chunk size
        };
        fwrite(wav_header, sizeof(wav_header), 1, main_priv.sndfile);

        main_priv.snd_recording = true;
    } else {
        main_priv.snd_recording = false;
        // write file size to header
        uint32_t size = main_priv.sndlen + 36;
        fseek(main_priv.sndfile, 4, SEEK_SET);
        fwrite(&size, sizeof(size), 1, main_priv.sndfile);
        // write data chunk size to header
        fseek(main_priv.sndfile, 40, SEEK_SET);
        fwrite(&main_priv.sndlen, sizeof(main_priv.sndlen), 1, main_priv.sndfile);
        fclose(main_priv.sndfile);
    }
}

static void update_input_key(input_state_t *input, uint32_t key, bool pressed) {
    switch (key) {
        case SDLK_UP:
            input->up = pressed;
            break;
        case SDLK_DOWN:
            input->down = pressed;
            break;
        case SDLK_LEFT:
            input->left = pressed;
            break;
        case SDLK_RIGHT:
            input->right = pressed;
            break;
        case SDLK_z:
            input->a = pressed;
            break;
        case SDLK_x:
            input->b = pressed;
            break;
        case SDLK_a:
            input->c = pressed;
            break;
        case SDLK_s:
            input->d = pressed;
            break;
        case SDLK_ESCAPE:
            input->menu = pressed;
            break;
        case SDLK_F7:
            if (!pressed) {
                toggle_gifrecord();
            }
            break;
        case SDLK_F8:
            if (!pressed) {
                toggle_sndrecord();
            }
            break;
        case SDLK_F9:
            if (!pressed) {
                core_push_event(main_priv.core, main_priv.core.card_present ? CORE_EVENT_CARD_OUT
                                                                            : CORE_EVENT_CARD_IN);
            }
            break;
    }
}

static void update_input_joy(input_state_t *input, SDL_JoystickID id, uint8_t axis, int16_t value) {
    if (id != 0)
        return;

    bool pressed = ABS(value) > JOY_DEADZONE;
    if (axis == 0) {
        input->left = pressed && (value < 0);
        input->right = pressed && (value > 0);
    }
    if (axis == 1) {
        input->up = pressed && (value < 0);
        input->down = pressed && (value > 0);
    }
}

static void update_input_but(input_state_t *input, SDL_JoystickID id, uint8_t button,
                             bool pressed) {
    if (id != 0)
        return;
    switch (button) {
        case 0:
            input->a = pressed;
            break;
        case 1:
            input->b = pressed;
            break;
        case 2:
            input->c = pressed;
            break;
        case 3:
            input->d = pressed;
            break;
        case 7:
            input->menu = pressed;
            break;
    }
}

static void update_input_hat(input_state_t *input, SDL_JoystickID id, uint8_t hat, uint8_t value) {
    if (id != 0)
        return;
    switch (value) {
        case SDL_HAT_UP:
            input->up = true;
            break;
        case SDL_HAT_DOWN:
            input->down = true;
            break;
        case SDL_HAT_LEFT:
            input->left = true;
            break;
        case SDL_HAT_RIGHT:
            input->right = true;
            break;
    }
}

static void process_input(input_state_t *input) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                main_priv.quit = true;
                break;

            case SDL_KEYDOWN:
            case SDL_KEYUP:
                update_input_key(input, event.key.keysym.sym, event.type == SDL_KEYDOWN);
                break;

            case SDL_JOYAXISMOTION:
                update_input_joy(input, event.jaxis.which, event.jaxis.axis, event.jaxis.value);
                break;

            case SDL_JOYBUTTONDOWN:
            case SDL_JOYBUTTONUP:
                update_input_but(input, event.jbutton.which, event.jbutton.button,
                                 event.type == SDL_JOYBUTTONDOWN);
                break;

            case SDL_JOYHATMOTION:
                update_input_hat(input, event.jhat.which, event.jhat.hat, event.jhat.value);
                break;
        }
    }
}

static void basedir_filename(char *fn, size_t fn_size, const char *filename) {
    strncat(fn, main_priv.base_dir, fn_size - strlen(fn) - 1);
    strncat(fn, "/", fn_size - strlen(fn) - 1);
    if (main_priv.game_name && strlen(main_priv.game_name)) {
        strncat(fn, main_priv.game_name, fn_size - strlen(fn) - 1);
        strncat(fn, "/", fn_size - strlen(fn) - 1);
    }
    strncat(fn, filename, fn_size - strlen(fn) - 1);
}

void *platform_mem_alloc(size_t size) {
    main_priv.allocs.cur += size;
    main_priv.allocs.frame_alloc += size;
    return calloc(size, 1);
}

void platform_mem_free(void *ptr) {
    size_t size = os_alloc_size(ptr);
    main_priv.allocs.cur -= size;
    main_priv.allocs.frame_free += size;
    free(ptr);
}

bool platform_file_exists(const char *filename) {
    struct stat stats;
    char fn[256] = {0};
    basedir_filename(fn, sizeof(fn), filename);
    int res = stat(fn, &stats);
    return res == 0;
}

platform_handle_t platform_file_open(const char *filename, bool write) {
    char fn[256] = {0};
    basedir_filename(fn, sizeof(fn), filename);
    FILE *f = fopen(fn, write ? "wb" : "rb");
    ASSERT_MSG(f, "Cannot open asset file %s", fn);
    return (platform_handle_t)f;
}

size_t platform_file_read(platform_handle_t h, size_t n, void *b) {
    ASSERT(h);
    return fread(b, 1, n, (FILE *)h);
}

size_t platform_file_write(platform_handle_t h, size_t n, void *b) {
    ASSERT(h);
    return fwrite(b, 1, n, (FILE *)h);
}

void platform_file_close(platform_handle_t h) {
    fclose((FILE *)h);
}

size_t platform_file_size(platform_handle_t h) {
    FILE *f = (FILE *)h;
    size_t pos = ftell(f);
    fseek(f, 0, SEEK_END);
    size_t size = ftell(f);
    fseek(f, pos, SEEK_SET);
    return size;
}

void platform_gfx_set_palette(uint8_t *colours, size_t count) {
    SDL_Color c[256] = {};
    ASSERT(count <= 256);
    for (size_t i = 0; i < count; i++) {
        c[i].r = colours[i * 3 + 0];
        c[i].g = colours[i * 3 + 1];
        c[i].b = colours[i * 3 + 2];
    }
    if (SDL_SetPaletteColors(main_priv.buffer->format->palette, c, 0, 256) < 0) {
        printf("Error initialising palette\n");
        exit(-1);
    }
    if (SDL_SetPaletteColors(main_priv.gifframe->format->palette, c, 0, 256) < 0) {
        printf("Error initialising palette\n");
        exit(-1);
    }
}

void audio_callback(void *userdata, uint8_t *stream, int len_bytes) {
    uint8_t *outstream = stream;
    while (len_bytes > 0) {
        // Synthesise audio data and push it into the stream
        int16_t buf[16];
        core_audio(main_priv.core, (uint8_t *)buf, sizeof(buf));
        for (size_t i = 0; i < NELEMS(buf); i++) {
            buf[i] = ((int32_t)buf[i] * main_priv.volume) / 255;
        }
        if (main_priv.snd_recording) {
            fwrite(buf, 1, sizeof(buf), main_priv.sndfile);
            main_priv.sndlen += sizeof(buf);
        }
        int res = SDL_AudioStreamPut(main_priv.stream, buf, sizeof(buf));
        if (res == -1) {
            printf("warning: failed to stream in audio data\n");
            break;
        }
        // pull resampled audio out of the stream and feed to SDL
        int read = SDL_AudioStreamGet(main_priv.stream, outstream, len_bytes);
        if (read == -1) {
            printf("warning: failed to stream out audio data\n");
            break;
        }
        len_bytes -= read;
        outstream += read;
    }
}

uint64_t platform_debug_get_usec(void) {
    return os_get_usec();
}

void platform_debug_printf(const char *fmt, ...) {
    char line[256];
    va_list arglist;

    va_start(arglist, fmt);
    vsnprintf(line, sizeof(line), fmt, arglist);
    printf("%s", line);
    va_end(arglist);
}

bool is_gamedir(const char *dirname) {
    char path[256] = {0};
    strncat(path, main_priv.base_dir, sizeof(path) - strlen(dirname) - 1);
    strncat(path, "/", sizeof(path) - strlen(dirname) - 1);
    strncat(path, dirname, sizeof(path) - strlen(dirname) - 1);
    strncat(path, "/", sizeof(path) - strlen(dirname) - 1);
    strncat(path, os_game_lib_name(), sizeof(path) - strlen(dirname) - 1);

    return access(path, F_OK) != -1;
}

void platform_sys_games_list(const char *games[], size_t &games_max) {
#if UNIFIED_BUILD == 1
    games[0] = STRINGIFY(GAME_NAME);
    games_max = 1;
#else
    struct dirent *ep;
    DIR *dp = opendir(main_priv.base_dir);
    ASSERT(dp);

    size_t game_count = 0;
    while ((ep = readdir(dp)) && (game_count < games_max)) {
        if (is_gamedir(ep->d_name)) {
            games[game_count++] = strdup(ep->d_name);
        }
    }
    closedir(dp);
    games_max = game_count;
#endif
}

game_api_t platform_sys_game_load(const char *filename) {
    main_priv.game_name = strdup(filename);
    // get path to game code (dynamic library)
    char fn[256] = {0};
    basedir_filename(fn, sizeof(fn), os_game_lib_name());
    return os_load_game_code(fn);
}

void platform_sys_game_unload(void) {
    free((void *)main_priv.game_name);
    main_priv.game_name = strdup("");
    os_unload_game_code();
}

void platform_sys_set_default_palette(void) {
    platform_gfx_set_palette((uint8_t *)syspalette.colours, syspalette.count);
}

uint32_t platform_sys_battery_percent(void) {
    return 100;
}

void platform_sys_set_brightness(uint8_t val) {
    (void)val;
}

void platform_sys_set_volume(uint8_t val) {
    main_priv.volume = val;
}

void parse_args(int argc, char *argv[], gameopts_t *opts) {
    opts->pixscale = DEFAULT_PIXEL_SCALE;
    opts->gifscale = DEFAULT_GIF_SCALE;
    int opt;
    while ((opt = getopt(argc, argv, "s:g:h")) != -1) {
        switch (opt) {
            case 's':
                opts->pixscale = atoi(optarg);
                break;
            case 'g':
                opts->gifscale = atoi(optarg);
                break;
            case 'h':
            default:
                printf("usage: %s [OPTIONS]\n", argv[0]);
                printf("  -h           this text\n");
                printf("  -s <SCALE>   pixel scale factor (eg 2 for 2x scaling)\n");
                printf("  -g <SCALE>   pixel scale factor for gif recordings\n");
                exit(-1);
                break;
        }
    }
}

void sdl_main_tick(void) {
    float now_tick = SDL_GetTicks();
    main_priv.allocs.frame_free = main_priv.allocs.frame_alloc = 0;
    debug_start_frame();
    {
        DEBUG_TIMED_BLOCK("input");
        main_priv.input.dt = (now_tick - main_priv.last_tick) / 1000.0f;
        main_priv.last_tick = now_tick;
        process_input(&main_priv.input);
    }
    if (core_tick(main_priv.core, main_priv.input, (uint8_t *)main_priv.buffer->pixels)) {
        main_priv.quit = true;
    }
    {
        DEBUG_TIMED_BLOCK("plat_blit");
        SDL_BlitSurface(main_priv.buffer, NULL, main_priv.buffer32, NULL);
        SDL_BlitScaled(main_priv.buffer32, NULL, main_priv.screen, NULL);
        SDL_UpdateWindowSurface(main_priv.Window);
        if (main_priv.gif_recording) {
            if (main_priv.gameopts.gifscale == 1) {
                SDL_BlitSurface(main_priv.buffer, NULL, main_priv.gifframe, NULL);
            } else {
                // can't use SDL_BlitScaled for 8b surfaces
                int scale = main_priv.gameopts.gifscale;
                for (int y = 0; y < SCREEN_HEIGHT; y++) {
                    for (int j = 0; j < scale; j++) {
                        uint8_t *srow = &((uint8_t *)main_priv.buffer->pixels)[y * SCREEN_WIDTH];
                        uint8_t *drow =
                            &((uint8_t *)main_priv.gifframe
                                  ->pixels)[(((y * scale) + j) * (SCREEN_WIDTH * scale))];
                        for (int x = 0; x < SCREEN_WIDTH; x++) {
                            for (int i = 0; i < scale; i++) {
                                *drow++ = *srow;
                            }
                            srow++;
                        }
                    }
                }
            }
            jo_gif_frame(&main_priv.gif, (uint8_t *)main_priv.gifframe->pixels, 2);
        }
    }
    {
        DEBUG_TIMED_BLOCK("waitframe");
        wait_for_next_frame();
    }
    debug_add_alloc_stats(main_priv.allocs.cur, main_priv.allocs.frame_alloc,
                          main_priv.allocs.frame_free);
    debug_finish_frame(debug_profile_finish, &main_priv.core.dbg_profile);
}

static platform_api_t _platform = {
    platform_mem_alloc,       platform_mem_free,       platform_file_open,    platform_file_read,
    platform_file_write,      platform_file_close,     platform_file_exists,  platform_file_size,
    platform_gfx_set_palette, platform_debug_get_usec, platform_debug_printf, debug_add_timed_block,
};

static platform_params_t params = {
    .screen_w = SCREEN_WIDTH,
    .screen_h = SCREEN_HEIGHT,
    .audio_rate = AUDIO_SAMPLE_RATE,
};

void sdl_main_init(int argc, char *argv[]) {
    parse_args(argc, argv, &main_priv.gameopts);

    int gifscale = main_priv.gameopts.gifscale;
    int pixscale = main_priv.gameopts.pixscale;

    // extract running directory name
    char *exename = strdup(argv[0]);
    main_priv.base_dir = strdup(dirname(exename));
    free(exename);

    platform = &_platform;

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK);

    if (SDL_NumJoysticks()) {
        main_priv.game_controller = SDL_JoystickOpen(0);
        if (!main_priv.game_controller) {
            printf("Warning: Unable to open game controller! SDL Error: %s\n", SDL_GetError());
        }
    }

    SDL_AudioSpec desiredSpec, obtainedSpec;
    desiredSpec.freq = AUDIO_SAMPLE_RATE;
    desiredSpec.format = AUDIO_S16SYS;
    desiredSpec.channels = 2;
    desiredSpec.samples = 256;
    desiredSpec.callback = audio_callback;
    desiredSpec.userdata = NULL;
    main_priv.volume = 255;

    if (SDL_OpenAudio(&desiredSpec, &obtainedSpec) < 0) {
        printf("Error initialising audio\n");
        exit(-1);
    } else {
        printf("Opened audio %dhz, %db%s%s, %d channels\n", obtainedSpec.freq,
               obtainedSpec.format & 0xff, SDL_AUDIO_ISSIGNED(obtainedSpec.format) ? " signed" : "",
               SDL_AUDIO_ISFLOAT(obtainedSpec.format) ? " float" : "", obtainedSpec.channels);
    }

    // Resample from our sample rate / 16bit mono to whatever the OS wants
    main_priv.stream = SDL_NewAudioStream(AUDIO_S16SYS, 1, AUDIO_SAMPLE_RATE, obtainedSpec.format,
                                          obtainedSpec.channels, obtainedSpec.freq);
    if (!main_priv.stream) {
        printf("Failed to create audio stream\n");
        exit(-1);
    }

    main_priv.Window = SDL_CreateWindow("", 0, 0, 0, 0, SDL_WINDOW_HIDDEN);
    SDL_SetWindowSize(main_priv.Window, params.screen_w * pixscale, params.screen_h * pixscale);
    SDL_ShowWindow(main_priv.Window);
    SDL_SetWindowPosition(main_priv.Window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
    SDL_SetWindowBordered(main_priv.Window, SDL_TRUE);

    main_priv.screen = SDL_GetWindowSurface(main_priv.Window);

    main_priv.buffer = SDL_CreateRGBSurface(0, params.screen_w, params.screen_h, 8, 0, 0, 0, 0);
    main_priv.buffer32 = SDL_CreateRGBSurface(0, params.screen_w, params.screen_h, 32, 0, 0, 0, 0);
    main_priv.gifframe = SDL_CreateRGBSurface(0, params.screen_w * gifscale,
                                              params.screen_h * gifscale, 8, 0, 0, 0, 0);
    ASSERT(main_priv.buffer->format->BytesPerPixel == 1);
    ASSERT(main_priv.buffer32->format->BytesPerPixel == 4);
    ASSERT(main_priv.gifframe->format->BytesPerPixel == 1);

    platform_gfx_set_palette((uint8_t *)syspalette.colours, syspalette.count);

    pcg32_seed(time(0));

    core_init(main_priv.core, _platform, params);
    core_push_event(main_priv.core, CORE_EVENT_START);
    core_push_event(main_priv.core, CORE_EVENT_CARD_IN);

    main_priv.start_time = main_priv.next_time = SDL_GetTicks() + TICK_INTERVAL;
    main_priv.last_tick = main_priv.start_time - TICK_INTERVAL;

    SDL_PauseAudio(0);
}

void sdl_main_term() {
    core_term(main_priv.core);

    if (main_priv.gif_recording) {
        toggle_gifrecord();
    }

    SDL_CloseAudio();
    SDL_FreeSurface(main_priv.gifframe);
    SDL_FreeSurface(main_priv.screen);
    SDL_FreeSurface(main_priv.buffer);
    SDL_FreeSurface(main_priv.buffer32);
    SDL_DestroyWindow(main_priv.Window);
    SDL_FreeAudioStream(main_priv.stream);
    if (main_priv.game_controller) {
        SDL_JoystickClose(main_priv.game_controller);
    }
    SDL_Quit();
    free(main_priv.base_dir);
}

bool sdl_main_done() {
    return main_priv.quit;
}
