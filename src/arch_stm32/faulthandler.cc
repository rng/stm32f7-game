#include <stdarg.h>
#include <string.h>

#include "engine/common.h"
#include "engine/font.h"

#include "faulthandler.h"
#include "stm32f7xx.h"
#include "tinyprintf.h"

int dprint(bitmap_t *fb, int x, int y, const char *fmt, ...) {
    char line[41];
    va_list arglist;

    va_start(arglist, fmt);
    tfp_vsnprintf(line, sizeof(line), fmt, arglist);
    font_print_col(fb, font_system(), x * 4, y * 8, line, 254);
    va_end(arglist);
    return strlen(line);
}

int hprint(bitmap_t *fb, int x, int y, const char *fmt, ...) {
    char line[41];
    va_list arglist;

    bitmap_draw_fillrect(fb, 0, y * 8 - 1, fb->width, y * 8 + 7, 254);
    va_start(arglist, fmt);
    tfp_vsnprintf(line, sizeof(line), fmt, arglist);
    font_print_col(fb, font_system(), x * 4, y * 8, line, 0);
    va_end(arglist);
    return strlen(line);
}

static void draw_regs(bitmap_t *fb, exception_regs_t *core, exception_sys_regs_t *sys, int y) {
    hprint(fb, 1, y++, "REGISTERS");
    y++;
    dprint(fb, 1, y++, "PC: %08X  LR: %08X", core->pc, core->lr);
    dprint(fb, 1, y++, "SP: %08X  R0: %08X", sys->sp, core->r0);
    dprint(fb, 1, y++, "R1: %08X  R2: %08X", core->r1, core->r2);
    dprint(fb, 1, y++, "R3: %08X  R12:%08X", core->r3, core->r12);
}

static void draw_assertion_fault(bitmap_t *fb, exception_regs_t *core, exception_sys_regs_t *sys) {
    hprint(fb, 1, 1, "ASSERT FAILED");

    // `fatal_error` is called with message as first parameter
    const char *assert_msg = (const char *)core->r0;
    dprint(fb, 1, 3, "%s", assert_msg ? assert_msg : "No assert message");

    draw_regs(fb, core, sys, 5);
}

static void draw_generic_fault(bitmap_t *fb, exception_regs_t *core, exception_sys_regs_t *sys) {
    hprint(fb, 1, 1, "SYSTEM FAULT");
    dprint(fb, 1, 3, "HFSR: %08x CFSR: %08x", sys->hfsr, sys->cfsr);
    int y = 4;

    // Hard fault
    if (sys->hfsr) {
        int x = dprint(fb, 1, y, "Hard Fault: ");
        if (sys->hfsr & SCB_HFSR_FORCED_Msk) {
            x += dprint(fb, x, y, "FORCED ");
        }
        if (sys->hfsr & SCB_HFSR_VECTTBL_Msk) {
            dprint(fb, x, y, "VECTTBL");
        }
        y++;
    }

    // MemManage, Bus, Usage faults
    if (sys->cfsr) {
        uint8_t mmfsr = sys->cfsr >> SCB_CFSR_MEMFAULTSR_Pos;
        uint8_t bfsr = sys->cfsr >> SCB_CFSR_BUSFAULTSR_Pos;
        uint16_t ufsr = sys->cfsr >> SCB_CFSR_USGFAULTSR_Pos;
        if (mmfsr) {
            const char *fields[] = {"IACCVIOL", "DACCVIOL", "?", "MUNSTKERR", "MSTKERR", "MLSPERR"};
            int x = dprint(fb, 1, y, "Mem Fault: ");
            if (mmfsr & 0x80) {
                x += dprint(fb, x, y, "(at %08X) ", sys->mmfar);
            }
            for (size_t bit = 0; bit < NELEMS(fields); bit++) {
                if (mmfsr & (1 << bit)) {
                    x += dprint(fb, x, y, "%s ", fields[bit]);
                }
            }
            y++;
        }

        if (bfsr) {
            const char *fields[] = {"IBUSERR",  "PRECISERR", "IMPRECISERR",
                                    "UNSTKERR", "STKERR",    "LSPERR"};
            int x = dprint(fb, 1, y, "Bus Fault: ");
            if (bfsr & 0x80) {
                x += dprint(fb, x, y, "(at %08X) ", sys->bfar);
            }
            for (size_t bit = 0; bit < NELEMS(fields); bit++) {
                if (bfsr & (1 << bit)) {
                    x += dprint(fb, x, y, "%s ", fields[bit]);
                }
            }
            y++;
        }

        if (ufsr) {
            const char *fields[] = {"UNDEFINSTR", "INVSTATE", "INVPC", "NOCP",      "?",
                                    "?",          "?",        "?",     "UNALIGNED", "DIVBYZERO"};
            int x = dprint(fb, 1, y, "Usage Fault: ");
            for (size_t bit = 0; bit < NELEMS(fields); bit++) {
                if (ufsr & (1 << bit)) {
                    x += dprint(fb, x, y, "%s ", fields[bit]);
                }
            }
            y++;
        }
    }

    draw_regs(fb, core, sys, 7);
}

void draw_fault_screen(bitmap_t *fb, fault_info_t *fault) {
    exception_regs_t *core = &fault->core_regs;
    exception_sys_regs_t *sys = &fault->sys_regs;

    if (core->pc == ((uint32_t)fatal_error & ~1)) {
        draw_assertion_fault(fb, core, sys);
    } else {
        draw_generic_fault(fb, core, sys);
    }
}
