#pragma once

#include <stdint.h>

typedef struct {
    uint32_t r0;
    uint32_t r1;
    uint32_t r2;
    uint32_t r3;
    uint32_t r12;
    uint32_t lr;
    uint32_t pc;
    uint32_t psr;
} exception_regs_t;

typedef struct {
    uint32_t sp;
    uint32_t hfsr;
    uint32_t cfsr;
    uint32_t mmfar;
    uint32_t bfar;
} exception_sys_regs_t;

typedef struct {
    exception_regs_t core_regs;
    exception_sys_regs_t sys_regs;
} fault_info_t;
