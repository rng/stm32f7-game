#include "stm32f722xx.h"

/*
ARMv7 SIMD optimised blitting

This code performs bitmap blits using ARMv7M SIMD instructions.
It operates on a per-line basis, copy pixels in word-sized chunks.

TODO: ensure dp is word aligned.
*/

// Normal blit
#define FASTBLIT_NORMAL(PIX_EXPR1, PIX_EXPR4)              \
    const int snext = src_stride - width;                  \
    for (int j = 0; j < height; j++) {                     \
        /* word at a time */                               \
        int remain = width;                                \
        while (remain > 3) {                               \
            uint32_t srcpix4 = *((uint32_t *)sp);          \
            uint32_t dstpix4 = *((uint32_t *)dp);          \
            __UADD8(srcpix4, 0x01010101);                  \
            *((uint32_t *)dp) = __SEL(dstpix4, PIX_EXPR4); \
            dp += 4;                                       \
            sp += 4;                                       \
            remain -= 4;                                   \
        }                                                  \
        /* trailing bytes, byte at time */                 \
        while (remain--) {                                 \
            uint8_t srcpix1 = *sp;                         \
            if (srcpix1 != 255) {                          \
                *dp = PIX_EXPR1;                           \
            }                                              \
            sp++;                                          \
            dp++;                                          \
        }                                                  \
        dp += dnext;                                       \
        sp += snext;                                       \
    }

// Horizontal flip blit
#define FASTBLIT_HFLIP(PIX_EXPR1, PIX_EXPR4)               \
    const int snext = src_stride + width;                  \
    for (int j = 0; j < height; j++) {                     \
        /* word at a time */                               \
        int remain = width;                                \
        while (remain > 3) {                               \
            sp -= 4;                                       \
            uint32_t srcpix4 = *((uint32_t *)sp);          \
            uint32_t dstpix4 = *((uint32_t *)dp);          \
            srcpix4 = __builtin_bswap32(srcpix4);          \
            __UADD8(srcpix4, 0x01010101);                  \
            *((uint32_t *)dp) = __SEL(dstpix4, PIX_EXPR4); \
            dp += 4;                                       \
            remain -= 4;                                   \
        }                                                  \
        /* trailing bytes, byte at time */                 \
        while (remain--) {                                 \
            sp--;                                          \
            uint8_t srcpix1 = *sp;                         \
            if (srcpix1 != 255) {                          \
                *dp = PIX_EXPR1;                           \
            }                                              \
            dp++;                                          \
        }                                                  \
        dp += dnext;                                       \
        sp += snext;                                       \
    }

static void bitmap_fast_blit_pal8(uint8_t *dp, uint8_t *sp, int width, int height, int dst_stride,
                                  int src_stride, bool hflip) {
    const int dnext = dst_stride - width;

    // dest = src if src != 255
    if (hflip) {
        FASTBLIT_HFLIP(srcpix1, srcpix4);
    } else {
        FASTBLIT_NORMAL(srcpix1, srcpix4);
    }
}

static void bitmap_fast_blit_col_pal8(uint8_t *dp, uint8_t *sp, int width, int height,
                                      int dst_stride, int src_stride, bool hflip, uint32_t col) {
    const int dnext = dst_stride - width;
    uint32_t colpix4 = (col << 24) | (col << 16) | (col << 8) | (col << 0);

    // dest = col if src != 255
    if (hflip) {
        FASTBLIT_HFLIP(col, colpix4);
    } else {
        FASTBLIT_NORMAL(col, colpix4);
    }
}
