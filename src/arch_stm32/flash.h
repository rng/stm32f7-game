#pragma once

#include <stdint.h>

typedef void (*flash_write_progress_cb_t)(int percent);

void flash_check_code_from_sd(uintptr_t flash_addr, const char *game_fn,
                              flash_write_progress_cb_t progress_cb);
