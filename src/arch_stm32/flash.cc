#include <stdint.h>
#include <string.h>

#include "engine/common.h"
#include "engine/platform.h"
#include "stm32f7xx_hal.h"

#include "arch_stm32/flash.h"

#define FLASH_TIMEOUT_MS (2000)

extern platform_handle_t platform_file_open(const char *filename, bool write);
extern size_t platform_file_read(platform_handle_t h, size_t n, void *b);
extern size_t platform_file_size(platform_handle_t h);
extern bool platform_file_exists(const char *filename);
extern void platform_file_close(platform_handle_t h);

static void flash_write_game_code(uintptr_t flash_addr, size_t code_size, platform_handle_t ch,
                                  flash_write_progress_cb_t progress_cb) {
    uint8_t write_buf[256];
    HAL_StatusTypeDef status;

    ASSERT(code_size < (128 * 1024)); // one sector
    ASSERT((code_size & 3) == 0);
    uint32_t write_addr = flash_addr;
    uint32_t end_addr = flash_addr + code_size;

    status = HAL_FLASH_Unlock();
    ASSERT(status == HAL_OK);

    // Erase game code sector
    FLASH_Erase_Sector(FLASH_SECTOR_5, FLASH_VOLTAGE_RANGE_3);
    FLASH_WaitForLastOperation(FLASH_TIMEOUT_MS);
    CLEAR_BIT(FLASH->CR, (FLASH_CR_SER | FLASH_CR_SNB));

    // Copy game code from SD
    while (write_addr < end_addr) {
        progress_cb(100 * (write_addr - flash_addr) / code_size);
        size_t nread = platform_file_read(ch, sizeof(write_buf), write_buf);
        for (size_t ofs = 0; ofs < nread; ofs += 4) {
            uint32_t word = *((uint32_t *)(&write_buf[ofs]));
            status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, write_addr, word);
            ASSERT(status == HAL_OK);
            write_addr += 4;
        }
    }
    FLASH_WaitForLastOperation(FLASH_TIMEOUT_MS);

    status = HAL_FLASH_Lock();
    ASSERT(status == HAL_OK);
    progress_cb(100);

    SCB_DisableICache();
    SCB_InvalidateICache();
    SCB_EnableICache();
}

void flash_check_code_from_sd(uintptr_t flash_addr, const char *game_fn,
                              flash_write_progress_cb_t progress_cb) {
    // Game code is stored on SD, but since we can't execute code from SD, we copy it to
    // flash then execute from there.
    if (platform_file_exists(game_fn)) {
        // Read the header from SD
        game_header_t sd_header;
        platform_handle_t ch = platform_file_open(game_fn, false);
        platform_file_read(ch, sizeof(game_header_t), &sd_header);
        platform_file_close(ch);

        // Only continue if the header for the game binary is valid
        if (sd_header.magic != GAME_HEADER_MAGIC) {
            return;
        }
        // If the timestamp of the game in flash is older than the version on SD,
        // OR the game name in flash differs from the game name on disk,
        // load the code from SD into flash.
        game_header_t flash_header = *(game_header_t *)flash_addr;
        if ((strncmp(sd_header.name, flash_header.name, sizeof(flash_header.name)) != 0) ||
            (sd_header.buildtime > flash_header.buildtime)) {

            platform_handle_t ch = platform_file_open(game_fn, false);
            size_t code_size = platform_file_size(ch);
            flash_write_game_code(flash_addr, code_size, ch, progress_cb);
            platform_file_close(ch);
        }
    }
}
