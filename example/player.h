#pragma once

#include <stdbool.h>

#include "engine/anim.h"
#include "engine/entitymgr.h"
#include "engine/platform.h"
#include "engine/vector.h"

struct player_t {
    entity_t e;
    v2f vel;
    bool flip;
    bool on_ground, on_ladder;
    anim_t anim;
    input_state_t prev_input;
    int health;
};
