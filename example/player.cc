#include <stdio.h>

#include "engine/collide.h"

#include "game.h"
#include "player.h"

#define PLAYER_CLIMBVEL 40
#define PLAYER_MOVEVEL 80
#define PLAYER_JUMPVEL 160

static const uint16_t _frames_idle[] = {4, 5, 6};
static const uint16_t _frames_walk[] = {0, 1};
static const uint16_t _frames_jump[] = {2, 3, 4};
static const uint16_t _frames_climb[] = {7, 8, 9};
static const anim_seq_t anim_seqs[] = {
    {_frames_idle, NELEMS(_frames_idle), true},
    {_frames_walk, NELEMS(_frames_walk), true},
    {_frames_jump, NELEMS(_frames_jump), false},
    {_frames_climb, NELEMS(_frames_climb), true},
};

enum { IDLE = 0, WALK = 1, JUMP = 2, CLIMBING = 3 };

static void check_collision(player_t *self, float dt, game_ent_context_t *ctx) {
    v2f v = self->vel * dt;
    collide_res_t r = collide_tilemap_check(&ctx->collide, &self->e.pos, v, self->e.bounds);
    if (r.collx) {
        self->vel.x = 0;
    }
    // on vertical collision set our `on_ground` state so we know if we can jump
    if (r.colly) {
        if (self->vel.y > 100) {
            ctx->camera.shake += 5.0f;
        }
        self->vel.y = 0;
        if (!self->on_ground) {
            sound_play_note(&ctx->mixer, 1, SOUND_NOTE_C3, 10);
        }
        self->on_ground = true;
    } else if (self->vel.y > 0) {
        self->on_ground = false;
    }
}

#define CLICKED(key) (input->key && !self->prev_input.key)
#define PRESSED(key) (input->key)
#define BUTTON_JUMP a

static void process_keys(player_t *self, entitymgr_t *em, input_state_t *input,
                         game_ent_context_t *ctx) {
    rect_t bounds = rect_t(self->e.pos.x, self->e.pos.y, self->e.bounds.w, self->e.bounds.h);
    self->on_ladder = collide_tilemap_touching_tile(&ctx->collide, &bounds, COLLIDE_TILE_LADDER);
    int anim = IDLE;
    // jumping
    if (PRESSED(BUTTON_JUMP)) {
        // only jump if we're on the ground
        if ((self->on_ground || self->on_ladder) && CLICKED(BUTTON_JUMP)) {
            self->vel.y = -PLAYER_JUMPVEL;
            sound_play_note(&ctx->mixer, 0, SOUND_NOTE_C4, 64);
        }
    } else if (self->vel.y < 0) {
        // reduce jump velocity if jump button is not held down
        // (allows for jump height control)
        self->vel.y /= 2.0f;
    }
    // climbing ladders
    if (self->on_ladder && !PRESSED(BUTTON_JUMP)) {
        anim = CLIMBING;
        if (PRESSED(up)) {
            self->vel.y = -PLAYER_CLIMBVEL;
        } else if (PRESSED(down)) {
            self->vel.y = PLAYER_MOVEVEL;
        } else {
            self->vel.y = 0;
            anim = IDLE;
        }
    }
    // falling
    if (!self->on_ladder) {
        self->vel.y += GRAVITY * input->dt;
    }
    // movement
    self->vel.x = 0;
    if (PRESSED(left)) {
        self->flip = false;
        self->vel.x = -PLAYER_MOVEVEL;
        if (self->on_ground) {
            anim = WALK;
        }
    } else if (PRESSED(right)) {
        self->flip = true;
        self->vel.x = PLAYER_MOVEVEL;
        if (self->on_ground) {
            anim = WALK;
        }
    }
    if (!self->on_ground && !self->on_ladder) {
        anim = JUMP;
    }
    anim_play(&self->anim, anim);
}

static void player_tick(player_t *self, entitymgr_t *em, input_state_t *input,
                        game_ent_context_t *ctx) {
    process_keys(self, (entitymgr_t *)em, input, ctx);
    anim_update(&self->anim, input->dt);
    check_collision(self, input->dt, ctx);
    self->prev_input = *input;
    render_push_sprite_w(&ctx->render, R_ID_PLAYER, self->e.pos, Z_ENT_PLAYER, self->flip,
                         self->anim.frame);
    // draw health bar in bottom left
    for (int i = 0; i < self->health; i++) {
        v2f pos = (v2f){(float)(i * 7 + 1), 110.f};
        render_push_sprite_s(&ctx->render, R_ID_ICONS, pos, Z_ENT_PLAYER, false, 0);
    }

    // find balls
    auto bounds = entity_bounds(&self->e);
    FOREACH_ENTITY_IN_BOUNDS(ent, em, bounds, ENTITY_FLAG_BALL) {
        if (ent != &self->e) {
            entity_send_event(ent, EVENT_TOUCH);
        }
    }
}

DEF_ENTITY_TYPE(player, player_new, player_tick, NULL, ENTITY_FLAG_COLLIDE | ENTITY_FLAG_PLAYER);

static void player_new(entitymgr_t *em, v2f pos, entity_props_t *props, int id) {
    rect_t bounds = {0, 0, 7, 8};
    auto self = entitymgr_new<player_t>(em, pos, bounds, &player_type, id);
    self->flip = true;
    self->vel = (v2f){0, 0};
    self->health = 3;
    anim_init_sequences(&self->anim, 0.12, anim_seqs);
}
