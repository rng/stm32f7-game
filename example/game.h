#pragma once

#include "engine/camera.h"
#include "engine/collide.h"
#include "engine/entitymgr.h"
#include "engine/render.h"
#include "engine/sound.h"

#include "asset_table.h"

#define GRAVITY (450.0f)

enum {
    // render layers (ordered from furthest to nearest)
    Z_MAP_BG,
    Z_MAP_BASE,
    Z_ENT_BACKROUND,
    Z_ENT_PLAYER,
    Z_ENT_PARTICLE,
    Z_MAP_FG,
    // tile types
    COLLIDE_TILE_LADDER = 2,
    // entity flags
    ENTITY_FLAG_PLAYER = FLAG(1),
    ENTITY_FLAG_BALL = FLAG(2),
    //
    EVENT_TOUCH = 1,
};

typedef struct {
    collide_conf_t collide;
    render_t render;
    sound_mixer_t mixer;
    camera_t camera;
    int ballcount;
} game_ent_context_t;

extern const entity_type_t player_type, spray_type, waterfall_type, torch_type;
