#pragma once

#include "engine/entitymgr.h"
#include "engine/render.h"

#include "game.h"

struct ball_t {
    entity_t e;
    v2f vel;
    int frame;
};

static void ball_tick(ball_t *self, entitymgr_t *em, input_state_t *input,
                      game_ent_context_t *ctx) {
    v2f v = self->vel * input->dt;
    collide_res_t r = collide_tilemap_check(&ctx->collide, &self->e.pos, v, self->e.bounds);

    // bounce with friction
    if (r.collx) {
        self->vel.x *= -0.9f;
    }
    if (r.colly) {
        self->vel.y *= -0.9f;
        self->vel.x *= 0.95f;
    }
    // add gravity
    if (self->vel.y < 150) {
        self->vel.y += (GRAVITY * input->dt);
    }
    // draw sprite
    render_push_sprite_w(&ctx->render, R_ID_BALL, self->e.pos, Z_ENT_PLAYER, false, self->frame);
    // respawn ball once it settles
    if (r.colly && ABS(self->vel.y) < 5.0f) {
        entitymgr_remove(em, &self->e);
        ctx->ballcount--;
    }
}

static void ball_event(ball_t *self, int event, void *param) {
    if (event == EVENT_TOUCH) {
        self->frame = 1;
    }
}

DEF_ENTITY_TYPE(ball, ball_new, ball_tick, ball_event, ENTITY_FLAG_COLLIDE | ENTITY_FLAG_BALL);

static void ball_new_vel(entitymgr_t *em, v2f pos, v2f vel) {
    rect_t bounds = {0, 0, 7, 8};
    auto self = entitymgr_new<ball_t>(em, pos, bounds, &ball_type);
    self->vel = vel;
}

static void ball_new(entitymgr_t *em, v2f pos, entity_props_t *props, int id) {
    ball_new_vel(em, pos, (v2f){0, 0});
}
