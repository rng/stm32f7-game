#include <math.h>
#include <string.h>

#include "engine/asset.h"
#include "engine/debug.h"
#include "engine/entitymgr.h"
#include "engine/font.h"
#include "engine/render.h"

#include "ent_ball.h"
#include "ent_waterfall.h"
#include "game.h"

enum {
    BALL_COUNT = 300,
};

typedef struct game_state_t {
    v2i screen_size;
    tilemap_t map;
    entitylist_t entlist;
    entitymgr_t entitymgr;
    game_ent_context_t context;
    entity_t *player;
} game_state_t;

platform_api_t *platform;

static const entity_type_t *const entity_types[] = {
    &player_type,
    &spray_type,
    &waterfall_type,
    &torch_type,
};

int map_tile(void *context, int x, int y) {
    tilemap_t *map = (tilemap_t *)context;
    if (tilemap_inside(map, x, y)) {
        // are we on a ladder (check against ladder tiles from our tileset)?
        int decor_tile = tilemap_tile(map, 0, x, y);
        int collide_tile = tilemap_tile(map, 1, x, y);
        if (decor_tile == 79 || decor_tile == 80) {
            return COLLIDE_TILE_LADDER;
        }
        // are we on a one-way tile (platforms)?
        if (collide_tile == 19 || collide_tile == 20 || collide_tile == 31) {
            return COLLIDE_TILE_ONEWAY;
        }
        // otherwise can we walk through this tile
        if (collide_tile == 0) {
            return COLLIDE_TILE_PASSABLE;
        }
    }
    return COLLIDE_TILE_IMPASSABLE;
}

void game_reset(game_state_t *game) {
    entitymgr_reset(&game->entitymgr);
    entitymgr_spawnlist(&game->entitymgr, &game->entlist);

    entity_t *e = game->entitymgr.entities;
    while (e) {
        if (e->type == &player_type) {
            game->player = e;
            break;
        }
        e = e->next;
    }
    ASSERT(game->player);

    v2f half_screen_size = v2f(game->screen_size.x / 2, game->screen_size.y / 2);
    camera_set_pos(game->context.camera, game->player->pos - half_screen_size);
}

struct game_state_t *game_init(platform_api_t *platform_api, platform_params_t *params) {
    platform = platform_api;
    struct game_state_t *game = (game_state_t *)platform->alloc(sizeof(game_state_t));

    game->screen_size = v2i(params->screen_w, params->screen_h);

    sound_init(&game->context.mixer, params->audio_rate, 2);
    render_init(&game->context.render, R_ID_MAX);
    entitymgr_register_types(&game->entitymgr, entity_types, NELEMS(entity_types));

    for (size_t i = 0; i < NELEMS(asset_map); i++) {
        render_load_sprite(&game->context.render, asset_map[i].id, asset_map[i].filename);
    }
    for (size_t i = 0; i < NELEMS(sound_map); i++) {
        sound_instrument_pcm(&game->context.mixer, sound_map[i].id, sound_map[i].filename);
    }

    asset_load_tilemap("data/testmap.dat", &game->map, &game->entlist);
    game->map.tileset = game->context.render.sprites[R_ID_TILES];

    v2i world_size =
        v2i(game->map.width * game->map.tilesize, game->map.height * game->map.tilesize);
    camera_init(game->context.camera, game->screen_size, world_size, 50);

    game_ent_context_t *ctx = &game->context;
    sound_instrument_synth(&ctx->mixer, 0, SOUND_WAVEFORM_SQR, 0, 2500, 120); // jump
    sound_instrument_synth(&ctx->mixer, 1, SOUND_WAVEFORM_SQR, 0, 1000, -50); // land

    game->context.collide = (collide_conf_t){
        game->map.tilesize,
        map_tile,
        &game->map,
    };

    game_reset(game);
    return game;
}

void game_term(game_state_t *game) {
    entitymgr_reset(&game->entitymgr);
    render_term(&game->context.render);
    sound_term(&game->context.mixer);
    platform->free(game);
}

bool game_tick(game_state_t *game, input_state_t *input, uint8_t *framebuffer) {

    bitmap_t screen = BITMAP8(game->screen_size.x, game->screen_size.y, framebuffer);
    bitmap_fill(&screen, 0);

    if (game->context.ballcount < BALL_COUNT) {
        game->context.ballcount++;
        v2f pos = game->player->pos.offset(0, -10);
        v2f vel = (v2f){(float)(5 * RANDINT(-20, 20)), (float)(-10 * RANDINT(10, 20))};
        ball_new_vel(&game->entitymgr, pos, vel);
    }

    // update entities
    entitymgr_tick(&game->entitymgr, input, &game->context);
    // push map to renderer
    render_push_tiles_w(&game->context.render, &game->map, 0, Z_MAP_BG);
    render_push_tiles_w(&game->context.render, &game->map, 1, Z_MAP_BASE);
    render_push_tiles_w(&game->context.render, &game->map, 2, Z_MAP_FG);
    // update camera
    rect_t view = camera_update(game->context.camera, game->player->pos, input->dt);
    // draw
    render_draw(&game->context.render, &screen, &view);
    return false;
}

void game_audio(game_state_t *game, void *audio_buf, size_t audio_buf_len) {
    sound_mix(&game->context.mixer, audio_buf, audio_buf_len);
}
