#pragma once

#include "engine/anim.h"
#include "engine/entitymgr.h"
#include "engine/timeout.h"
#include "engine/vector.h"

#include "game.h"

struct waterfall_t {
    entity_t e;
    timeout_t life;
};

struct water_t {
    entity_t e;
};

struct spray_t {
    entity_t e;
    anim_t anim;
};

struct torch_t {
    entity_t e;
    anim_t anim;
};

static void spray_tick(spray_t *self, entitymgr_t *em, input_state_t *input,
                       game_ent_context_t *ctx) {
    render_push_sprite_w(&ctx->render, R_ID_SPRAY, self->e.pos, Z_ENT_PARTICLE, false,
                         self->anim.frame);
    anim_update(&self->anim, input->dt);
}

DEF_ENTITY_TYPE(spray, spray_new, spray_tick, NULL, ENTITY_FLAG_NONE);

static void spray_new(entitymgr_t *em, v2f pos, entity_props_t *props, int id) {
    rect_t bounds;
    auto self = entitymgr_new<spray_t>(em, pos.offset(-8, 0), bounds, &spray_type, id);
    static const uint16_t frames[] = {0, 1, 2};
    anim_init_sequence(&self->anim, 0.08, frames, NELEMS(frames), true);
}

static void water_tick(water_t *self, entitymgr_t *em, input_state_t *input,
                       game_ent_context_t *ctx) {
    v2f v = {0, 100 * input->dt};
    collide_res_t r = collide_tilemap_check(&ctx->collide, &self->e.pos, v, self->e.bounds);

    render_push_sprite_w(&ctx->render, R_ID_WATER, self->e.pos, Z_ENT_BACKROUND, false, 0);

    if (r.collx || r.colly) {
        entitymgr_remove(em, &self->e);
    }
}

DEF_ENTITY_TYPE(water, water_new, water_tick, NULL, ENTITY_FLAG_NONE);

static void water_new(entitymgr_t *em, v2f pos, entity_props_t *props, int id) {
    rect_t bounds = {0, 0, 8, 8};
    entitymgr_new<water_t>(em, pos, bounds, &water_type, id);
}

static void waterfall_tick(waterfall_t *self, entitymgr_t *em, input_state_t *input,
                           game_ent_context_t *ctx) {
    if (timeout_expired(&self->life, input->dt, 0)) {
        timeout_init(&self->life, RANDFLOAT(0.5f, 1.2f));
        water_new(em, self->e.pos, NULL, -1);
    }
}

DEF_ENTITY_TYPE(waterfall, waterfall_new, waterfall_tick, NULL, ENTITY_FLAG_NONE);

static void waterfall_new(entitymgr_t *em, v2f pos, entity_props_t *props, int id) {
    rect_t bounds;
    auto self = entitymgr_new<waterfall_t>(em, pos, bounds, &waterfall_type, id);
    timeout_init(&self->life, 0.1);
}

static void torch_tick(torch_t *self, entitymgr_t *em, input_state_t *input,
                       game_ent_context_t *ctx) {
    render_push_sprite_w(&ctx->render, R_ID_TORCH, self->e.pos, Z_ENT_BACKROUND, false,
                         self->anim.frame);
    anim_update(&self->anim, input->dt);
}

DEF_ENTITY_TYPE(torch, torch_new, torch_tick, NULL, ENTITY_FLAG_NONE);

static void torch_new(entitymgr_t *em, v2f pos, entity_props_t *props, int id) {
    rect_t bounds;
    auto self = entitymgr_new<torch_t>(em, pos, bounds, &torch_type, id);
    static const uint16_t frames[] = {0, 1, 2};
    self->anim.index = RANDINT(0, 3);
    anim_init_sequence(&self->anim, RANDFLOAT(0.1f, 0.15f), frames, NELEMS(frames), true);
}
