#!/usr/bin/env python3
import glob
import sys
import os

HDR="""// This file is autogenerated by assetheader.py.

#pragma once

typedef struct {
    int id;
    const char *filename;
} asset_mapping_t;

enum {"""


def makemap(fns, prefix, elt, ext):
    r = []
    for fn in sorted(fns):
        base = os.path.splitext(os.path.basename(fn))[0]
        fid = prefix + base.split("_")[elt].upper()
        fn = fn.replace(ext, ".dat")
        r.append((fid, os.sep.join(fn.split(os.sep)[1:])))
    return r


def main():
    basedir = sys.argv[1]
    outname = sys.argv[2]
    files = sys.argv[3:]

    sprites = []
    sounds = []

    for fn in files:
        assert fn.endswith(".dat")
        assert fn.startswith("build")
        fn = os.sep.join(fn.split(os.sep)[2:])
        wav = fn.replace(".dat", ".wav")
        png = fn.replace(".dat", ".png")
        if os.path.exists(wav):
            sounds.append(wav)
        if os.path.exists(png):
            sprites.append(png)

    spritemap = makemap(sprites, "R_ID_", 0, ".png")
    soundmap = makemap(sounds, "S_ID_", -1, ".wav")

    outfile = open(outname, "w")
    def out(s=""):
        outfile.write("{}\n".format(s))

    out(HDR)
    for i, (sid, _) in enumerate(spritemap):
        out("    {}{},".format(sid, " = 0" if i == 0 else ""))
    out("    R_ID_MAX,")
    out("    //")
    for i, (sid, _) in enumerate(soundmap):
        out("    {}{},".format(sid, " = 0" if i == 0 else ""))
    out("    S_ID_MAX,")
    out("};")
    out()
    out("// clang-format off")
    out("static const asset_mapping_t asset_map[] = {")
    for sid, sfn in spritemap:
        out("    {{{}, \"{}\"}},".format(sid, sfn))
    out("};")
    out()
    out("static const asset_mapping_t sound_map[] = {")
    for sid, sfn in soundmap:
        out("    {{{}, \"{}\"}},".format(sid, sfn))
    out("};")
    out("// clang-format on")


main()
