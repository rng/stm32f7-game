#!/usr/bin/env python3

import os
import re
import sys
import struct
import json
import enum
import math
import wave
import argparse

from PIL import Image


class BitmapFormat(enum.IntEnum):
    """ This must match bitmap_format_t in bitmap.h """
    RGBA8888 = 1
    RGB565   = 2
    RGBA5551 = 3
    PAL8     = 4


class AssetType(enum.IntEnum):
    """ This must match asset_type_t in asset.c """
    IMAGE = 1
    TILEMAP = 2
    PALETTE = 3
    WAVE = 4


def rgba8888_to_rgb565(r, g, b, a):
    return ((r >> 3) << 11) | ((g >> 2) << 5) | (b >> 3)


def rgba8888_to_rgba5551(r, g, b, a):
    a = 1 if a > 0 else 0
    return ((r >> 3) << 11) | ((g >> 3) << 6) | ((b >> 3) << 1) | (a)


def rgba8888_to_pal8(x, y, r, g, b, a, palette):
    if a < 128:
        return 255
    mindist, minidx = 1000000, -1
    for i, (cr, cg, cb) in enumerate(palette):
        dist = math.sqrt((r-cr)**2 + (g-cg)**2 + (b-cb)**2)
        if dist < mindist:
            mindist = dist
            minidx = i
    assert mindist < 10, "Can't find approximate for colour (%d,%d,%d) at %d,%d" % (r, g, b, x, y)
    return minidx


def make_header(typ, version=1):
    return struct.pack("ccccII", b'G', b'a', b'm', b'e', version, typ)


def make_header_img(w, h, tilew, tileh, fmt):
    return make_header(AssetType.IMAGE) + struct.pack("IIIII", w, h, tilew, tileh, fmt)


def make_header_map(w, h, tilesize, layercount, etypecount, entcount):
    return make_header(AssetType.TILEMAP) + struct.pack("IIIIII", w, h,
                                                        tilesize, layercount,
                                                        etypecount, entcount)


def make_header_pal(colour_count):
    return make_header(AssetType.PALETTE) + struct.pack("I", colour_count)


def make_header_wave(channel_count, sample_count):
    return make_header(AssetType.WAVE) + struct.pack("II", channel_count, sample_count)


def parse_img(w, h, data, palette):
    rdata = bytes()
    for j in range(h):
        for i in range(w):
            pix = data[i,j]
            rdata += struct.pack("B", rgba8888_to_pal8(i, j, *pix, palette=palette))
    return rdata


def convert_img_bin(w, h, tilew, tileh, data, assetname):
    return make_header_img(w, h, tilew, tileh, BitmapFormat.PAL8) + data


def convert_img_hdr(w, h, tilew, tileh, data, assetname):
    template = """
#pragma once
static const uint8_t {arrname}[] = {{ {arrdata} }};

static const bitmap_t {assetname} = {{
    .width = {w},
    .height = {h},
    .tile_width = {tilew},
    .tile_height = {tileh},
    .format = BITMAP_FORMAT_PAL8,
    .data = (uint8_t *){arrname},
}};
"""
    arrdata = ",".join("0x%02x" % x for x in data)
    arrname = assetname + "_data"
    return template.format(w=w, h=h, tilew=tilew, tileh=tileh, arrname=arrname, assetname=assetname, arrdata=arrdata).encode("utf8")


def pack_map_entities(types, ents):
    def pack_props(props):
        r = b''
        for k, v in props:
            r += struct.pack("II", k, v)
        return r

    types = list(sorted(types))
    r = b''.join(struct.pack('12s', et.encode('utf8')) for et in types)
    for etype, eid, x, y, props in ents:
        eidx = types.index(etype)
        r += (struct.pack('HHHHH', len(props), eidx, eid, x, y) + pack_props(props))
    return r


def hash_propname(name):
    # 32 bit FNV hash (https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function)
    h = 0x811c9dc5
    for ch in name.encode("utf8"):
        h = (h * 16777619) & 0xFFFFFFFF
        h = h ^ ch
    return h


def hash_props(props):
    return [(hash_propname(k), v) for (k, v) in props.items()]


def convert_map(mapjson):
    def _check_tiles(layer, w, h):
        for i in range(w):
            for j in range(h):
                tile = layer['data'][i + j*w]
                if tile > 255:
                    raise Exception('Tile %d > 255 at %d,%d on layer %s' % (
                        tile, i,j, layer['name']))

    layercount = 0
    layers = b''
    entities = []
    entity_types = set()
    w = h = None
    tilesize = mapjson['tilewidth']
    assert tilesize, 'Invalid tile size'
    assert mapjson['tileheight'] == tilesize, 'Expected square tiles'
    for layer in mapjson['layers']:
        if layer['type'] == 'tilelayer':
            w, h = layer['width'], layer['height']
            _check_tiles(layer, w, h)
            layers += bytearray(layer['data'])
            layercount += 1
        elif layer['type'] == 'objectgroup':
            for obj in layer['objects']:
                typ = obj['name']
                assert len(typ) < 12, 'Entity type name must be <12 chars'
                entity_types.add(typ)
                props = hash_props(obj.get('properties', {}))
                entities.append((typ, obj['id'], obj['x'], obj['y'], props))
        else:
            print('Ignoring unhandled map layer "%s"' % layer['type'])

    ents = pack_map_entities(entity_types, entities)

    hdr = make_header_map(w, h, tilesize, layercount, len(entity_types), len(entities))
    return hdr + layers + ents


def parse_palette(f):
    lines = f.readlines()
    assert len(lines) and lines[0] == "GIMP Palette\n", "Expected a GIMP palette file"
    colours = []

    for line in lines[1:]:
        if line.startswith("#"):
            continue
        ls = line.strip().split()
        assert len(ls) >= 3, ("expected and RGB entry, got %s" % (line.strip()))
        rgb = list(map(int, ls[:3]))
        colours.append(rgb)

    return colours


def convert_palette(f):
    colours = parse_palette(f)
    colbytes = b''
    for colour in colours:
        colbytes += bytearray(colour)
    return make_header_pal(len(colours)) + colbytes


def convert_wave(f):
    wav = wave.open(f)
    nchannels, sampwidth, framerate, nframes, _, _  = wav.getparams()
    assert sampwidth == 2, "16bit samples required"
    assert framerate == 8000, "8KHz sample rate required, got {}".format(framerate)
    data = wav.readframes(nframes)
    return make_header_wave(nchannels, nframes) + data


def convert():
    parser = argparse.ArgumentParser()
    parser.add_argument("--header", "-H", action="store_true")
    parser.add_argument("--palette", "-P")
    parser.add_argument("input")
    parser.add_argument("output")
    args = parser.parse_args()
    infn = args.input
    in_name, in_ext = os.path.splitext(infn)
    in_file = os.path.basename(infn)

    if in_ext == '.png':
        palette = parse_palette(open(args.palette))

        img = Image.open(infn)
        img = img.convert('RGBA')

        tilew, tileh = w, h = img.size
        sizematch = re.match('.*_(\d+)x(\d+)', in_name)
        if sizematch:
            tilew, tileh = map(int, sizematch.groups())
        imgdata = parse_img(w, h, img.load(), palette)
        imgname = "_".join(os.path.basename(in_name).split("_")[:-1])
        if args.header:
            data = convert_img_hdr(w, h, tilew, tileh, imgdata, imgname)
        else:
            data = convert_img_bin(w, h, tilew, tileh, imgdata, imgname)
    elif in_ext == '.json':
        mapjson = json.load(open(infn))
        data = convert_map(mapjson)
    elif in_ext == '.gpl':
        data = convert_palette(open(infn))
    elif in_ext == '.wav':
        data = convert_wave(infn)
    else:
        raise Exception('unhandled file type %s' % in_ext)
    open(args.output, 'wb').write(data)


if __name__ == '__main__':
    convert()
