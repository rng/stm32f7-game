# Summary

Set up the basics of audio playback and mixing in our engine.

# Background

NOTE: there are two uses of the word `sample` that we'll cover here:

1. A small audio clip, eg a sound effect or instrument note.
2. A single integer value representing the amplitude of a sampled waveform

For clarity in this RFC we'll refer to the first case as a `clip` and the
second case as a `sample`.

In [RFC-0009][rfc9] we defined the platform audio API. The platform API now
provides a "fill this audio buffer" callback:

```c
void (*audio)(game_state_t *game, void *audio_buf, size_t audio_len);
```

It's up to the game and engine to implement this callback and fill the audio
buffer with appropriate samples during gameplay. In RFC-0009 we talked about
both [PCM][pcm] clip playback and using a [synthesiser][synth]. Building a
synth is likely the approach we'll take long-term, as it allows for much more
compact representation and storage of our in game sounds. However it requires
building more engine code and tooling (since the synth would be custom, it'd
need a tool for composing or converting sound and music), than playing back
simple PCM audio clips.

As such, we'll start with a simple clip playback system. These clips will
be stored in RAM (and thus consume a lot of our console's available memory),
but will enable basic testing of the audio system in-game.

Additionally, we will have support for audio mixing such that we can play back
multiple clips at once ([polyphony][polyphony]). This enables in game sound
effects to play back at the same time as the music, as well as allowing more
interesting music.

# Overview

There are a couple parameters we care about when talking about PCM audio:

- Number of channels: number of parallel audio channels. eg. mono, stereo,
  [5.1 surround][5pt1]
- Sample rate: number of audio samples per second. eg. 44100Hz for CD audio
- Bit depth: number of bits per sample. eg. 16bits for CD audio

These parameters affect the fidelity of the audio, but also the amount of
storage (RAM/Flash) and processing power required during playback. For our
implementation, we will assume a fixed set of parameters for all audio in our
system: 1 channel, 8000Hz, 16bits/sample (both input clip data, and the
output path to speakers). This is a low spec compared to modern audio systems,
but is fine for a retro console.

By fixing the parameters we simplify processing that needs to happen during
playback. That is, we can assume our clips will already be in the correct
format and won't need to be [resampled][resample].

With fixed parameters, our audio system can be reduced to the follow:

1. Maintain a list of currently playing clips, along with the current
   position for each clip (that is how far through the clip's playback we
   are).
2. Each time the we're called by the platform layer, we iterate for the number
   of requested samples and for each iteration we sum the current samples from
   each playing clip.
3. If in doing so we finish a clip, we simply use 0 for the remaining samples.

Thus the resulting audio buffer returned to the platform code has the summation
of all playing samples. Our initial implementation won't handle independent
volume for each clip, but adding support would just involve scaling each clip
sample before summing.

# Implementation

We'll break the implementation down into the clip asset pipeline and the
mixer/playback system.

## Clip assets

Audio clips will be stored in game as an array of 16 bit int samples. A clip
structure will contain a pointer to sample data and a sample count. We assume
8000Hz sample rate everywhere.

Our asset converter will consume [WAV][wav] files as they are simple, supported
by all audio generation/editing tools, and are easily consumed by Python using
the [wave][wave] package. The asset converter will expect 8000Hz, Mono wave
files to simplify the conversion.

Asset loading will be similar to bitmap and tileset loading in the engine.

## Playback and Mixing

We'll have the concept of a "channel" in our playback system. A channel will
store the state of a playing clip; so if we have 4 channels, we can play at
most 4 clips at once. Our channel structure will look like:

```c
typedef struct {
    bool active;
    int pos;
    sound_clip_t clip;
} sound_channel_t;
```

Where `active` tells the mixer whether the channel is currently playing a
sample, `pos` is the index into the clip sample data, and `clip` is the audio
clip we're currently playing on the channel.

To start playing a clip, we simply iterate through the channels looking for an
inactive channel. This does mean we could potentially not find an available
channel (eg if there's a lot of action and the game is playing lots of
effects). In this case we could choose to stop playing an existing clip and
replace it with a new clip, or simply drop the request to play the new clip.

As described above, our mixer will iterate through active channels and sum the
clip sample data into the final buffer passed to the platform layer.

```c
for (int chidx=0; chidx<SOUND_NUM_CHANNELS; chidx++) {
    // iterate through active channels
    sound_channel_t *ch = &mixer->channels[chidx];
    if (ch->active) {
        // From the clip we copy N samples, where N is the smaller of:
        // - the number of samples requested by the platform layer
        // - the number of samples remaining in the clip
        int src_samples = MIN(ch->clip->sample_count - ch->pos, dst_samples);
        if (src_samples > 0) {
            // if there are samples to copy, copy them and update our position
            // in the clip
            int16_t *src_buf = ch->clip->samples + ch->pos;
            memcpy(dst_buf, src_buf, src_samples * 2);
            ch->pos += src_samples;
        } else {
            // otherwise we're doing playing back the clip - mark the channel
            // as inactive
            ch->active = false;
        }
    }
}
```

# Alternatives

The outlined system provides a bare minimum framework for playing back samples
in game. We'll want to add support for synthesising sounds rather than playing
back raw samples. However this can be added to our existing channel
infrastructure - each channel could play back either a sample, or a synthesised
sound.


[rfc9]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/rfc-0009-audio-platform.md
[pcm]: https://en.wikipedia.org/wiki/Pulse-code_modulation
[synth]: https://en.wikipedia.org/wiki/Programmable_sound_generator
[polyphony]: https://en.wikipedia.org/wiki/Polyphony
[5pt1]: https://en.wikipedia.org/wiki/5.1_surround_sound
[resample]: https://en.wikipedia.org/wiki/Sample-rate_conversion
[wav]: https://en.wikipedia.org/wiki/WAV
[wave]: https://docs.python.org/3/library/wave.html
