# Open Projects / TODOs

## Small

- [MSC] Add commandline configurable PC pixel scaling
- [MSC] Add submodule-aware makefile for games submoduling the engine

## Medium

- [GFX] Add palette-setting platform API
- [GFX] Add mode setting (resolution/bit-depth) platform API
- [GFX] Add sprite animations
- [AST] Add asset metadata file parsing
- [DBG] Add debug print on STM & PC
- [DBG] Add basic profiler
- [DBG] Add fault handler and info screen on STM
- [DBG] Add backtrace handler on PC
- [MSC] Add "home/system" screen with game selection
- [HW] Choose LCD module

## Large

- [SND] Add sound/music synthesizer
- [ENT] Add core object/entity system
- [GFX] Optimise (SIMD) bitmap blitter
- [AST] Add asset hotloading support
- [MSC] Add USB mass storage to STM platform to load game data files
- [MSC] Add load/save support
