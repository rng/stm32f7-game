# Summary

Outline the high level block diagram for the STM32-based hardware.

# Background

Thus far, we've used the [STM32 Discovery][stm32disco] to validate the platform
and engine implementation on the STM32 microcontroller. The Discovery is a
great dev platform but isn't a great fit as a real playable console - it lacks
physical buttons, isn't quite the right form factor, and has a bunch of
unneeded peripherals (Ethernet, SPDIF, etc).

We're going to design a custom board roughly based on the Discovery, but with
the specific peripherals we want for a handheld console. This RFC will cover
the high level block diagram of our system: what the major components are, and
how they connect.

# Overview

The diagram below shows the high-level block diagram. Boxes represent groups of
(or single) physical components and arrows indicate signals/wires connecting
those components (along with the protocol/type of signal).

```
                                                       .-----------.
                   .-----------. 3V  .-----. Backlight |           |
                .->| Regulator |---->|  x  |---------->|    LCD    |
                |  '-----------'     '-----'           |           |                  .---------------.
                |                       ^              '-----------'       SPI & GPIO |               |
           3.7V |  .-----------. 3.3V   |                    ^             .--------->|    SD Card    |
                +->| Regulator |--->    |                    | 18bit RGB   |          |               |
.---------.     |  '-----------'        |          .-------------------.   |          '---------------'
|         |   .---.                     |    PWM   |                   |   |
| Battery |-->| x | power switch        '----------|                   |   |
|         |   '---'                                |                   |   |          .---------------.
'---------'     |               .---------.  ADC   |                   |<--'     GPIO |               |
     ^          '-------------->| divider |------->|                   |        .---->| Buttons + LED |
     |                          '---------' GPIO   |                   |        |     |               |
.---------.     .--------------------------------->|        CPU        |        |     '---------------'
|         |-----'               .---------.        |                   |<-------'
| Charger |<------------+------>| divider |------->|                   |
|         |       5V5   |       '---------' GPIO   |                   |              .---------------.
'---------'             |                          |                   |              |               |
                     .-----.             USB Data  |                   |------------->|   Amplifiers  |
                     | USB |<--------------------->|                   |     Audio    |               |
                     '-----'                       |                   |              '---------------'
                                                   '-------------------'
```

# Implementation

We'll dive into the 4 major sections of the block diagram below:

## Power and Charging

Our console will be powered by a 3.7V [lithium ion][liion] battery. These
[batteries][battery] are cheap and common in most portable hardware and
supporting hardware (chargers, etc) are readily available. 5V will be supplied
from USB for charging the battery. The 5V supply will also be fed into the CPU
via a [resistor divider][rdiv], not for power, but to enable the CPU to detect
when USB is connected. A charger chip will handle all the logic for charging
ass lithium ions require a rather complex [charging process][charge]. The
charger will indicate charging status to the CPU via a single general purpose
IO ([GPIO][gpio]).

Power to the rest of the system will be supplied by the battery, via a physical
on/off switch. This switch will not disable charging, rather it will disable
the system's main (3.3V supply). The main supplies will be a 3.3V supply to all
of the digital logic, and a 3.0V supply to the LCD backlight (which is just a
bunch of white LEDs). Both of the supply voltages will be regulated from the
3.7V battery supply using [buck converters][buck] (as they're more efficient
than linear regulators).

Battery charge measurement for lithium ions is more complex than other battery
chemistries due to their [flat discharge curve][discharge]. Advanced "fuel
gauge" chips measure the current flowing in and out of the battery then
integrating that value to perform "coulomb counting". This gives a much more
accurate state of charge estimate for the battery, but comes at additional cost
and complexity - the fuel gauge typically communicates over I2C, and requires
configuration and calibration. We will perform the bare minimum of state of
charge measurement using a resistor divider and STM32 ADC to get a very rough
estimate.

## Display

There are 3 main ways to interface with the type of LCD we'll be using in the
console:

- RGB (often called "Dot Clock" mode)
- MCU (often called "8080" mode)
- SPI

Lets talk about the RGB mode first. When driving an LCD over an RGB interface
the signals between processor and the LCD are as follows:

- Red data (5-8 signals depending on bit depth)
- Blue data (5-8 signals depending on bit depth)
- Green data (5-8 signals depending on bit depth)
- Horizontal and Vertical Sync
- Dot clock

These signals can be thought of as a direct drive of the LCD pixels. To display
an image the processor sets the colour value for one pixel on the red, green
and blue signals, then toggles the dot clock. This signals to the LCD that a
the RGB data is ready which it then shifts in and uses to drive the first pixel
colour. This process repeats for each pixel on the display for a single frame
(with pulses of the horizontal and vertical sync signals at the start of each
frame and line respectively).

RGB mode gives the most control over the LCD - there's no smarts in the display
and the CPU does all the heavy lifting / timing control (though in our case the
STM32's LCD peripheral manages that for us).

The other two modes move some smarts into the display. In both MCU and SPI
modes, the LCD includes a controller chip that includes an onboard framebuffer.
The CPU uses the MCU or SPI interface to the controller to write image data
into the LCDs framebuffer. The controller chip then drives the display with the
data from the framebuffer. The difference between SPI and MCU mode is the
interface through which read and write commands are sent to the LCD controller
chip - either via a 4 wire [SPI bus][spi], or via a 8-18bit parallel bus.

SPI and MCU mode reduces the load on the CPU - since the display controller has
an onboard framebuffer, CPU doesn't need to store the full framebuffer in RAM,
and can use fewer signals to drive the display.

Most displays in the size range we want include an onboard controller, since
they're often driven by low-power microcontrollers. Our STM32 supports both MCU
and RGB mode displays, and both modes end up sharing a significant number of
pins on the STM32. So for now we'll leave this open, but will probably end up
with an RGB interface for the additional flexibility we gain in driving the
display.

## Audio Output

The high level requirement for our audio system simple: a way to transform
relatively low data rate (~8K samples/second) digital audio data into an analog
audio signal to drive both internal speakers or connected headphones:

    digital sample data --> digital to analog conversion --> amplifier --> speaker / headphones

We have a couple options for the digital to analog conversion stage:

- STM32 Pulse-Width Modulation ([PWM][pwm])
- STM32 Digital to Analog Converter ([DAC][dac])
- External [Codec][codec]

The first two options (PWM and DAC) use onboard peripherals of the STM32
microcontroller. In the first case our digital samples are converted by the PWM
peripheral into a stream of pulses. The higher the digital sample value is, the
higher the percentage of the pulse is positive. The resulting pulse stream is
fed through a low pass filter which transforms the pulse stream into an analog
signal. Using PWM is cheap as the peripheral is built into the STM and needs
cheap external filtering.

However, if we have DAC hardware on the microcontoller (which we do on our
STM32), we should use it - a DAC peripheral does all the work of converting
sample data to analog voltages, no external filtering needed. In both the PWM
and DAC cases, we can use the STM32's DMA hardware to automatically transfer
blocks of sample (see [RFC-0009][rfc9]) from memory to the output peripheral.

The final option is to use an external Codec chip. These chips will range from
simple single-channel output-only to full 5.1 output + dual mic input ADC+DAC
systems, often with built-in amplifiers and filtering. The interface between
the Codec and the microcontroller is typically [I2S][i2s] - a serial bus,
similar to SPI over which we transfer digital sample data. A codec will
typically combine the DAC and amplifier (at least for headphones).

For high quality audio output a Codec is the obvious choice. But we're going
for a retro chip-tune-y sound - we probably won't go higher than 8Khz, and may
only do mono audio. As such we'll initially plan for just using the onboard DAC
of the STM32.

The output of the DAC needs an amplifier to drive the (relatively) low
[impedance][impedance] headphones or speakers. This will also be the point at
which we adjust volume - while we could adjust the "volume" of our digital
samples, this leads to less resolution at lower volumes. Headphone jacks will
typically include a switch that disengages when the headphones are inserted.
This can be used to switch between driving the speakers and the headphones. Our
aim will be to find an amplifier that can drive both a mono speaker and
(potentially stereo) headphones.

## User Input + SD

User input is very simple - we just have a set of pushbuttons connected to the
STM32's GPIOs. We will likely also have an LED or two for non-LCD based signals
(power, charge, etc). The SD interface is also relatively simple - the STM32
has onboard SD/MMC hardware support, so we directly wire those to an SD card
socket (with a GPIO input to the card-detect switch on the socket).

# Concerns / Open Questions

The specific component choices are still largely open and will be discussed in
a future RFC. Most of these decisions will be validated by the first prototype
implementation of the board, and so still may change as we learn things from
the prototype.

[stm32disco]: https://www.st.com/en/evaluation-tools/32f746gdiscovery.html
[liion]: https://en.wikipedia.org/wiki/Lithium-ion_battery
[battery]: https://www.sparkfun.com/products/13855
[rdiv]: https://en.wikipedia.org/wiki/Voltage_divider
[charge]: https://batteryuniversity.com/learn/article/charging_lithium_ion_batteries
[gpio]: https://en.wikipedia.org/wiki/General-purpose_input/output
[buck]: https://en.wikipedia.org/wiki/Buck_converter
[discharge]: https://www.powertechsystems.eu/home/tech-corner/lithium-ion-state-of-charge-soc-measurement/
[spi]: https://en.wikipedia.org/wiki/Serial_Peripheral_Interface
[pwm]: https://en.wikipedia.org/wiki/Pulse-width_modulation
[dac]: https://en.wikipedia.org/wiki/Digital-to-analog_converter
[codec]: https://en.wikipedia.org/wiki/Audio_codec
[rfc9]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/rfc-0009-audio-platform.md
[i2s]: https://en.wikipedia.org/wiki/I%C2%B2S
[impedance]: https://en.wikipedia.org/wiki/Electrical_characteristics_of_dynamic_loudspeakers
