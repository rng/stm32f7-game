# Summary

Outlines a minimal user interface library for game and system UI.

# Background

The home screen / system [code][syscode] is the first code in the system to draw a user interface. It's also a mess - mixing code to draw widgets (using `bitmap` primitives) and handling user input, with lots of magic numbers defining dimensions. This style of UI is hard to change and update as we have to manually keep track of where every widget is as well as manually manage rendering and input-handling of the widgets.

To make UIs easier to build we'll build a library that handles rendering and input-handling of simple UIs. This can be used both in games as well as for the home screen.

## Immediate and Retained Mode

[Immediate mode][immediate] is a common pattern in graphics libraries, as an alternative to [retained mode][retained] libraries. One of the core differences between the two patterns is where the state for the scene (be it a complex 3d scene or a user interface "scene" in the form of a window of widgets) is stored. For retained mode, the scene is stored in the library itself, whereas in an immediate mode system the scene is stored in the calling code. For an explicit pseudo-code example of a simple UI with two widgets:

```py
def scene_init():
    window = ui_new_window("title")
    lbl = ui_new_label("this is a label)
    window.add_child(lbl)
    btn = ui_new_button("click me")
    btn.on_click_event(lambda: print("clicked the button"))
    window.add_child(btn)

def scene_frame():
    ui_handle_events()
    ui_render()
```

In this retained mode example, the UI is created in the init section and for each `ui_new_*` call the UI library's internal model of the UI is built up (including layout of the widgets using `add_child` and callback handling with `on_click_event`). Once the UI is constructed the calling code can step back and the UI library handles all the processing and rendering.

Compare this to an immediate mode implementation:

```py
def scene_init():
    pass

def scene_frame():
    ui_start()
    ui_window("title")
    ui_label("this is a label")
    if ui_button("click me"):
        print("clicked the button")
    ui_end()
```

In this implementation, the library has no knowledge of the structure of the UI (that is, compared to the retained example, the library has no idea that the button is a child of the window). Here, the structure of the UI is expressed by the sequence of function calls in the code, not some data structure in the library. Events / user interactions are also handled directly via inline code, rather than a callback that is called by the event handler in the retained mode example.

Both immediate and retained mode libraries have their advantages, and there are multiple libraries for building both style's of UI (examples include [QT][qt] for a retained and [Dear ImGui][dearimgui] for immediate). For simpler / smaller interfaces, immediate mode libraries are often more straightforward, so that's what we'll implement.

# Overview

Our library's public interface will be divided into two major parts:

1. Layout calls, controlling where widgets end up relative to each other.
2. Widget calls, which draw individual widgets and handle their input from the user.

## Layout

Our layout system will be very simple - a screen will be made up of rows, where each row can contain a predetermined number of widgets. Any widget call will draw itself into the next available slot in the current row. As an example:

```py
ui_start(ui)
ui_row(ui, 2)
ui_label(ui, "one")
ui_label(ui, "two")
ui_row(ui, 1)
ui_button(ui, "click me")
```

This will draw a UI with two rows; the first will have two equally sized labels, while the second will be completely filled with a button. We'll need to maintain a little state (where the next available row and slot are), which will be stored in the `ui` structure.

## Widgets

There will be one function per widget and the function will request the next available slot/row, then draw the widget at that location. If the widget is interactive it will draw the appropriate state change (different colour for an active button for example), and return some indication to the caller of any user interaction (such as a boolean for button press). For more complex stateful widgets, additional state will be passed into the widget function:

```c
int slider_position = 42;

void scene_tick() {
    ui_start(ui);
    ui_row(ui, 1);
    ui_slider(ui, "volume", &slider_position, max=0, min=100);
    ui_end(ui);
}
```

In this case the `slider_position` will both be used to draw the current slider position, as well as be updated in the case the user adjusts the slider.

# Implementation

To start, our library will be tightly coupled to the blitting code (`bitmap.c`) and there won't be a lot of customisability (widget shapes, customised sprites for each widget, etc) to simplify implementation.

## Widget types

We'll start with a few basic widgets:

- Label: a piece of text.
- Button: label, but interactive - returns a boolean if clicked.
- Sprite: a Bitmap rendered into the widget slot.
- Slider: an interactive adjustment for an integer variable - passed a state value which is updated when the user interacts (A/B buttons) with the widget.
- Checkbox: an interactive true/false control - passed a state value which is updated when interacted with.

All widgets will have a simple horizontal alignment option to control if they render aligned to the left, centre, or right of their containing layout slot.

## Cursor

With mouse input, it's relatively easy to tell if a widget has been interacted with ("was the last mouse click in the bounds of the widget?"). With a d-pad, things get a little more complex as we need to keep track of a cursor position in the UI. This is one place where a retained mode system would be easier, as the library would have a full internal model of the UI structure. In our case we'll keep a count of how many widgets are added in x and y (slot and row) directions, as well as an x/y cursor position. Cursor position will then be updated whenever a direction button is pressed and then clamped to the widget count bounds.

Widgets will have a concept of whether they're interactive, which will allow the cursor system to skip over them if they're not interactive.

This means we won't be able to define more complex focus/cursor behaviours like you could in a retained mode library, but should be fine for simple UI structures.

[syscode]: https://bitbucket.org/rng/stm32f7-game/src/ce5dcb828bff52701341cdd3778680ce91718e5b/src/system/core.cc#lines-135
[immediate]: https://en.wikipedia.org/wiki/Immediate_mode_GUI
[retained]: https://en.wikipedia.org/wiki/Retained_mode
[qt]: https://en.wikipedia.org/wiki/Qt_(software)
[dearimgui]: https://github.com/ocornut/imgui
