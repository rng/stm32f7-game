# Summary

Outline a system for managing in-game objects.

# Background

All games will have in-game entities or objects - player characters, enemies,
projectiles, etc. Creation, destruction and updating state of entities is a
large part of the logic that makes a game interesting. Entity systems can be
quite complex (for example an [Entity Component Systems][ecs]), for our engine
we'll start very simple.

# Overview

Our entity system will support adding and removing entities and calling a
`tick` function on all the entities. The `tick` function will update the
entities state and draw them to the screen. The core of the entity system will
involve a list of entity objects, where each entity has a "type" that defines
it's behaviour.

The entity types behave roughly like classes in an Object Oriented system. For
example, an entities type could be defined as follows (in Python):

```python
class BallEntity(object):
    def tick(self, input):
        self.position += self.velocity * input.dt
```

And then to use that entity:

```
entities = []
entities.append(BallEntity())

for entity in entities:
    entity.tick(input)
```

The primary feature we want here is the [polymorphic][polymorphic] behaviour of
the entity type's `tick` functions. That is we multiple different entity types
with different tick function implementation.

# Implementation

A polymorphic interfaces in C is simple, if clunky - we just expose a function
pointer per entity type, where each entity type exposes a different function
via that function pointer.

Implementing the example from above in C could look something like this:

```c

typedef struct {
    void (*tick)(void *self, input_state_t *input);
} entity_type_t;

typedef struct {
    entity_type_t *type;
    v2f pos, velocity;
} ball_t;

void ball_tick(ball_t *self, input_state_t *input) {
    ...
}

ball_t ball_new() {
    ball_t *ball = alloc(sizeof(ball_t));
    ball.tick = ball_tick;
    return ball;
}
```

The `ball_t` structure is the "instance" state - each ball entity created by
`ball_new` allocates a new `ball_t` structure. That structure contains both the
per-entity state as well as a pointer to the entity-type-specific behaviour
(the tick function).

If all additional entity types define their state structures in a similar
fashion (with the type pointer as the first element), then we can iterate
through all entities and call their ticks functions as follows:

```c
typedef struct {
    entity_type_t *type;
} entity_t;

// psuedocode iterator
for(entity_t *entity in entities) {
    entity->type.tick(entity, input);
}
```

This glosses over the details of how we iterate through our entities however.
As an initial simple implementation, we'll just have entities in a linked list.

Our API for the entity system will be as follows:

```c
// Iterate through all entities calling their respective `tick` functions.
void entitymgr_tick(entitymgr_t *em, input_state_t *input);

// Add an entity `ent` of type `typ` to the entity manager.
void entitymgr_add(entitymgr_t *em, entity_t *ent, const entity_type_t *typ);

// Remove an entity `ent` from the entity manager.
void entitymgr_remove(entitymgr_t *em, entity_t *ent);
```

# Concerns / Open Questions

We lose a bunch of type safety with the polymorphic tick function pointers (we
trust that all entity structures have the correct layout). This, combined with
lack of operator overloading for vector types may be a good enough argument for
switching to a minimal C++ subset.


[ecs]: https://en.wikipedia.org/wiki/Entity_component_system
[polymorphic]: https://en.wikipedia.org/wiki/Polymorphism_(computer_science)
