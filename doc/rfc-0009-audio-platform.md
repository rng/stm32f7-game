# Summary

This RFC covers the design the audio interface between platform and game code,
along with the platform audio drivers.

# Background

Modern gaming hardware (PCs & consoles) have a fundamentally different audio
hardware interface compared to Gameboy-era consoles. Almost all game audio
hardware today expose a way to send multiple channels of 
[Pulse-code modulated(PCM)][pcm] audio data to the audio hardware which then
mixes the different channels (eg background music and in-game sounds) and
generates analog waveforms (using a [digital to analog converter (DAC)][dac])
fed to the speakers.

Storing and transferring the digital data for this sort of audio hardware is
significant when for older/smaller gaming hardware however. A 1 minute music
clip at [CD-audio][cdaudio] level quality (stereo, 44100 samples/second, 16
bits/sample) is approximately 10MB of data.

    2 (channels) * 44100 (samples/s) * 2 (bytes/sample) * 60 (seconds) = 10.1MB

Our console will have significant RAM (320KB) compared to a Gameboy, but still
is a tiny amount of space compared to a short audio clip. While we can
significantly compress audio (with formats like [MP3][mp3] and 
[OGG Vorbis][ogg]), decompressors again are out of reach for our console's CPU
(unlike modern PCs which can dedicate a CPU/thread to audio decoding).

Rather than expose a raw PCM interface to games, most Gameboy-era hardware
instead implements an audio [synthesiser][synth], typically using a
[custom sound chip][psg]. The sound chip will typically be able to generate a
number of tones using simple waveforms (sine/triangle/noise), shaping the
waveform into a sound that approximates the timbre of various instruments. The
game code will configure the tone generators then trigger them in sequence to
build up a tune / sound effect.

These synthesisers allow storing the audio data more parametrically - rather
than pre-mixed raw PCM data, you can instead store notes to be played back,
along with configuration of the various "instruments" (waveform generators). A
1 minute music clip for a synthesiser would be orders of magnitude (KB rather
than MB) smaller than a PCM recording of the same music.

# Overview

No modern microcontroller has synthesiser hardware - they're too special
purpose. However microcontrollers are now fast enough to process PCM audio in
real time.

The STM32F7 includes an [I2S][i2s] peripheral block. I2S is used to transfer
PCM audio data between chips. In this case the STM32 would send audio data to a
[Codec][codec] chip, which would translate the I2S PCM data-stream into an
analog waveform (using a builtin DAC). The codec can also be used to digitise a
microphone and send I2S data back to the STM32. While the STM can stream PCM
audio data out over I2S we still have the problem of storing audio data. Given
the 320K of RAM (and assuming no additional external RAM), our options are:

1. Stream audio data from SD. A small buffer in RAM holds the next chunk of
   audio data which is [DMA][dma]'d out over I2S.
2. Build a synthesiser in software. The higher level API looks like a hardware
   synth - define instruments, then trigger them to play notes. The synth 
   generates PCM audio into a small buffer every frame which is DMA'd out over
   I2S.

The important point here is that both options share a common lower-level API -
a small (~1 frame worth of PCM data) is kept in RAM and DMA'd out via I2S. At
this level it doesn't matter if the high level code is filling that buffer from
streamed SD audio data, or from a software synth.

# Implementation

For our audio platform interface we'll keep things simple, so that we can
support either high-level audio approach described above. We'll update our game
API struct with an `audio` call. The platform layer will call this every time
it needs new audio, passing in the game state, along with a buffer to fill.

    typedef struct {
        ...
        void (*audio)(game_state_t *game, void *audio_buf, size_t audio_len);
    } game_api_t;

Additionally we'll add a `sample_rate` parameter to our `platform_params`
struct. The sample rate will specific how many samples per second the platform
audio hardware is expecting from the game. This is needed for both streaming
existing audio data and synthesising new audio. In the first case, existing
audio will need to be [resampled][resample] to the desired sample rate (if we
don't, an 8KHz wave file played at 16KHz sounds twice as fast as expected). In
the second case, the sample rate is used by the synthesiser to calculate
waveforms (again to get the pitch correct).

## Asynchronous audio

An import detail with the new `audio` call is that it may be called
asynchronously to our game `tick` function. Especially on the PC platform where
many other processes are running, we can't provide a guarantee that each `tick`
call will be on exact frame boundaries. This means (assuming fixed audio update
rate), the audio update and game tick may overlap. 

As an example, the diagram below represents a 60Hz game tick, with a audio
update filling a 256 sample buffer for 16Khz audio:

    (256 samples * 1/16000 samples/s) = 0.016s = 16ms:

            16.66ms                19.10ms                17.15ms
    |<-- frame 1 tick -->|<---- frame 2 tick ---->|<-- frame 3 tick -->|

           16.0ms            16.0ms            16.0ms            16.0ms
    |<---- audio ---->|<---- audio ---->|<---- audio ---->|<---- audio ---->|

This setup matches how both our platform layers request audio (covered in the
next sections). On PC, SDL has an audio callback that's called from a separate
thread to buffer more audio data. On STM32, the I2S DMA interrupt fires
(asynchronously) when the DMA buffer needs to be refilled. The asynchronous
audio call means access to the audio state in shared game state struct will
need to be protected (with a mutex or equivalent).

If we called `audio` synchronously with `tick` we'd need to predict how much
audio we'd need to buffer (accounting for some potential variance in game tick
time). While this is certainly doable, it adds complexity to the platform code.
This may very well be the right answer longer term to simplify game code, but
for now this matches the platform behaviour and simplifies platform bring-up.

## PC Implementation

The PC platform audio implementation is simple. SDL requires a callback that
fills a buffer with audio samples - exactly what our `audio` call does. So we
simply wrap the game `audio` call in the callback type expected by SDL:

    void audio_callback(void *userdata, uint8_t *stream, int len) {
        game_audio(game_state, stream, len);
    }

We then configure SDL's [audio subsystem][sdlaudio] to use our callback. We
also request a specific system audio configuration - in our case 16KHz, 16bit
signed samples, stereo, with a buffer of 256 samples. Finally we un-pause the
audio system will start calling our `audio_callback`:

    void main() {
        SDL_AudioSpec desired_spec;
        desiredSpec.freq = 16000;
        desiredSpec.format = AUDIO_S16SYS;
        desiredSpec.channels = 2;
        desiredSpec.samples = 256;
        desiredSpec.callback = audio_callback;

        SDL_OpenAudio(&desired_spec, ...);
        SDL_PauseAudio(0);
    }

## STM Implementation

The STM platform audio implementation is a little more complex, but is similar
in structure to the PC. First we'll take a brief detour through how the DMA
system works on the STM.

The STM DMA hardware has multiple channels. Each channel can - without software
involvement - copy data between memory and a memory/peripheral (and visa
versa). So in our case, we say "DMA please copy N bytes from location X in
memory to the I2S peripheral". After the DMA setup and start call, the CPU is
free to do other things while the copy happens in the background. However if we
want to continuously stream more than a single buffer worth of data, we need a
way to keep feeding new data to the DMA system.

The DMA system has two features to assist with the continuous use case. First
each channel can be configured to treat the memory to copy from as a circular
buffer. That is when the DMA system reaches the end of the buffer to copy, it
automatically restarts copying from the first memory location. This is great if
you want to keep looping a set of samples to play, but not if you want to
stream multiple buffers worth of samples.

The second feature is the pair of transfer complete interrupts. The DMA system
fires an interrupt when it has transferred half of the buffer, as well as when
it has finished transferring the full buffer. Combined with the circular buffer
mode, we can use the interrupts to keep refilling the buffer "ahead" of the
ongoing DMA transfer:

            DMA pointer
                 |
                 v
      +------------------------+------------------------+
      |                        |                        |   DMA buffer
      +------------------------+------------------------+
      0                       127                      255
                               |                        |
                               v                        v
                         half interrupt           full interrupt

Taking the diagram above, the steps for continuous audio streaming would be as
follows:

1. Fill the whole buffer, start the DMA transfer
2. DMA starts transferring samples from 0 of the DMA buffer
3. DMA transfers the 127th sample and fires the half interrupt
4. Our code starts to refill the _first half_ of the DMA buffer (0-128). DMA
   has already transferred those bytes so we can overwrite them. This refill
   happens as the DMA engine is continuing to transfer the second half of the
   buffer. So we have "half a buffer's worth of transfer time" to refill.
5. DMA finishes transferring the last sample and fires the full interrupt. By
   now we have refilled the first half of the buffer with the next set of
   samples, so the DMA engine loops back and starts transferring the new data
   from 0. Meanwhile we can start refilling the _second half_ of the buffer.

This circular ping-pong style of DMA buffer (which has similarities to
[double buffering][dblbuf] in graphics) allow us to constantly refill the
buffer as the DMA system is copying for us. With this process we can now build
the audio transfer system, starting with the transfer interrupts:

    // DMA buffer
    int16_t audiobuf[AUDIO_SAMPLES];
 
    void BSP_AUDIO_OUT_HalfTransfer_CallBack(void) {
        // half interrupt - refill the first half of the buffer
        game_audio(game_state, audiobuf, AUDIO_SAMPLES / 2);
    }

    void BSP_AUDIO_OUT_TransferComplete_CallBack(void) {
        // full interrupt - refill the second half of the buffer
        game_audio(game_state, audiobuf + AUDIO_SAMPLES / 2, AUDIO_SAMPLES / 2);
    }

With those in place, the remaining audio parts are handled by the STM BSP. We
initialise the audio system with the desired sample rate (which configures the
DMA system and codec chip). The we start playing audio passing in the DMA
buffer (which starts the DMA transfer).

    void main() {
        BSP_AUDIO_OUT_Init(..., 16000);
        // fill the full buffer for the first call
        game_audio(game_state, audiobuf, AUDIO_SAMPLES);
        BSP_AUDIO_OUT_Play(audiobuf, sizeof(audiobuf));
    }

# Concerns / Open Questions

As mentioned above, the platform audio API could be refactored to provide a
simpler synchronous interface to the game code. I'm still uncertain of the
value of doing so as we can also provide a simpler audio API at a higher level
(above the platform API in the engine). The engine audio system can abstract
away the details of the asynchronous audio callback and handle locking
internally.

[pcm]: https://en.wikipedia.org/wiki/Pulse-code_modulation
[dac]: https://en.wikipedia.org/wiki/Digital-to-analog_converter
[cdaudio]: https://en.wikipedia.org/wiki/Compact_Disc_Digital_Audio
[mp3]: https://en.wikipedia.org/wiki/MP3
[ogg]: https://en.wikipedia.org/wiki/Vorbis
[synth]: https://en.wikipedia.org/wiki/Frequency_modulation_synthesis
[psg]: https://en.wikipedia.org/wiki/Programmable_sound_generator
[i2s]: https://en.wikipedia.org/wiki/I%C2%B2S
[codec]: https://en.wikipedia.org/wiki/Audio_codec
[dma]: https://en.wikipedia.org/wiki/Direct_memory_access
[resample]: https://en.wikipedia.org/wiki/Sample-rate_conversion
[sdlaudio]: https://wiki.libsdl.org/SDL_AudioSpec
[dblbuf]: https://en.wikipedia.org/wiki/Multiple_buffering#Double_buffering_in_computer_graphics
