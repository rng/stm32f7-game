# Summary

A simple in-game timing profiler using the debug timing infrastructure described in [RFC-0011][rfc11].

# Background

The debug timing system in [RFC-0011][rfc11] provides averaged timing of a given chunk of code, output via the debug console. This is useful for measuring a piece of critical code during optimisation, but doesn't give us a good high level overview of where our game is spending time. Additionally, it can't provide information on the changes in timing as the game state changes - for example, if the AI code takes 1ms in one part of a level, but 100ms in another.

To provide us these details, we'll implement a graph-based [profile][profile] view of the time spent in the game. This graph will be a stacked bar graph rendered as an overlay over the game screen. Each column in the graph represents a frame in the game, with each stacked chunk representing a portion of the game code (rendering, input handling, AI, etc).

Since we have a fixed amount of time per frame (33ms at 30 frames/second), we can set the max Y value for the graph at that time. The bar stack, will then show at a glance what percentage of time is going to each part of our game code, as well as how much of our frame time we're consuming.

An attempt at representing the graph in ASCII below:

```
frame time (ms)
^
|-  -  -  -  - r-  -  -  -  33ms (30FPS) limit
|r  r     r    r   r
|rrrr     rrr rr  rrr   r
|rrrarr  rrrrrrrrrarrrr rr
|arraaarrrrrarrrraarrrrrar
|aaaaaaaaaaaaaaaaaaaaaaaaa
|iiiaaiiiiiaaiiiiiaaaiiiii
+--------------------------> frame (older to newer)

(i = input)
(a = ai/game logic)
(r = rendering)
```

# Overview

Our profiler will be broken up into two parts:

1. Collating timing block information from the engine into a per frame block of debug info.
2. Rendering current and historical frame blocks.

```
                                        frame info
              debug                    (all timings                  
.---------.   events   .---------.    for this frame)   .---------.    
|game code| ---------> |collation| -------------------> |rendering|
'---------'            '---------'                      '---------'
```

For now, we'll focus on two types of debug events: high level code timings, and memory usage. So for each frame we should be able to answer roughly "where did our code spend its time", and "how much memory did it consume". This can then be used to drive optimisation.

# Implementation

## Event collation

We'll add one call to our platform API:

```c
void (*debug_log_timed_block)(const char *name, uint64_t start, uint32_t delta);
```

This call will be used by our `DEBUG_TIMED_BLOCK()` macro to log a named timing block to the platform layer (with associated start and delta time in microseconds). The rest of the logic will be entirely platform side.

The collation system will keep a list of debug events that occurred in the current frame (where an event is a timed block, or memory usage sample):

```c
enum debug_event_type_t {
    DEBUG_EVENT_BLOCK,
    DEBUG_EVENT_MEM,
};

struct debug_event_t {
    debug_event_type_t type;      // event type
    union {

        // timed block info
        struct {
            uint64_t t_start;     // block start in us
            uint32_t dt;          // block time in us
            char name[16];        // block name
        } block;

        // memory allocation info
        struct {
            uint32_t delta_alloc; // number of bytes allocated
            uint32_t delta_free;  // number of bytes freed
            uint32_t cur;         // total current bytes allocated
        } mem;
    };
};
```

A debug frame is just a collection of these events:

```c
struct debug_frame_t {
    debug_event_t events[MAX_DEBUG_EVENTS];
    int event_count;
};
```

The collation system will glue the platform API (`debug_add_timed_block`) callback and the memory allocator into this event list. As a result, at the end of each frame we will have a list of events (assuming our engine code is properly instrumented) showing where we spent our CPU time and memory.

## Profile graph rendering

At the end of each frame, the debug frame info can be passed to the graph rendering system. The renderer will need to keep track of historic data (in the form of the last N frames of event data). This will be stored in the form of a [FIFO][fifo] queue with a size of however far we want our graph to go back. Initially, we'll render one frame per pixel, so on a 128px wide screen we can render 128 frames (or roughly 4 seconds of historical data at 30FPS).

The graph renderer will take a `debug_frame_t` and iterate through all the events building a grouped "stack" of timings. That is if our events are as follows (2 calls into AI code, 1 into rendering and 1 into input):

```
ai (1ms), ai (2ms), render (4ms), input (2ms)
```

The resulting stack would look like this:

```
ai:     3
input:  2
render: 4
```

We'll sort the resulting list by timing name to ensure consistent stackup across frames. The stack can then be saved into a queue (popping the last element once we reach the defined queue size), and rendered by iterating through the whole queue:

```
for x, stack in enumerate(frame_queue):
    ypos = 0
    for timing in stack:
        draw_vertical_bar(x=x, y1=ypos, y2=ypos + timing)
        ypos += timing
```

# Alternatives

Rendering our profile graph to a tiny console screen means we can't show much information. We could instead pipe the profile information over a debug channel to a host PC. This would allow a much more detailed debug view, at the cost of requiring a tethered machine.

# Concerns / Open Questions

The debug system will consume additional memory / performance and as such won't let the game run at the absolute limit of the hardware. This is likely an acceptable compromise however, and if a game really does reach the limits of the hardware, less invasive debug tools could be implemented.

[rfc11]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/rfc-0011-debug-logs-timing.md
[profile]: https://en.wikipedia.org/wiki/Profiling_(computer_programming)
[fifo]: https://en.wikipedia.org/wiki/FIFO_(computing_and_electronics)
