# Summary

Proposes switching the default framebuffer and image format to 8-bit paletted.

# Background

Our initial asset pipeline defined in [RFC-0005][rfc5] uses 16bit RGB565 images
to match the initial framebuffer format we set up in [RFC-0002][rfc2]. While
RGB bitmap formats are convenient, they consume more memory than the
alternative: paletted images.

## Palettes

In an RGB image format, each pixel value contains the red, green and blue
values for that pixel. In our case we have 2 bytes per pixel, with 5 bits of
red and blue data and 6 bits of green data. Paletted images on the other hand
store a palette index for each pixel. This index doesn't tell us anything about
the specific colour, but rather indexes into a palette which has the RGB values
specified:

                     palette
      image          .-----.
    .-------.        |R G B|
    |0 1 1 2|       0|0 0 0|
    |   `---------> 1|1 0 1|
    |1 2 2 1|   .-> 2|1 1 1|
    '--|----'   |    '-----'
        `-------'

In the above example, we have a 4x2 pixel image. Each pixel value is an index
into the palette which contains 3 colours. On rendering the image to the
display the hardware (or rendering code if there was no hardware support),
would take each pixel value, index into the palette to find the RGB values,
then use those to render the pixel to the screen.

The palette adds additional overhead to rendering each pixel (palette lookup),
but also provides a significant win in image size. In the above example, each
pixel value could be represented by 2 bits, meaning we could pack 4 pixels into
a byte (8x more compact than the equivalent RGB565 image). This is in fact what
the [NES][nes] used for its sprites.

The LCD subsystem of the STM32 supports paletted images in hardware, which
means switching from RGB565 doesn't incur additional rendering cost. Not using
an RGB-based image format does mean our games will be restricted from doing
more interesting pixel-colour manipulations. For example, with a palette it
becomes much harder to say "lighten this pixel by X%", or "tint this sprite
red". Palette-based rendering is much more common with the type of hardware
we're aiming to product (all of the [16-bit era consoles][16bit] were
palette-based). 

# Overview

Supporting palettes doesn't require significant new code, though we will need
changes through much of our system so far:

- Platform layer support for paletted framebuffers (both SDL, and the STM
  hardware provide palette support however)
- support for blitting 8 bit bitmaps, including some method for transparency in
  sprites (see below)
- Support for converting RGB source images into paletted asset files
- Support for converting palette definitions into asset files and loading those
  palettes in game

During asset creation, we'll use [GIMP Palettes][pal]. These are a very
straightforward ASCII palette format that could be converted to whatever the
developer prefers in image editors. We'll provide a default system palette
hard-coded in the platform layer.

For image transparency, we no longer have the concept of an alpha channel in
our image formats (eg our RGB5551 images which had a single bit of
transparency). Instead we'll use a "colour key" for transparency. A colour key
is simply a special palette index which our rendering system treats as
transparent. Whenever the blitter sees a pixel of that value, it simply skips
copying it to the destination buffer.

# Implementation

We'll switch our engine and platform to using 8 bit palettes by default. This
doesn't preclude us from adding back the option of RGB565-based bitmaps in the
future, but doing so will require additional mode-setting platform APIs.

## Asset Pipeline

The asset converter will now take a palette file for any image conversion.
It'll still read RGB png images, but will take each RGB pixel value and search
the palette for the nearest match. Images should be edited using the palette
the developer will use in game to avoid incorrect colour rendering in game.

The converter will also take transparent pixels and output a colour key of 255
- the value we'll use to indicate transparent pixels.

## Platform Layers

SDL provides a [palette][sdlpal] which can be linked to 8 bit image formats and
handles the rendering and palette lookup for us. The STM32 LCD hardware also
supports palettes in a similar fashion so we'll need to call a STM BSP function
to set our system palette.

## Blitter

The blitter will (for the time being), assume only 8 bit paletted source and
destination bitmaps (for code simplicity). Blitting will simply involve copying
bytes from source to destination, except where the source byte is a 255 (our
colour key).

# Concerns / Open Questions

This is a reasonably significant design choice that comes with a couple compromises:

- By depending on the platform hardware palette, we can't easily do fancy systems
  like per-sprite palettes (as the early consoles did). We could implement these
  in software, but that comes at a performance cost.
- Palettes do impose additional complexity on the asset pipeline (the developer
  needs to specifically configure their image editor to use the palette), but
  this feels like an acceptable burden given the target hardware and resource
  saving benefits.
- Initially we won't provide a palette-setting platform API and rely on the
  default system palette, but the setting API should be very simple to add.


[rfc2]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/rfc-0002-hardware-architecture.md
[rfc5]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/rfc-0005-asset-pipeline.md
[nes]: http://nesdev.com/NinTech.txt
[16bit]: https://en.wikipedia.org/wiki/Fourth_generation_of_video_game_consoles
[pal]: https://docs.gimp.org/2.10/en/gimp-concepts-palettes.html
[sdlpal]: https://wiki.libsdl.org/SDL_Palette
