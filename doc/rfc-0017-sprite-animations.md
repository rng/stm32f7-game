# Summary

Outline a minimal system for animating sprites.

# Overview

Our engine and game currently use static sprites for all our in game entities
(and we only have one sprite at that). If we want a game more interesting than
a collection of red bouncing balls we'll both need to add additional sprites,
and a way to animate those sprites.

Animated sprites are little more than rapidly drawing a sequence of different
images. Since our bitmap format is capable of storing multiple frames/images
(see [RFC-0007][rfc7] for the details), we already have much of the
infrastructure to build animations. The missing components are:

- A way of defining which frames (and in what order) to play back, and the
  delay between each frame.
- A system to drive the animations, using the definitions above.

# Implementation

Lets start by defining a sequence of sprite frame indices:

```c
int animation[] = {1, 5, 6};
```

With this we could play an animation using:

```c
int framedelay_ms = 100;
for (int i=0; i<3; i++) {
    int frame = animation[i];
    draw_bitmap_frame(sprite_id, x, y, frame);
    delay_ms(framedelay_ms);
}
```

The issue with the above code is that we want to draw at least 30 frames per
second to get smooth gameplay. If a sprite is calling a delay function for
100ms, it'll lock up (our single-threaded) game for 3 frames (30 FPS = 33ms /
frame).

Rather than delay between frames, we'll keep track of time elapsed in an
animation, updating it each frame and using the time to determine which frame
to display:

```c
float anim_elapsed;
int anim_idx;

void game_tick(..., float dt) {
    // update the elapsed time baesd on game tick delta time
    anim_elapsed += dt;
    // has 0.1s (100ms) elapsed?
    if (anim_elapsed > 0.1) {
        anim_idx++;
        // reset elapsed
        anim_elapsed -= 0.1;
    }
    int frame = animation[anim_idx];
    draw_bitmap_frame(sprite_id, x, y, frame);
}
```

This new code, while a little more complex, never blocks the game tick. For
each animation, all we need to store is how much time has elapsed, and where we
are currently in the animation. We should also store the expected required
delay between each frame.

```c
typedef struct {
    int *frames;     // pointer to a list of frame numbers
    float elapsed;   // elapsed time for current frame
    int frame_idx;   // current frame index
    float frametime; // time to display each frame
} anim_t;
```

We can then rewrite the code above into a more generic animation helper:

```c
// call with frame delta time, returns current frame to draw
int anim_update(anim_t *anim, float dt) {
    anim->elapsed += dt;
    if (anim->elapsed > anim->frametime) {
        anim->frame_idx++;
        anim->elapsed -= anim->frametime;
    }
    return anim->frames[frame_idx]
}
```

Our usage would then look something like:

```c
int animation[] = {1, 5, 6};
anim_t anim = {
    .frames = animation,
    .frametime = 0.1
};

void game_tick(..., float dt) {
    int frame = anim_update(&anim, dt);
    draw_bitmap_frame(sprite_id, x, y, frame);
}
```

# Concerns / Open Questions

There are a couple complexities we didn't cover in the code above. First is
what to do when we finish the last frame. For some animations we'll want to
loop (restart at frame index 0), but for others we'll want to just stop on the
last frame. As such we'll need to add a frame count value to our `anim_t`
structure, as well as a boolean controlling whether to loop or not.

Additionally, many entities in our game will likely need a range of different
animations. For example our player character will need at least a walk
animation, along with a jump animation. To handle this we'll want our frame
array to include multiple sets of sequences, with some way of triggering a
certain animation.

[rfc7]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/rfc-0007-fonts.md
