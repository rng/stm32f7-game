# Summary

Outlines basic debug infrastructure for the platform layer; the ability to
print messages to a debug console, and measure high resolution time intervals.

# Background

While we have the ability to interactively debug our game code with GDB
(remotely via [JTAG][jtag] in the case of the STM platform), simple
[print debugging][printdebug] is still a powerful tool.

Libc's `printf` is a natural answer to debug prints as it is standard and
available on all platforms. However we will likely want significantly different
behaviour for our two platform's debug logging. On PC it'll be fine to do a
normal printf to the console (maybe eventually to a log file). On STM we don't
have a standard console, so we will either want to somehow route logs to a
connected PC, or to a log file. As such we'll expose a debug printf method in
our platform API that each platform can implement appropriately.

In addition to debug logs, timing of portions of code becomes increasingly
valuable as we make the engine and game more complex. Even the simplest
measurement - how long a single game frame takes to process and render - tells
us how fast (in frames per second) we can run the game. To support this we'll
expose a high-resolution timer (in microseconds) via the platform API to let
engine and game code measure performance.

# Overview

Let's start with the new `platform_api_t` methods:

```c
// Return high resolution (usec)
uint64_t (*debug_get_usec)(void);

// log a formatted string to the platform's debug output console
void (*debug_printf)(const char *fmt, ...);
```

These two methods form the core additions for our initial debug infrastructure.
Note that the `debug_get_usec` call isn't intended to provide "wall-clock"
time. That is, it doesn't represent time since epoch like [Unix time][unixtime]
and can't tell you current calendar date or time. Instead two calls two
`debug_get_usec` are intended to give you an accurate measure of time elapsed.
`debug_printf` is a [printf format string][printfformat]-based call that is
backed by whatever log output system the platform chooses.

## High-level Debug API

On top of these two low-level platform APIs we'll build high-level debug
features. We'll start simple with the following:

```c
// Print a formatted debug message to the platform debug console
DEBUG_LOG(platform, ...)

// Create a timer block for the enclosing lexical scope
// (eg time between { and }). Store elapsed time in `timing` structure
DEBUG_TIMED_BLOCK(platform, timing)
```

These two calls will be macros that can be disabled with a global flag (eg.
`#define DEBUG 0`), allowing for simple debug and release builds of the game.

`DEBUG_LOG` will be wrapper around straightforward `platform->debug_printf`,
while `DEBUG_TIMED_BLOCK` will be a little more involved. `DEBUG_TIMED_BLOCK`
will measure the time spent in the [lexical block][block] in which it is
defined. As an example:
    
```c
debug_timing_t ai_time;

void do_complex_game_ai() {
    DEBUG_TIMED_BLOCK(platform, &ai_time);
    // complex AI stuff, call other functions, reticulate splines, etc.
    // ...
}
```

The `DEBUG_TIMED_BLOCK` will measure the time spent between the opening `{` of
`do_complex_game_ai` and the closing `}`, including time spent in other nested
function calls. It will update the `ai_time` structure with min, max, and
average time spent, over multiple calls to `do_complex_game_ai`. This will
allow us to roughly profile time spent in core game and engine code.

# Implementation

## Platform Layer

The PC implementation of the debug platform API is straightforward:
`debug_get_usec` will be backed by `clock_gettime` and `debug_printf` will be a
`printf` wrapper.

The STM implementation is more interesting. We will use the ARM Cortex
[Data Watch and Trace Unit (DWT)][dwt] to count number of clock cycles. Since
we know how fast the CPU core is clocked (based on external crystal oscillator
and internal clock config), we can calculate elapsed time in microseconds
exactly. The DWT requires a little bit of setup (writing the correct values to
registers), but after that a single register read will give you the elapsed
clock cycles.

For `debug_printf` we first need to decide _where_ we're going to send the
debug logs. Since we haven't started any work on USB connectivity the options
are:

- The LCD display
- A log file on SD
- A UART (serial port)
- The connected JTAG debugger

The LCD is a bit limited at 160x120 as it would quickly fill up with text even
with a tiny font. A log file on SD would work, but loses the liveness that
makes print debugging valuable. A UART simple from the STM-side, but requires
additional hardware (USB-to-serial converter) connected between the board and
the PC. That leaves the JTAG debugger. Debug prints via JTAG require a little
background context, so lets take a brief detour into ARM semihosting.

## Semihosting

A typical embedded debug setup for an ARM processor looks something like this:

                                           |------ Host PC ------|
             JTAG/SWD                  USB
    .-----.            .-------------.     .---------.     .-----.
    | MCU | <--------> | Debug Probe | <-> | OpenOCD | <-> | GDB |
    '-----'            '-------------'     '---------'     '-----'

A Debug Probe ([JTAG Adapter][adapter]) connects to the Microcontroller
microconcontroller's debug port. [OpenOCD][openocd] (or an equivalent piece of
software) knows how to speak to the debug probe (over USB or Ethernet) and
presents a standard interface which a debugger (GDB in our case) can connect
to. So for example, when you inspect a variable in the debugger (eg `p var`),
gdb sends a memory read command to OpenOCD. OpenOCD translates that into
commands the debug probe understands, which in turn sends the necessary JTAG
commands to the microcontroller to read out the chunk of target memory.  The
data is returned all the way back up the chain, and gdb pretty prints it.

[Semihosting][semihosting] is a process by which a special sequence of
instructions executed on the target triggers some behaviour on the host PC (in
OpenOCD). ARM defines a range of semihosting commands such as reading and
writing from the host's stdin/stdout, reading and writing to other files,
getting the current time, etc. In the Cortex-M series to trigger a semihosting
command you populate `r0` with a semihosting command, `r1` with a parameter,
and then execute the breakpoint instruction with a parameter of `0xab`:

    mov  r0, #5 # write string command
    mov  r1, <address of char write buffer>
    bkpt 0xab

After executing the breakpoint instruction, OpenOCD will (via the debug probe)
catch the breakpoint, read the command and parameter memory, then print the
string stored in the location pointed to by the parameter.

This all sounds rather involved, but gives us access to a simple live debug
console, with no additional hardware connected to the target. We'll use
semihosting to back our initial STM `debug_printf` platform call, though we may
eventually replace it with a USB-based console.

## Timed Block details

`DEBUG_TIMED_BLOCK` can be broken down into three steps:

1. On creating the timed block we store the current time in microseconds
2. When we leave the scope of code block, get the current time and calculate
   the delta since the start time.
3. Update a timing structure with the new delta time.

The straightforward implementation of this would look like:

```c
{
    uint64_t t_start = platform->debug_get_usec(); // 1. measure start time
    // do stuff that takes time
    uint64_t t_end = platform->debug_get_usec();   // 2. measure end time
    uint64_t t_delta = t_end - t_start;            // 3. update timing
    timing.min = MIN(timing.min, t_delta);
    timing.max = MAX(timing.min, t_delta);
    timing.sum += t_delta;
    timing.count++;
}
```

This is fine, but makes for a bunch of additional work to add or move the range
over which a timing block measures. What we'd really like is a way to
automatically run steps 2 and 3 when we leave the block scope. This is exactly
what a [destructor][destructor] would do if the timing block was a stack
allocated object in C++. Since we're using C, we don't have objects or
destructors, but we can use a Clang/GCC extension which allows marking a
variable with ["cleanup" function][cleanup] that runs when the variable goes
out of scope. We can use this as follows:

```c
void timing_block_cleanup(uint64_t *t_start) {
    uint64_t t_end = platform->debug_get_usec();   // 2. measure end time
    uint64_t t_delta = t_end - *t_start;           // 3. update timing
    // do timing update stuff
}

{
    uint64_t t_start __attribute__((cleanup(timing_block_cleanup)));
    t_start = platform->debug_get_usec();          // 1. measure start time
    // do stuff that takes time
}
```

In this case we create a `t_start` variable as before, but mark it with a
`cleanup` attribute. Once `t_start` goes out of scope, the
`timing_block_cleanup` function gets called with a pointer to `t_start`. In
`timing_block_cleanup`, we then do all the code from step 2 and 3 as before.

We can then wrap the cleanup attribute magic in the `DEBUG_TIMED_BLOCK` macro
to make it easy to add to code. This means adding a timing to a block is a
simple matter of declaring a timing variable and dropping in a
`DEBUG_TIMED_BLOCK(platform, timing_var)` into the block we want to measure.

# Concerns / Open Questions

Semihosting is very slow (100s of bytes per second), so writing log messages
will impact game performance. We will want to eventually replace it with a USB
logging interface which will also allow using debug prints without OpenOCD.


[jtag]: https://en.wikipedia.org/wiki/JTAG
[printdebug]: https://en.wikipedia.org/wiki/Debugging#Techniques
[unixtime]: https://en.wikipedia.org/wiki/Unix_time
[printfformat]: https://en.wikipedia.org/wiki/Printf_format_string
[block]: https://en.wikipedia.org/wiki/Block_(programming)
[dwt]: http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0337h/BIIFBHIF.html
[semihosting]: https://developer.arm.com/docs/dui0471/k/what-is-semihosting/what-is-semihosting
[openocd]: http://openocd.org/doc-release/html/About.html#What-is-OpenOCD_003f
[adapter]: https://en.wikipedia.org/wiki/JTAG#Adapter_hardware
[destructor]: https://en.wikipedia.org/wiki/Destructor_(computer_programming)
[cleanup]: https://en.wikipedia.org/wiki/Resource_acquisition_is_initialization#Clang_and_GCC_%22cleanup%22_extension_for_C
