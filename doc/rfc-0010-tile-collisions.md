# Summary

Outline a system for calculating collisions between 2d rectangular objects and
tile maps.

# Background

With a tile map system in place we can start to think about collision
detection. Whether we implement a side-scrolling or top-down game on the
engine, we'll want in game objects (characters, enemies, projectiles) to
interact with the tile-based world. That is, objects should not be able to move
through solid objects, where solidity is defined on a per tile basis.

A helpful reference is Higher-order Fun's
[Guide to Implementing 2d Platformers][guide]. We'll be implementing
"Tile-based (smooth)" collision detection described in that guide.
Specifically, all our game objects will be able to move in (sub)pixel
increments, and will collide with full tiles.  Initially we won't implement any
of the more complex collision types (slopes, ladders, one-way platforms, etc).

# Overview

Let's walk through an example scenario as context. Assume we have our player
character at position (1,1). The character is moving at a rate of 2.82
pixels/tick (roughly 2 pixels/tick in both x and y). After one tick our
character should be at (3,3) (start position + velocity).

     0 1 2 3 4

     1 s
        \
     2 --x-----
          \
     3     e

Now assume there's a horizontal wall at y=2. Rather than ending up at (3,3)
we'd want our character to stop at (2,2). The obvious approach is to check if
the end position is inside an impassable object, and if so find the
intersection between the solid and our motion vector. This works fine until we
are moving fast and encounter a thin object (eg. moving at a velocity greater
than 1 tile size, and hit a 1-tile wide wall). In a single step both start and
end positions will be passable and so our naive approach will "penetrate" the
thin object and continue moving as if the wall never existed.

To avoid this problem, we need to step our object along the velocity vector in
small enough increments to always ensure we'll detect an obstacle. In our case
the smallest increment will be the tile size. The algorithm is then to
increment along one axis, check for collision, then increment along the other
axis then check for collision again. If either collides, we stop, otherwise we
continue incrementing until we've scanned through the full velocity vector.

# Implementation

One of the goals behind the collision sub-system (and future systems in the
engine), is to avoid tightly coupling it to other portions of the codebase. By
keeping the various engine systems loosely-coupled, users of the library can
easily mix and match engine components (with their own components) as needed by
their specific game.

## API structure

As such we'll try to keep hard dependencies on other portions of the engine out
of the collision checker. Our public API will be as follows:

    collide_res_t collide_tilemap_check(
                        collide_conf_t *config,
                        float x, float y,
                        float vx, float vy,
                        int w, int h);

The `collide_tilemap_check` function will check for the collision of an object
at position `x,y`, of size `w,h` moving at velocity `vx,vy` (all in pixels).
The function will check for collisions using user provided tile map
configuration in the `config` struct:

    typedef bool (*collide_pass_fn_t)(void *context, int tx, int ty);

    typedef struct {
        int tilesize;
        collide_pass_fn_t pass;
        void *context;
    } collide_conf_t;

The config struct doesn't reference our `tilemap.h` API directly which allows
the user to provide other implementations, change how passability is defined.
All our collider expects is a function `pass` that returns a boolean
passability value for a given tile coordinate.

The collider will return a struct with results of the check in pixels
(`rx,ry`), tiles (`tx,ty`) and on which axis we collided (`collx,colly`):

    typedef struct {
        float rx, ry;
        int tx, ty;
        bool collx, colly;
    } collide_res_t;

## Using the collision system

In use in the game, configuring the collider will look something like:

    // tile is passable if it's inside the bounds of the map, and is empty
    bool map_passable(void *ctx, int x, int y) {
        tilemap_t *map = ctx;
        return tilemap_inside(map, x, y) && (tilemap_tile(map, 0, x, y) == 0);
    }

    // configure collider using our map tileset / passability function
    collide_conf_t config = {
        .tilesize = map->tileset.tile_width,
        .pass = map_passable,
        .context = map,
    };

And using it is as follows (assuming an object with a position `x,y`, velocity
`vx,vy` and size `w,h`):

    // perform the collision 
    collide_res_t collide = collide_tilemap_check(&config, x, y, vx, vy, w, h);

    // update object position with result of the collision check
    x = collide.rx;
    y = collide.ry;

    // if we collided in either axis, bounce (flip velocity in that axis)
    if (collide.collx) {
        vx *= -1;
    }
    if (collide.colly) {
        vy *= -1;
    }

# Concerns / Open Questions

The current API assumes floating point position and velocities which work for
our target (as it has a [floating point coprocessor][vfp], making floating
point operations cheap. However this is not always the case, especially on
smaller microcontrollers, so it would be nice to have a way to enable the
collision system work on integer or [fixed-point][fixedpoint] object positions
/ velocities.

[guide]: http://higherorderfun.com/blog/2012/05/20/the-guide-to-implementing-2d-platformers/
[vfp]: https://en.wikipedia.org/wiki/ARM_architecture#Floating-point_(VFP)
[fixedpoint]: https://en.wikipedia.org/wiki/Fixed-point_arithmetic
