# Summary

Separate the platform and game code into separate binaries to enable multiple
game loading (a home-screen / launcher). Also discuss potential for game code
hot-loading during development.

# Background

Our game code is currently linked into the same binary as the platform code
(for each platform) as follows (using the SDL platform on windows as an
example):

          game.exe
    +------------------+
    |  src/engine/*.c  |
    |  example/game.c  |
    |        ^         |
    |--- game_api_t ---|
    |        v         |
    | src/plat_sdl/*.c |
    +------------------+

This is convenient for development and simplifies the overall architecture but
means that each game caries a copy of all the platform code with it. While this
is fine on PC platforms, it wastes (comparatively) limited flash space on the
STM platform.

We will separate the game code into a binary that the platform loads on
startup, as shown:

    game1.dll (game2.dll, etc)
    +------------------+
    |  src/engine/*.c  |
    |  example/game.c  |
    +------------------+
             ^
         game_api_t
             v
    +------------------+
    | src/plat_sdl/*.c |
    +------------------+
        platform.exe

This design is not uncommon in games - see Fabien Sanglard's [Quake 2 source
code review][q2] for an in-depth look at this style of platform/game code
split.

# Overview

Given how our game and platform code are structured currently, separating them
does not require significant refactoring. Platform code calls into game code
only via the `game_api_t` structure, which in turn provides game code the
ability to call into platform features via the `platform_api_t` structure.

The only link time dependency between game and platform code is the
`game_[init|term|tick]` functions. If we can provide a way for those to be
resolved at run time rather than at compile time, we can separate the two
portions of code. For runtime resolution we'll use [shared libraries][sharedlib]
(`.dll` / `.so` / `.dylib` files on Win / Linux / Mac). PC operating systems
provide a way to load shared libraries at runtime ([dlopen][dlopen] /
[LoadLibrary][loadlibrary]) and then access functions in the shared library
([dlsym][dlsym] / [GetProcAddress][getprocaddress]).

After implementing this RFC, the game boot process will look something like:

1. Platform code launches
2. Platform code looks for game shared library (likely in a game specific
   folder)
3. Platform code loads shared library and gets references to the game
   init/term/tick functions
4. Platform code calls into game code via references to shared library (passing
   platform API in)
5. Game code runs as before, calling back into platform code via platform API
   structure.

With this system implemented it also becomes possible to repeat steps 2-4 while
the game is running. This allows us to reload new game code while maintaining
live state (where the player is in the game, enemies onscreen, etc). This
"hot-reloading" allows for faster iteration / tweaking of game code during
development.

# Implementation

As with the platform layer, shared library implementation will be significantly
different between the PC platform and the STM platform. On PC we will use the
operating system provided functions for loading shared libraries and resolving
functions. Since we don't have an operating system on the STM platform we lack
the [dynamic loading][dynamicloading] capabilities needed to easily implement
shared libraries. Instead we will implement our own, simplified loader
specifically for game code.

## PC Dynamic Loading

Using shared libraries on PC is relatively simple. We'll use the POSIX
functions in this RFC, but equivalents exist on Windows. The core
implementation is outlined below:

    // function type exposed by the game shared library
    typedef game_api_t *(*get_game_api_fn_t)();
    // load our game shared library
    void *game_lib = dlopen("game.so", RTLD_NOW);
    // get a reference to the `get_game_api` function in the shared library
    get_game_api_t get_game_api = dlsym(game_lib, "get_game_api");

    // call the `get_game_api` function in the shared library, returning a
    // game_api structure
    game_api_t game_api = get_game_api();
    // using the returned game api, call into game code in the shared library
    game_api->init();
    game_api->tick(...);

The only function publicly exposed by our game shared library is
`get_game_api`, which would be implemented something like this in the shared
library:

    game_api_t get_game_api() {
        return (game_api_t){
            .init = game_init,
            .term = game_term,
            .tick = game_tick,
        }
    }

    void game_init(...) {
    }
    
    ...

So the `get_game_api` function returns a structure that contains function
pointers to the game init/term/tick functions inside the shared library which
we can call as before.

## STM Dynamic Loading

Bare-metal code separation is in some ways more complex, since we don't have an
operating system to load shared libraries and find functions for us. Instead we
will need to build an equivalent system from scratch. However we can apply some
simplifications due to our use case:

- We only need to load one shared lib at a time (one game's code)
- We only need the platform layer to find one function (`get_game_api`)

These two restrictions make the platform:game code relationship look a lot like
a [bootloader][bootloader] and application. Typically a bootloader is the first
piece of code to run on an embedded system and once it's done hardware
initialisation will jump to a fixed address in flash where the application is
loaded:

    +-----------+       <- start of flash (eg 0x8000000)
    |boot loader|
    |           |
    +-----------+       <- fixed start of app code (eg 0x8008000)
    |           |
    |application|
    |           |
    +-----------+

Our platform code is serving a similar role to a bootloader in that it needs to
call into some application code. However when a bootloader jumps to application
code, it typically relinquishes control and the app runs until a reboot. In our
case the platform code will call into the game code once to retrieve the
`game_api`, which in turn will provide the platform code a set of pointers to
the `game_[init|term|tick]` functions.

    +------------+      <- start of flash (eg 0x8000000)
    |  platform  | --.  calls to fixed address
    |            |   |
    +------------+ <-'  <- fixed start of game code (eg 0x8008000)
    |get_game_api| <-.  returns game_api struct
    |            |   |  with pointers to game
    | game_init  | :-+  functions
    | game_term  | :-+
    | game_tick  | :-+
    +------------+

To implement this we can mostly rely on the linker. We link the platform code
starting at the beginning of flash (0x8000000 in the STM32). This is the same
as before, and causes the STM to boot normally into the platform code. Now
instead of linking the game code along with the platform code, we link it into
a separate binary starting at a fixed (known to the platform) location in flash
(eg 0x8008000). Through a linker script we also ensure that `get_game_api` in
the game code is linked at the start of the game binary (at 0x8008000). On
startup, the platform code can jump to address 0x8008000 which we have now
ensured contains the `get_game_api` function. This function then returns the
`game_api` structure which contains pointers into game code. The platform code
can now call these function pointers as before.

An example of what this looks like in platform code is as follows:

    get_game_api_fn_t get_game_api = (get_game_api_fn_t)(0x8008000);
    game_api_t game_api = get_game_api();
    ...

And a matching linker script for the game would contain something like:

    FLASH (rx)      : ORIGIN = 0x08008000 /* game code starts at 0x8008000 */

    SECTIONS {
    .text : {                             /* .text section contains code   */
        KEEP (*(.get_game_api))           /* ensure the first function is  */
        ...                               /* get_game_api                  */
    } >FLASH

## Allocation of STM Memory Space

Since game code on STM needs to be compiled to a fixed location, it's worth
talking through what the allocation of RAM looks like in this system. Per
[RFC-0002][rfc2] we propose using RAM as the location for game code (and
assuming no external RAM) we need to fit platform globals, game globals, game
code, heap, and stack. Assumptions:

- 160x120 RGB565 framebuffer (~37K)
- Additional platform globals (20K?)
- Game data is larger than game code

We get this:

   +--------------------+ 0K
   | plat globals (64K) |
   +--------------------+ 64K
   |   game code (64K)  |
   +--------------------+ 128K
   | game globals (32K) |
   +--------------------+ 160K
   |  game heap (150K)  |
   +--------------------+ 310K
   |     stack (10K)    |
   +--------------------+ 320K

Which seems workable, and the game developer has some flexibility to adjust
sizing of the respective game regions.

## Game Bundles

Given the system described above, we should now be able to build "game
bundles". That is, directories that contain game code and data. The game loader
(platform executable) can be pointed at a game bundle and boot into the game.
The platform could in theory include "home screen"-style logic that lists
available game bundles. Game bundles could also be cross-platform by including
the shared library for each platform:

    mygame/
    ├── data/
    │   ├── map.dat
    │   └── testsprite.dat
    ├── game.dll
    └── game.stm

There should probably be some sort of game metadata (name, author, etc) in the
bundle, readable by the platform without loading the shared library, but we can
add that later.

## Hot-loading

With the shared library system in place, hot-loading game code while the game
is running is almost free. While the game is running, the game c files can be
modified and shared library rebuilt. The game can then run the shared library
loading logic between two game frames (that is between calls to
`game_api->tick`) to switch to the new game code. This makes a couple of
assumptions about game code:

- No global state (other than the `game_state_t` passed into `game_api->tick`).
  On reload of the shared library all global variables within that shared
  library will be reset. We could call `game_api->init`, but that would reset
  all game state which make it harder to use the hot-loading to iterate on game
  behaviour (like physics).
- Game state structure / types don't change across hot-loads. As an example:
  assume an object in the game is represented as a `struct { int x, y }` and
  during a hot-load we change the structure to be a `struct { int alive, x, y }`
  After the reload the code running will assume the in-memory representation of
  the object starts with the `alive` field. However the actual object was
  created by the previous iteration of the code where the first field was `x`.
  This will lead to all sorts of confusion, crashes and hilarity. Nevertheless,
  a large number of changes to game code (anything that doesn't change the
  "shape" of global state) will work fine across reloads.

Despite these restrictions, I still think a hot-loading system provides value
for little additional cost.

# Alternatives

The game / platform code separation comes at a complexity cost, and introduces
an API compatibility constraint between game and platform code. We could instead
distribute each game bundled with the engine code linked in. What we gain in
simplicity however, costs us in game size, which in turn impacts the ability to
build a seamless game-loading experience (that is if each game included
platform code, loading each one from SD to RAM/Flash would take longer).

# Concerns / Open Questions

- How do we detect an updated shared lib during gameplay
- How do we avoid loading a partial shared lib (while the compiler is writing a
  new one)
- Is the "no globals" requirement for hot-loading workable in practice

[q2]: http://fabiensanglard.net/quake2/index.php
[sharedlib]: https://en.wikipedia.org/wiki/Library_(computing)#Shared_libraries
[dlopen]: http://man7.org/linux/man-pages/man3/dlopen.3.html
[dlsym]: http://man7.org/linux/man-pages/man3/dlsym.3.html
[loadlibrary]: https://docs.microsoft.com/en-us/windows/desktop/api/libloaderapi/nf-libloaderapi-loadlibrarya
[getprocaddress]: https://docs.microsoft.com/en-us/windows/desktop/api/libloaderapi/nf-libloaderapi-getprocaddress
[dynamicloading]: https://en.wikipedia.org/wiki/Dynamic_loading
[bootloader]: https://en.wikipedia.org/wiki/Booting#Modern_boot_loaders
[rfc2]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/rfc-0002-hardware-architecture.md
