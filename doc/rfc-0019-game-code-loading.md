# Summary

Outlines the system for loading game code from SD into flash.

# Background

While [RFC-0006][rfc6] defines the layout of platform and game code in flash, it doesn't specify how game code is loaded into flash. The console hardware has a micro SD slot intended for easy loading/sharing of games, but since the STM32 cannot execute code directly from SD, whenever a game is selected we will need to write it to flash before launching it.

Flash erase and write times are relatively slow - for the STM32F7, it takes roughly 7ms to erase and 25ms to program 1KB of data. So for the example game (15KB) that's 0.5 seconds to write the game to flash, not counting the time to read from SD. As such, we'll want to avoid rewriting a game from SD to flash if it has been previously loaded.

# Overview

With the platform/game code separation, the build system for the STM32 emits a `game.bin` file for the game code, alongside game assets:

    build/mini/example/
    ├── data
    │   ├── ball.dat
    │   ├── jump.dat
    │   ├── ...
    │   ├── tiles_testmap_8x8.dat
    │   └── water_8x8.dat
    └─── game.bin

This bin file is a raw ARM binary linked at a specific address (`0x8010000`) and must be written to flash starting at that address to be usable. Our platform code will do the following:

1. Load the game bin file from SD
2. Check if the data is already in flash
3. If not, erase and write the bin file contents to flash
4. Jump to the code in flash

If the code already exists in flash (for example if the console is rebooted after writing a game to flash), then steps 3 (the slow step) will be skipped. This process is similar to how modern game consoles allow installing games from optical media to the console's internal hard drive.

# Implementation

The platform layer for the STM32 already has support for loading files from the SD (used for game data currently), so we'll reuse that to load game binaries.

## New game code checks

To determine if a game has already been written to flash, we'll prepend a small header to the game binary:

```c
typedef struct {
    uint32_t magic;      // Magic value to sanity check the header
    uint32_t buildtime;  // Time the binary was built
    char name[24];       // UTF-8 game name
} game_header_t;
```

This header will include the game name and build time (in seconds). This is simple to generate and quick to load and check. The check logic will be as follows:

```c
game_header_t flash_header, sd_header;
// read game headers for existing game in flash and from game to load in sd
// ...
// check if game names match
bool game_names_different = strncmp(sd_header.name, flash_header.name, 24);
// check if sd game is newer
bool sd_is_newer = sd_header.buildtime > flash_header.buildtime;
if (game_names_different || sd_is_newer) {
    // copy sd to flash
}
```

That is if the game names on sd and flash differ, or if the sd game is newer, write it to flash. If a copy needs to happen, we simply copy the entire binary file (header and code) into flash.

## Flash operations

Most of the flash logic is performed by the STM32 HAL. The STM32 supports erasing flash at a [sector-level][flasherase] and writing flash at 8, 16, or 32 bit granularity. Sectors are either 64 or 128 KB.

For now we'll locate the game code in a single 128KB sector (half of the STM32's flash), requring a single erase operation.

```c
// Unlock flash for erase/write
HAL_FLASH_Unlock();

// Erase game code sector
HAL_FLASH_Erase_Sector(FLASH_SECTOR_4, FLASH_VOLTAGE_RANGE_3);
HAL_FLASH_WaitForLastOperation(FLASH_TIMEOUT_MS);

// Copy game code from SD
uint32_t write_addr = GAME_CODE_FLASH_ADDR;
while (write_addr < end_addr) {

    // Read a chunk of data from SD
    uint32_t buf[64];
    platform_file_read(sd_file, sizeof(buf), buf);

    // Write the data, one word at a time to flash
    for (int i = 0; i < 64; i++) {
        HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, write_addr, buf[i]);
        write_addr += 4;
    }
}

// Wait for last write to finish
HAL_FLASH_WaitForLastOperation(FLASH_TIMEOUT_MS);

// Lock flash
HAL_FLASH_Lock();
```

We avoid having to load the full binary from SD - interleaving the reads from SD with writes to flash. This could be optimised in the future by having the SD code DMA read data from disc, while flash writes are occuring.

# Concerns / Open Questions

The development process for games should still be able to use JTAG to flash game and platform code directly to flash - the flashed game code will always have a newer timestamp than the SD version (if one exists). However, will need to validate that this workflow is reasonble in practice.

[rfc6]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/rfc-0006-game-binaries.md
[flasherase]: https://en.wikipedia.org/wiki/Flash_memory#Block_erasure
