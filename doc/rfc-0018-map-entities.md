# Summary

Define a system for adding game entities to tilemaps and automatically spawning
those entities on map load.

# Background

In our example game code today, any entities are spawned by explicit calls to
an `<foo>_new` call as follows:

```c
asset_load_tilemap("data/testmap.dat", &game->map);
asset_load_image("data/tiles_testmap_8x8.dat", &game->map.tileset);
...
waterfall_new(&game->entitymgr, (v2f){4 * 8, 98 * 8});
game->player = player_new(&game->entitymgr, (v2f){2 * 8, 112 * 8});
```

# Overview

This system doesn't scale very well as we add more entities - it becomes
increasingly hard to visualise where entities are relative to the map. To make
it easier to add entities to a level, we'll add the ability to define where
entities are locate in the map editor.

We'll use Tiled's [object layers][objlayer] feature to add objects to the map.
All we care about initially is object names and positions. When exported to
JSON a Tiled object definition looks like this:


```json
{
    "id":21,
    "name":"player",
    "type":"",
    "x":16,
    "y":896
    "height":8,
    "width":8,
    "rotation":0,
    "visible":true,
},
```

This appears as a 8x8px (1 tile) rectangle with a "player" label above it in
Tiled.

# Implementation

One of the core problems to solve with map entity definitions is how the game
engine knows which entity to spawn. In game code, we only have `entity_type_t`
structures and `*_new` functions. There's nothing to tell the game code that a
"player" string in the map asset should call `player_new`. To handle this we'll
update our `entity_type_t` structures to include a reference to the entity's
name and new function:

```c
 typedef struct entity_type_t {
     const char *name;
     entity_spawn_fn_t spawn;
     entity_tick_fn_t tick;
 } entity_type_t;
```

Eventually we can replace the name string with an integer ID (which would
improve performance and memory usage for map entity spawning). But as an
initial implementation this is sufficient, and doesn't require us to map Tiled
object name strings to integer IDs.

## Updated Map Assets

We'll update our asset converter to take the Tiled object definitions and pack
them after tile data in our map binaries. The structure will be roughly as
follows:

```c
typedef struct {
    char v[12];
} entname_t;

typedef struct {
    uint16_t nameid, x, y;
} entinstance_t;

typedef struct {
    uint32_t etypecount;
    uint32_t entcount;
    entname_t *entnames;
    entinstance_t *entities;
} entitylist_t;
```

We'll have a list of entity names (eg "player"), followed by a list of entity
"instances". Each instance will reference a name (via an index into the list of
names), as well as an x and y position.

## Spawning

Once this structure is loaded from a map, we can spawn entities as follows:

```c

for (int i=0; i<entitylist->entcount; i++) {
    entinstance_t *instance = &entitylist->entities[i];

    // get entity type name from the name table
    char *name = entitylist->entnames[instance->nameid];

    // get the entity type structure with that name
    entity_type_t *type = entitymgr_get_type_by_name(em, name);

    // call the entity type's spawn function with this instance's position
    type->spawn(em, (v2f){instance->x, instance->y})
}
```

[objlayer]: https://doc.mapeditor.org/en/stable/manual/objects/
