# Summary

Outline a [display-list][displaylist] based rendering system. This system will
load drawable assets, batch up commands, sort and filter them (so we only draw
what we need), and draw to the framebuffer.

# Background

Our engine currently provides the `bitmap_blit` function to [blit][blit]
sprites to the framebuffer. This is fine for relatively simple single-screen
scenes. However as we increase the complexity of our game we encounter a few
interesting scenarios:

1. Each entity that wants to draw a sprite needs to know where the current
   [viewport][viewport] is. For example, assume an enemy is at position 100,100
   in the game world, but the player is at 500,500. If we focus on the player
   and draw everything around them that fits on the screen, we won't need to
   draw the enemy sprite (it should be [clipped][clip]). As it stands currently
   each entity will need to perform clipping before it tries to draw a sprite.
2. As we add more entities and effects, we will want to layer how we draw
   sprites (for example we may want the player to always be in front of enemies
   so the character is easy to see). Given how our entity system works, if we
   just draw sprites in the order of our entity list, entities created after
   the player will draw over the player sprite. To resolve this we'd need to
   sort sprites _before_ we draw them.

# Overview

To handle both of these scenarios, we can decouple the step of requesting a
drawing operation from the step of performing that drawing operation. More
concretely, we'd do the following:

1. Create an empty list of "render operations"
2. Game code adds operations to the list (eg. draw sprite 1 at position
   (100,100), layer 1)
3. Step 2 repeats until all operations have been added.
4. Game code tells the renderer to process the render operation list
5. The renderer sorts all operations by their layer, then filters out
   operations which fall outside a viewport passed in from the game code.
6. The renderer then draws the remaining operations to a framebuffer.

Initially our renderer will only support sprite drawing render operations, but
we can easily add further operations as needed (eg, drawing line/circles,
tilemaps, etc).

# Implementation

Our implementation will based around an array of render operation structures as
follows:

```c
struct _render_op_t {
    render_op_id_t op;
    uint8_t layer;
    union {
        struct {
            int16_t x, y;
            render_sprite_id_t sprite;
            uint8_t tile;
        } s;
    };
} render_op_t;

render_op_t render_op_list[MAX_RENDER_OPS];
int num_ops;
```

We'll use a [tagged-union][tagged] to allow a range of render ops (in C a
tagged-union ends up being an enum and a union, where the enum tells you which
member of the union to look at). Each request to draw a sprite will push a
render op onto the list as follows:

```c
void render_push_sprite(int x, int y, int layer, render_sprite_id_t sprite, int tile) {
    render_op_t op = { RENDER_OP_SPRITE, layer, {{x, y, sprite, tile}} };
    render_op_list[num_ops++] = op;
}
```

Sprite IDs will simply be indices into an array of sprite `bitmap_t`'s loaded
at startup. Once all render ops have been pushed onto the list, game code can
call a draw function which will iterate through each op and draw them to a
framebuffer:

```c
void render_draw(bitmap_t *framebuffer, rect_t viewport) {
    // sort render op list into increasing layer order
    sort_by_layer(render_op_list, num_ops);
    // iterate through the ops
    for (int i=0; i<num_ops; i++) {
        render_op_t *op = &render_op_list[i];
        switch (op->op) {
            case RENDER_OP_SPRITE:
                render_draw_sprite(op);
                break;
        }
    }
}
```

Each render op type will then be handled similar to the following:

```c
void render_draw_sprite(render_op_t *op) {
    // only render a sprite if it's in the viewport
    if (op_in_view(op, viewport)) {
        // calculate the position of the sprite relative to the screen then draw
        int x = op->s.x - viewport.x;
        int y = op->s.y - viewport.y;
        bitmap_blit_tile(framebuffer, &sprites[op->sprite], x, y, op->s.tile);
    }
```

Note we'll have to consider partially in-view operations when filtering and
clip them to the screen boundaries appropriately.

# Concerns / Open Questions

The op list could grow quite large for a complex scene, so it might sense to
filter ops at the time of adding them to the list. This would require passing
the viewport to the renderer at the start of a frame.

[displaylist]: https://en.wikipedia.org/wiki/Display_list
[blit]: https://en.wikipedia.org/wiki/Bit_blit
[viewport]: https://en.wikipedia.org/wiki/Viewport
[clip]: https://en.wikipedia.org/wiki/Clipping_(computer_graphics)
[tagged]: https://en.wikipedia.org/wiki/Tagged_union
