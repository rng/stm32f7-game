# Summary

Defines the initial set of functions (memory allocation + file access) exposed
from the platform code to the game.

# Background

[RFC-0003][rfc3] mentions platform API (platform exposing functionality to game
code) in the context of hardware acceleration, but there are a number of more
common OS/Platform APIs we'd like to expose to game code. Specifically memory
allocation and file access, both of which will be needed for loading game
assets (like sprites or music). 

File access is a good example of the need for a platform interface. On any PC
OS we can use the C standard library (libc) file IO functions (fopen, fread,
etc), and the details of reading and writing to the underlying disk will be
handled for us by the OS. However there's no standard non-volatile storage on
small embedded systems - our dev board and final console will have an SD card,
but that's not a given in most embedded cases. Given the range of
microcontrollers, peripherals, and embedded OS's, libc's fopen on embedded will
rarely do anything without additional [glue code][newlibglue]. This glue code
hooks up the libc function to the underlying hardware driver.

We could use libc as our platform API by implementing the necessary glue code
on STM32, but this restricts our API to functionality provided by libc (memory,
file, etc) and we'd still need a separate API for things like the hardware
acceleration mentioned in [RFC-0003][rfc3]. Instead we'll define a structure of
function pointers that the platform code passes to the game. This struct will
define our platform API and give us full flexibility to define platform
interfaces as needed.

# Overview

First lets look at an example interaction between and platform and game code
using the platform API:

       platform   calls   game
       --------------------------------------------
           main    -->    game_init(plat_api)
                 
         malloc    <--    plat_api.mem_alloc(100)
          fopen    <--    plat_api.file_open('foo')
          fread    <--    plat_api.file(...)
                 
           main    -->    game_tick(...)
                          ...

In this example, the platform API is implemented for PC and the platform main
calls `game_init` with a platform API struct. When the game calls
`plat_api.mem_alloc` the platform code implementing this calls out to malloc to
allocate memory from the OS. 

Given that example, we can define a basic API struct, as well as a modified
`game_init`:
                        
    // Asset handle used by platform_api_t file functions
    typedef void *platform_handle_t;

    typedef struct {
        // memory
        void *(*alloc)(size_t size);
        void (*free)(void *ptr);

        // files
        platform_handle_t (*file_open)(const char *name, bool write);
        size_t (*file_read)(platform_handle_t h, size_t n, void *buf);
        size_t (*file_write)(platform_handle_t h, size_t n, void *buf);
        void (*file_close)(platform_handle_t h);
    } platform_api_t;

    game_state_t *game_init(platform_api_t *platform, platform_params_t *params);

# Implementation

## PC Platform

Implementation on Windows / Mac / Linux is simple - our API is almost 1:1 with
most libc functions, so our platform API is just a thin set of shims over
malloc/fopen/fread/etc.

## STM32 Platform

The STM32 platform is more involved. For memory, we'll use malloc/free, but for
files we need to do more work. In terms of low-level hardware drivers,
STM32Cube and the Discovery board BSP provide a [good basis][bspsd] for reading
and writing bytes to the SD card. However to make access to the contents of the
SD easy from PCs, we'll want a file system on the disk (mapping raw bytes on
the disk to higher level concepts like directories and files). The [FAT
filesystem][fat] is supported by all PC OS's and is implemented by
[FatFs][fatfs] for embedded targets. So our resulting stack looks something
like:

        game code
    -- platform API --
     platform wrappers
          FatFs
       Discovery BSP
    STM32Cube SD driver

# Alternatives

As mentioned above we could use `libc` as an platform abstraction layer, and
I'm still on the fence as this being a better idea. If we used `libc` we could
add other non-libc-supported platform abstractions via our own custom struct.
If we later decide `libc` is a better abstraction choice, we can relatively
easily switch from the current solution

An alternative to the struct-of-function-pointers approach is to have the game
code link against a platform object file which provides the platform-specific
functions. This makes for a runtime vs compile time difference. In our current
system if the platform and game code are built separately (eg as DLLs), it's
possible to update (or fix bugs in) the platform code without rebuilding the
game. The compromise is flexibility of calling (slightly simpler to call
functions linked into our binary rather than function pointers) vs decoupling
(the platform code being separately linked allows more options in updating
platform / game code independently).

# Concerns / Open Questions

[rfc3]: https://bitbucket.org/rng/stm32f7-game/src/86831f8698bd718693c21d9ab9eb77e0387048c2/doc/rfc-0003-engine-platform.md
[newlibglue]: https://www.embecosm.com/appnotes/ean9/ean9-howto-newlib-1.0.html#id2719266
[fatfs]: http://elm-chan.org/fsw/ff/00index_e.html
[bspsd]: https://bitbucket.org/rng/stm32f7-game/src/master/src/lib/bsp/STM32746G-Discovery/stm32746g_discovery_sd.c
[fat]: https://en.wikipedia.org/wiki/File_Allocation_Table
