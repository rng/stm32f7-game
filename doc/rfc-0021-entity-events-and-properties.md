# Summary

A system for setting properties on in game entities, as well as a way for entities to communicate with each other.

# Background

The entity system in the engine is rather bare-bones at this stage. Only entity position and type can be defined in the level files. This results in very limited control when building levels. As en example, there's no way to define which direction an enemy is facing in the level editor. Additionally, once a level is loaded and entities are being simulated in the game engine, there's no way for entities to interact with each other (for example, to have the player pick up a power-up).

To address the first issue, we're going to add properties to our entities - effectively a map of key-value pairs like `facing_direction = left`. These properties will be definable in the level editor and automatically passed to new entities when spawned from a level.

For the second issue, we'll add a very simple event system whereby entities can send messages ("events") to other entities.

# Overview

Tiled supports [custom properties][tiledprops] on objects in the editor. This is a perfect fit for our property system. When exported to JSON a property on an object is represented as follows:

```json
     "id":21,
     "name":"player",
     "properties":[
            {
             "name":"direction",
             "type":"int",
             "value":1
            }],
     "visible":true,
     "x":16,
     "y":896
```

We'll add logic to our asset converter to extract these properties and support in the level file format for representing them. Then on level load, properties will be passed to entity `new` functions.

Events will be incredibly basic initially - a new `event` callback added to `entity_type_t` structure will take an event id and a parameter. An event can then be sent from one object to another with:

```c
target_ent->type->event(target_ent, EVENT_ID_DAMAGE);
```

# Implementation

## Properties

To simplify the storage and memory management of properties we're going to restrict property values to integers and hash the keys. This makes for a straightforward key-value pair structure:

```c
struct entprop_t {
    uint32_t key, val;
};

struct entity_props_t {
    size_t count;
    entprop_t *p;
}
```

For our asset converter to export properties from a Tiled level, we'll do something like:

```python
encoded_props = ""
for property in tiled_obj['properties']:
    assert property['type'] == 'int', "Only int properties supported"
    key = prop_hash(property['name'])
    val = property['value']
    encoded_props += struct.pack('II', key, val)
```

Looking up a property in the game code will be similar:

```c
bool prop_lookup(entity_props_t *props, char *key, uint32_t *val) {
    uint32_t hkey = prop_hash(key);
    for (int i=0; i<props->count;i ++) {
        if (props->p[i].key == hkey) {
            *val = props->p[i].val;
            return true;
        }
    }
    return false;
}
```

Overall simple, but with a couple issues we'll need to keep in mind:

* No handling of hash collisions in key names. For now we can check for collisions when converting the level assets and fail early.
* Linear lookup of keys, so won't scale to larger number of properties. However the expectation is only a few (5-10) properties per entity.

## Events

As mentioned earlier, events are very simple - we'll expand our entity type structure as follows:

```c
typedef void (*entity_event_fn_t)(void *entity, int event, void *param);

struct entity_type_t {
    const char *name;
    entity_spawn_fn_t spawn;
    entity_tick_fn_t tick;
    entity_event_fn_t event;
    uint32_t flags;
};
```

Then add a function to call the event callback for a given entity:

```c
void entity_send_event(entity_t *e, uint32_t msg, void *param) {
    ASSERT_MSG(e->type->event, "%s has no event handler", e->type->name);
    e->type->event(e, msg, param);
}
```

The `void*` parameter will allow passing a range of parameters to the recipient object:

```c
// player takes 1 damage
entity_send_event(player, ENTITY_EVENT_DAMAGE, (void*)1);

// player picks up a blue cheese
inventory_item_t item = { ITEM_CHEESE, ITEM_COLOUR_BLUE };
entity_send_event(e, ENTITY_ENVENT_PICKUP, &item);
```

# Concerns / Open Questions

* The event system is making the entity type object look more and more like a C++ class. Given that we've switched to C++ (initially for operator overloading and destructors), it might be worth seeing what a class-based version of the type object looks like.
* Additional type support for properties (strings or floats for example) could be handy, and at that point it might be worth re-evaluating using the hashing system for keys.

[tiledprops]: https://doc.mapeditor.org/en/stable/manual/custom-properties/
